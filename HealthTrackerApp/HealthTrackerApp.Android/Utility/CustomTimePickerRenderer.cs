﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HealthTrackerApp.Droid.Utility;
using HealthTrackerApp.Utility;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
namespace HealthTrackerApp.Droid.Utility
{
    class CustomTimePickerRenderer : TimePickerRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
        {
            base.OnElementChanged(e);
            this.Control.SetTextColor(Android.Graphics.Color.Black);
            this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            this.Control.SetPadding(20, 0, 0, 0);
            // this.Control.SetHeight(100);
            GradientDrawable gd = new GradientDrawable();
            gd.SetCornerRadius(50); //increase or decrease to changes the corner look
            gd.SetColor(Android.Graphics.Color.White);
            gd.SetStroke(3, Android.Graphics.Color.White);
            this.Control.SetBackgroundDrawable(gd);

            CustomTimePicker element = Element as CustomTimePicker;
            if (!string.IsNullOrWhiteSpace(element.Placeholder))
            {
                Control.Text = element.Placeholder;
            }
            this.Control.TextChanged += (sender, arg) =>
            {
                var selectedDate = arg.Text.ToString();
                if (selectedDate == element.Placeholder)
                {
                    Control.Text = DateTime.Now.ToString("h:mm tt");
                }
            };
        }
    }
}