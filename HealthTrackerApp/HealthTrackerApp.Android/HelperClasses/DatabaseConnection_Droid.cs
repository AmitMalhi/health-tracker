﻿using System.IO;
using HealthTrackerApp.iOS.HelperClasses;
using SQLite;
using HealthTrackerApp.DatabaseLayer.Interface;

[assembly: Xamarin.Forms.Dependency(typeof(DroidDatabaseConnection))]
namespace HealthTrackerApp.iOS.HelperClasses
{
    public class DroidDatabaseConnection : IDatabaseConnection
    {
        public SQLiteConnection DbConnection()
        {
            var dbName = "HealthTrackerDB.db3";
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
            return new SQLiteConnection(path);
        }
        public bool DBFileExists()
        {
            var dbName = "HealthTrackerDB.db3";
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
            if (File.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}