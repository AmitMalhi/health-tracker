﻿using AndroidHUD;
using HealthTrackerApp.Droid.HelperClasses;
using HealthTrackerApp.HelperClasses;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(ShowProgressDialog))]
namespace HealthTrackerApp.Droid.HelperClasses
{
    public class ShowProgressDialog : IShowProgressDialog
    {
        public void dissmissDialog()
        {
            AndHUD.Shared.Dismiss(Forms.Context);
        }

        public void showDialog(string message)
        {
            AndHUD.Shared.Show(Forms.Context, message);
        }
    }
}