﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HealthTrackerApp.Droid.HelperClasses;
using HealthTrackerApp.HelperClasses;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(ShareNotes))]
namespace HealthTrackerApp.Droid.HelperClasses
{
    public class ShareNotes : IShareNotes
    {
       
  public void OpenShareIntent(string texttoshare)
        {
            var myIntent = new Intent(Android.Content.Intent.ActionSend);
            myIntent.SetType("text/plain");
            myIntent.PutExtra(Intent.ExtraText, texttoshare);
            Forms.Context.StartActivity(Intent.CreateChooser(myIntent, "Choose an App"));
        }
    }
}