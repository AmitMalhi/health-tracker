﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using HealthTrackerApp.iOS.Utility;
using HealthTrackerApp.Utility;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
namespace HealthTrackerApp.iOS.Utility
{
   public class CustomTimePickerRenderer : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);

            if (this.Control == null)
                return;
            var element = e.NewElement as CustomTimePicker;
            if (element != null)
            {
                if (!string.IsNullOrWhiteSpace(element.Placeholder))
                {
                    Control.Text = element.Placeholder;
                }
            }

            Control.BorderStyle = UITextBorderStyle.RoundedRect;
            Control.Layer.MasksToBounds = true;
            Control.Layer.CornerRadius = 20;
            Control.Layer.BorderWidth = 1f;
            Control.AdjustsFontSizeToFitWidth = true;
            Control.TextColor = UIColor.Black;

            Control.ShouldEndEditing += (textField) => {
                var seletedDate = (UITextField)textField;
                var text = seletedDate.Text;
                if (text == element.Placeholder)
                {
                    Control.Text = DateTime.Now.ToString("h:mm tt");
                }
                return true;
            };
        }
        private void OnCanceled(object sender, EventArgs e)
        {
            Control.ResignFirstResponder();
        }
        private void OnDone(object sender, EventArgs e)
        {
            Control.ResignFirstResponder();
        }
    }
}