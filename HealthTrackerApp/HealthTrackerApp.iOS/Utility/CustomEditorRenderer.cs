﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using HealthTrackerApp.iOS.Utility;
using HealthTrackerApp.Utility;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace HealthTrackerApp.iOS.Utility
{
    class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {

            base.OnElementChanged(e);
            if (Control != null)
            {

                //Control.IndicatorStyle = UITextBorderStyle.None;
                //Control.Layer.CornerRadius = 10;
                //Control.TextColor = UIColor.White;
            }
        }
    }
}