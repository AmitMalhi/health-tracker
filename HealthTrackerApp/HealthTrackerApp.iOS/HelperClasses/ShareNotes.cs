﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.iOS.HelperClasses;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(ShareNotes))]
namespace HealthTrackerApp.iOS.HelperClasses
{
    public class ShareNotes : IShareNotes
    {
        public void OpenShareIntent(string texttoshare)
        {
            var activityController = new UIActivityViewController(new NSObject[] { UIActivity.FromObject(texttoshare) }, null);
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(activityController, true, null);
        }
    }
}