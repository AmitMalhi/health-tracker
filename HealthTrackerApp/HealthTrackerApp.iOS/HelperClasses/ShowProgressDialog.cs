﻿using BigTed;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.iOS.HelperClasses;

[assembly: Xamarin.Forms.Dependency(typeof(ShowProgressDialog))]
namespace HealthTrackerApp.iOS.HelperClasses
{
    public class ShowProgressDialog : IShowProgressDialog
    {
        public void dissmissDialog()
        {
            BTProgressHUD.Dismiss();
        }

        public void showDialog(string message)
        {
            BTProgressHUD.Show(message, -1, ProgressHUD.MaskType.Black);
        }
    }
}