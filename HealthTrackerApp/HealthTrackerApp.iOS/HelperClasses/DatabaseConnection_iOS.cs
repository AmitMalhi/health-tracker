﻿using System;
using System.IO;
using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.iOS.HelperClasses;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(iOSDatabaseConnection))]
namespace HealthTrackerApp.iOS.HelperClasses
{
    class iOSDatabaseConnection : IDatabaseConnection
    {
        public SQLiteConnection DbConnection()
        {
            var dbName = "HealthTrackerDB.db3";
            string personalFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, dbName);
            return new SQLiteConnection(path);
        }
        public bool DBFileExists()
        {
            var dbName = "HealthTrackerDB.db3";
            string personalFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, dbName);
            if (File.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}