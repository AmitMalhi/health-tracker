﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Reminders.Models;
using HealthTrackerApp.Utility;
using Plugin.LocalNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using static HealthTrackerApp.Reminders.Pages.ReminderDetailsPage;

namespace HealthTrackerApp.Reminders.Pages
{
    public class ReminderPage : BasePage, IReminderPageUpdate
    {
        private ListView listView;
        private List<Reminder> reminders,finalreminderlist;
        private StackLayout stackLayout;
        private long lastPress;
        private ScrollView scrollView;
        

        public ReminderPage()
        {
            Title = "Reminders";
            reminders = new List<Reminder>();
            finalreminderlist = new List<Reminder>();
            listView = new ListView();
            stackLayout = new StackLayout();
            scrollView = new ScrollView();
            stackLayout.Orientation = StackOrientation.Vertical;

            // add the icon on the toolbar
            ToolbarItems.Add(new ToolbarItem("", "add.png", () =>
            {
                //logic code goes here

                Navigation.PushAsync(new ReminderDetailsPage(this));
            }));

            //Fetch reminder from Database
            reminders = new ReminderDBRepository().GetAll();

            if(reminders != null && reminders.Count > 0)
            {
                setUI();
            }
            else
            {
                if (InternetConnectivity.IsInternetConnected)  //Check Internet Connection
                {
                    reminders = APICalls.GetReminders(); //Hit API

                    //Save to Database
                    foreach (var reminderItem in reminders)
                    {
                        new ReminderDBRepository().InsertOrReplace(reminderItem);
                    }

                    //Fetch reminder from Database
                    reminders = new ReminderDBRepository().GetAll();

                    setUI();
                }
                else
                {
                    DisplayAlert("No Internet", "", "OK");
                }
            }

            //Device.BeginInvokeOnMainThread(new Action(() =>
            //{
            //    DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            //}));
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
        }

        void setUI()
        {
            if (reminders != null && reminders.Count > 0)
            {
                foreach (var item in reminders)
                {
                    var activityLabel = new Label();
                    activityLabel.Text = item.Activity;
                    activityLabel.FontSize = 18;
                    activityLabel.TextColor = Color.FromHex(MyUtility.MenuTextColor);

                    var dateLabel = new Label();
                    dateLabel.Text = item.Date;
                    dateLabel.FontSize = 15;
                    dateLabel.TextColor = Color.FromHex(MyUtility.MenuTextColor);

                    var activityDateStack = new StackLayout();
                    activityDateStack.Orientation = StackOrientation.Vertical;
                    activityDateStack.Children.Add(activityLabel);
                    activityDateStack.Children.Add(dateLabel);
                    activityDateStack.Margin = new Thickness(10, 10, 10, 0);
                    activityDateStack.WidthRequest = 500;

                    
                    var deletereminderImg = new Image();
                    deletereminderImg.Source = "delete.png";
                    deletereminderImg.WidthRequest = 25;
                    deletereminderImg.HeightRequest = 25;
                    deletereminderImg.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnDeleteIconClick(item.ReminderId)) });

                    Switch mSwitch = new MySwitch((int)item.ReminderId);
                    mSwitch.IsToggled = item.IsEnabled;
                    mSwitch.Toggled += MSwitch_Toggled;

                    var activityToggleStack = new StackLayout();
                    activityToggleStack.Orientation = StackOrientation.Horizontal;
                    activityToggleStack.Children.Add(activityDateStack);
                    if(item.IsImgVisible)
                    activityToggleStack.Children.Add(deletereminderImg);
                    activityToggleStack.Children.Add(mSwitch);
                    activityToggleStack.Margin = new Thickness(0, 10, 10, 0);
                    stackLayout.Children.Add(activityToggleStack);
                }
            }
            else
            {
                StackLayout emptyLayout = new StackLayout();
                emptyLayout.VerticalOptions = LayoutOptions.Center;
                emptyLayout.HorizontalOptions = LayoutOptions.Center;
                Label emptyLabel = new Label();
                emptyLabel.Text = "No Reminders";
                emptyLayout.Children.Add(emptyLabel);
                stackLayout.Children.Add(emptyLayout);
            }

            scrollView.Content = stackLayout;
            this.Content = scrollView;
        }

        private async void OnDeleteIconClick(long reminderid)
        {
          bool isdelete = await  DisplayAlert("Alert", "Do you want to delete this reminder?", "yes", "no");
            if (isdelete)
            {
                CrossLocalNotifications.Current.Cancel((int)reminderid);
                new ReminderDBRepository().Delete(reminderid);
                reminders.Clear();
                reminders = new ReminderDBRepository().GetAll();
                stackLayout = new StackLayout();
                setUI();
            }
            else
            {
                return;
            }
            
        }

        private void MSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            var isEnable = e.Value;
            var selectedReminder = sender as MySwitch;
            var reminderId = selectedReminder.Tag;

            Reminder reminder = APICalls.GetReminders().Where(x => x.ReminderId == reminderId).ToList().FirstOrDefault();

            if(reminder != null)
            {
                if (isEnable)
                {
                    var obj = APICalls.GetReminders().FirstOrDefault(x => x.ReminderId == reminderId);

                    obj.IsEnabled = true;
                    new ReminderDBRepository().Update(obj);

                    CrossLocalNotifications.Current.Show(reminder.Activity, "", (int)reminder.ReminderId, reminder.Created.AddSeconds(MyUtility.reminderTime));
                }
                else
                {
                    var obj = APICalls.GetReminders().FirstOrDefault(x => x.ReminderId == reminderId);
                    obj.IsEnabled = false;
                    new ReminderDBRepository().Update(obj);

                    CrossLocalNotifications.Current.Cancel((int)reminder.ReminderId);
                }
            }
            else
            {
                if (!isEnable)
                {
                    Reminder dbreminder = new ReminderDBRepository().Get(x => x.ReminderId == reminderId);
                    dbreminder.IsEnabled = false;
                    new ReminderDBRepository().Update(dbreminder);
                    CrossLocalNotifications.Current.Cancel((int)dbreminder.ReminderId);
                }
                else
                {
                    Reminder dbreminder = new ReminderDBRepository().Get(x => x.ReminderId == reminderId);
                    dbreminder.IsEnabled = true;
                    new ReminderDBRepository().Update(dbreminder);
                    DateTime currentdatetime = Convert.ToDateTime(dbreminder.Date);
                    CrossLocalNotifications.Current.Show(dbreminder.Activity, "", (int)dbreminder.ReminderId, currentdatetime);
                }
            }
            
        }
        protected override bool OnBackButtonPressed()
        {
            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 5000)
            {
                DisplayAlert("", "Press back again to exit", "OK");
                lastPress = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void UpdateManualReminderInfo()
        {
            reminders.Clear();
            stackLayout = new StackLayout();
             reminders = new ReminderDBRepository().GetAll();
            setUI();
        }
        
    }
}
