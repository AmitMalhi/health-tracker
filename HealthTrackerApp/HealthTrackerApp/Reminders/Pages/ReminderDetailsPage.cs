﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Reminders.Models;
using HealthTrackerApp.Utility;
using Plugin.InputKit.Shared.Controls;
using Plugin.LocalNotifications;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Reminders.Pages
{
   public class ReminderDetailsPage : BasePage
    {
        private StackLayout stacklayout;
        private Label reminderlbl, datepickerlbl, timepickerlbl;
        private AdvancedEntry reminderentry;
        private CustomDatePicker reminderdatepicker;
        private CustomTimePicker remindertimepicker;
        private Reminder currentreminder;
        private IReminderPageUpdate reminderPageUpdate;

        public ReminderDetailsPage(IReminderPageUpdate reminderPageUpdate)
        {
            Title = "Add Reminder";
            NavigationPage.SetHasBackButton(this, true);
            stacklayout = new StackLayout();
            reminderlbl = new Label();
            datepickerlbl = new Label();
            timepickerlbl = new Label();
            
            this.reminderPageUpdate = reminderPageUpdate;
            reminderdatepicker = new CustomDatePicker();
            remindertimepicker = new CustomTimePicker();
            reminderentry = new AdvancedEntry();
            ToolbarItems.Add(new ToolbarItem("save", "", () => {
                string dateval = reminderdatepicker.Date.ToString("MM/dd/yyyy");
              //  string timeval = remindertimepicker.Time.ToString(" h:mm tt");
                DateTime date = reminderdatepicker.Date + remindertimepicker.Time;
                if (!string.IsNullOrEmpty(reminderentry.Text) && !string.IsNullOrEmpty(dateval))
                {
                    // save values in the database
                    currentreminder = new Reminder(DateTime.UtcNow.Ticks,reminderentry.Text, date, true,true);
                    new ReminderDBRepository().Save(currentreminder);
                    reminderPageUpdate.UpdateManualReminderInfo();
                    CrossLocalNotifications.Current.Show(currentreminder.Activity, "",(int)currentreminder.ReminderId, date);
                    Navigation.PopAsync();
                }
                else
                {
                    DisplayAlert("Alert", "Title cannot be empty!", "Ok");
                }
                 
            }));
            updateUI();
        }

        public interface IReminderPageUpdate
        {
            void UpdateManualReminderInfo();
        }

        private void updateUI()
        {
            // update UI for the reminderlbl
            reminderlbl.Text = "Title";
            reminderlbl.Margin = new Thickness(5, 5, 5, 0);

            reminderentry.HorizontalOptions = LayoutOptions.FillAndExpand;
            reminderentry.Margin = new Thickness(5, 0, 5, 0);

            datepickerlbl.Text = "Date";
            datepickerlbl.Margin = new Thickness(5, 0, 5, 0);

            reminderdatepicker.HorizontalOptions = LayoutOptions.FillAndExpand;
            reminderdatepicker.Margin = new Thickness(5, 0, 5, 0);
            reminderdatepicker.HeightRequest = 50;

            timepickerlbl.Text = "Time";
            timepickerlbl.Margin = new Thickness(5, 0, 5, 0);

            remindertimepicker.HorizontalOptions = LayoutOptions.FillAndExpand;
            remindertimepicker.Margin = new Thickness(5, 0, 5, 0);
            remindertimepicker.HeightRequest = 50;
            remindertimepicker.Time = DateTime.Now.TimeOfDay;
            remindertimepicker.Format = "hh:mm tt";


            stacklayout.HorizontalOptions = LayoutOptions.FillAndExpand;
            stacklayout.Spacing = 10;
            stacklayout.Children.Add(reminderlbl);
            stacklayout.Children.Add(reminderentry);
            stacklayout.Children.Add(datepickerlbl);
            stacklayout.Children.Add(reminderdatepicker);
            stacklayout.Children.Add(timepickerlbl);
            stacklayout.Children.Add(remindertimepicker);
            this.Content = stacklayout;
        }
    }
}
