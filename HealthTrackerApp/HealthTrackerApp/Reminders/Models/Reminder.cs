﻿using HealthTrackerApp.Utility;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Reminders.Models
{
   public class Reminder
    {
        [PrimaryKey]
        public long ReminderId { get; set; }
        public string Activity { get; set; }
        public string Date { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime Created { get; set; }
        public bool IsImgVisible { get; set; }

        public Reminder(long id, string Activity, DateTime Created, bool IsEnabled, bool IsImgVisible)
        {
            this.IsImgVisible = IsImgVisible;
            this.ReminderId = id;
            this.Activity = Activity;
            this.Date = Created.ToString("MM/dd/yyyy h:mm tt");
            this.Created = Created;
            this.IsEnabled = IsEnabled;
        }

        public Reminder()
        {

        }
    }
}
