﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.NutritionValues.Models;
using HealthTrackerApp.NutritionValues.NutritionHelperClass;
using HealthTrackerApp.Utility;
using Plugin.InputKit.Shared.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.NutritionValues.Pages
{
    class NutritionItemPage : BasePage
    {
        private SearchBar searchBar;
        private ObservableCollection<FoodItemHelper> nutritionitems;
        private ObservableCollection<FoodItems> mfinallist;
        private StackLayout stackLayout;
        private ListView searchlistmenu, mfinallistview;
        private int mealId;
        private DateTime xdateTime;
        private Label foodquantity;
        FoodItems fd;

        public NutritionItemPage(int id, DateTime dateTime)
        {
            NavigationPage.SetHasNavigationBar(this, true);

            NavigationPage.SetHasBackButton(this, true);

            if (id == 1)
            {
                Title = "Breakfast";
            }
            else if (id == 2)
            {
                Title = "Lunch";
            }
            else if (id == 3)
            {
                Title = "Dinner";
            }

            this.mealId = id;

            xdateTime = dateTime.Date;

            nutritionitems = new ObservableCollection<FoodItemHelper>();

            mfinallist = new ObservableCollection<FoodItems>();

            searchBar = new SearchBar();
            stackLayout = new StackLayout();
            searchlistmenu = new ListView();
            mfinallistview = new ListView();

            GetAndSetDatafromAPI();

            searchBar.TextChanged += SearchBar_TextChanged;
            searchlistmenu.ItemSelected += Searchlistmenu_ItemSelected;

            mfinallistview.ItemTapped += Mfinallistview_ItemTapped;
        }

        async void GetAndSetDatafromAPI()
        {
            NutrientsHelper nutrients = await ApiHelper.APICalls.getFoodItemsFromAPI();

            new FoodItemDBRepository().DeleteAll();

            if (nutrients != null && nutrients.foodItemHelper.Count > 0)
            {
                foreach (var item in nutrients.foodItemHelper)
                {
                    new FoodItemDBRepository().Save(new FoodItemHelper(item.id, item.item, item.calorie, item.fat, item.carbs, item.protein));
                }
            }

            var xfoodItem = new FoodItemDBRepository().GetAll();

            foreach (var item in xfoodItem)
            {
                nutritionitems.Add(item);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var xfoodItems = new NutritionDBRepository().GetAll().Where(x => x.Foodid == mealId).Where(x => x.dateTime == xdateTime).ToList();

            if (xfoodItems != null && xfoodItems.Count > 0)
            {
                mfinallist.Clear();
                foreach (var item in xfoodItems)
                {
                    mfinallist.Add(item);
                }
            }
            else
            {
                mfinallist.Clear();
            }
            InitUI();
        }

        private void InitUI()
        {
            searchlistmenu.IsVisible = false;
            searchBar.Placeholder = "Search Food Item";
            searchBar.HorizontalOptions = LayoutOptions.FillAndExpand;
            searchBar.VerticalOptions = LayoutOptions.StartAndExpand;
            stackLayout.Children.Add(searchBar);
            stackLayout.Children.Add(searchlistmenu);
            stackLayout.Children.Add(mfinallistview);

            mfinallistview.ItemTemplate = new DataTemplate(() =>
            {
                var label = new Label();
                label.SetBinding(Label.TextProperty, "Item");
                label.FontSize = 19;
                label.TextColor = Color.Black;
                label.Margin = new Thickness(10);

                foodquantity = new Label();
                foodquantity.SetBinding(Label.TextProperty, "Quantity");
                foodquantity.FontSize = 19;
                foodquantity.TextColor = Color.Black;
                foodquantity.Margin = new Thickness(10);
                foodquantity.HorizontalOptions = LayoutOptions.EndAndExpand;

                ViewCell viewCell = new ViewCell();
                viewCell.View = new StackLayout
                {
                    Children = { label, foodquantity },
                    Orientation = StackOrientation.Horizontal
                };
                return viewCell;
            });

            searchlistmenu.ItemTemplate = new DataTemplate(() =>
            {
                var label = new Label();
                label.SetBinding(Label.TextProperty, "item");
                label.FontSize = 19;
                label.TextColor = Color.Black;
                label.Margin = new Thickness(10);
                return new ViewCell
                {
                    View = label
                };
            });

            if (mfinallist != null && mfinallist.Count > 0)
            {
                mfinallistview.ItemsSource = mfinallist;
            }

            Content = stackLayout;
        }

        private void Mfinallistview_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var fd = e.Item as FoodItems;
            Navigation.PushAsync(new ItemDetailsPage(fd));
        }

        private void DeleteAction_Clicked(object sender, EventArgs e)
        {
            var mi = sender as MenuItem;
            var fi = mi.CommandParameter as FoodItems;

            new NutritionDBRepository().Delete(fi.xid);

            var xfoodItems = new NutritionDBRepository().GetAll().ToList().Where(x => x.Foodid == mealId).Where(x => x.dateTime == xdateTime).ToList();

            mfinallist.Clear();

            if (xfoodItems != null && xfoodItems.Count > 0)
            {
                foreach (var item in xfoodItems)
                {
                    mfinallist.Add(item);
                }
            }
            mfinallistview.ItemsSource = mfinallist;
        }

        private void Searchlistmenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            FoodItemHelper foodItems = (FoodItemHelper)e.SelectedItem;

            string dateTime = DateTime.Now.Ticks.ToString();

            var quantity = 1.0f;
            
            fd = new FoodItems(foodItems.id + dateTime, foodItems.item, foodItems.calorie, foodItems.fat, foodItems.carbs, foodItems.protein, mealId, xdateTime, quantity);
            new NutritionDBRepository().Save(fd);
            mfinallist.Add(fd);
            mfinallistview.ItemsSource = mfinallist;
            searchBar.Text = "";
            searchlistmenu.IsVisible = false;
            mfinallistview.IsVisible = true;
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(e.NewTextValue))
            {
                mfinallistview.IsVisible = true;
                searchlistmenu.IsVisible = false;
                searchlistmenu.ItemsSource = new List<FoodItemHelper>();
            }
            else
            {
                searchlistmenu.IsVisible = true;
                mfinallistview.IsVisible = false;
                string keyword = searchBar.Text.ToLower();
                IEnumerable<FoodItemHelper> filtereditems = nutritionitems.Where(x => x.item.ToLower().Contains(keyword));
                searchlistmenu.ItemsSource = filtereditems;
            }
        }
    }
}