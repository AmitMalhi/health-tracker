﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.NutritionValues.Models;
using HealthTrackerApp.NutritionValues.NutritionHelperClass;
using HealthTrackerApp.Profile.Pages;
using ProgressRingControl.Forms.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.NutritionValues.Pages
{
    class NutritionMainPage : BasePage
    {
        private ProgressRing progressRing;
        private ProgressBar carbbar, fatbar, proteinbar;
        private StackLayout parentStack, progressStack, mprogressStack, carbStack, fatStack, proteinStack, finalprogresslayout;
        private Label caloriesLabel, fatLabel, carbLabel, proteinLabel, breakfastlbl, lunchlbl, dinnerlbl, carbInLabel, fatInLabel, proteinInLabel;
        private UserProfile userProfile;
        private StackLayout dateselectionlayout, breakfaststack, lunchstack, dinnerstack;
        private Image leftarrow, rightarrow, breakfasticon, lunchicon, dinnericon, baddicon, laddicon, daddicon;
        private DatePicker datepicker;
        private ScrollView scrollView;
        
        protected override void OnAppearing()
        {
            base.OnAppearing();

            Title = "Nutrition Value";

            userProfile = new UserProfileDBRepository().GetFirstOrDefault();

            InitUI();

            InitValues();
        }

        private void InitUI()
        {
            scrollView = new ScrollView();
            scrollView.BackgroundColor = Color.FromHex("#f3f5f6");
            progressStack = new StackLayout();
            progressStack.Margin = new Thickness(10);

            progressRing = new ProgressRing();
            progressRing.RingBaseColor = Color.DarkGray;
            progressRing.RingProgressColor = Color.FromHex("#4c7e90");
            progressRing.RingThickness = 12;

            progressRing.Margin = new Thickness(5);
            progressRing.HeightRequest = 150;
            progressRing.Progress = 0;
            progressRing.AnimatedProgress = 0;
            progressRing.StartProgressToAnimation();

            caloriesLabel = new Label();
            caloriesLabel.Text = "CALORIES";
            caloriesLabel.TextColor = Color.Black;
            caloriesLabel.FontSize = 15;
            caloriesLabel.HorizontalOptions = LayoutOptions.Center;

            mprogressStack = new StackLayout();
            mprogressStack.Orientation = StackOrientation.Horizontal;
            mprogressStack.Margin = new Thickness(10);
            mprogressStack.HorizontalOptions = LayoutOptions.Center;

            carbInLabel = new Label();
            carbInLabel.TextColor = Color.Black;
            carbInLabel.FontSize = 10;
            carbInLabel.HorizontalOptions = LayoutOptions.Center;

            carbbar = new ProgressBar();
            carbbar.Progress = 0;
            carbbar.ProgressTo(0, 250, Easing.Linear);
            carbbar.ProgressColor = Color.FromHex("#4c7e90");
            carbbar.WidthRequest = 50;

            carbLabel = new Label();
            carbLabel.Text = "CARBS";
            carbLabel.TextColor = Color.Black;
            carbLabel.FontSize = 12;
            carbLabel.HorizontalOptions = LayoutOptions.Center;

            carbStack = new StackLayout();
            carbStack.HorizontalOptions = LayoutOptions.Start;
            carbStack.Children.Add(carbInLabel);
            carbStack.Children.Add(carbbar);
            carbStack.Children.Add(carbLabel);

            fatInLabel = new Label();
            fatInLabel.TextColor = Color.Black;
            fatInLabel.FontSize = 10;
            fatInLabel.HorizontalOptions = LayoutOptions.Center;

            fatbar = new ProgressBar();
            fatbar.Progress = 0;
            fatbar.ProgressTo(0, 250, Easing.Linear);
            fatbar.ProgressColor = Color.FromHex("#4c7e90");
            fatbar.WidthRequest = 50;

            fatLabel = new Label();
            fatLabel.Text = "FAT";
            fatLabel.TextColor = Color.Black;
            fatLabel.FontSize = 12;
            fatLabel.HorizontalOptions = LayoutOptions.Center;

            fatStack = new StackLayout();
            fatStack.HorizontalOptions = LayoutOptions.Center;
            fatStack.Children.Add(fatInLabel);
            fatStack.Children.Add(fatbar);
            fatStack.Children.Add(fatLabel);

            proteinInLabel = new Label();
            proteinInLabel.TextColor = Color.Black;
            proteinInLabel.FontSize = 10;
            proteinInLabel.HorizontalOptions = LayoutOptions.Center;

            proteinbar = new ProgressBar();
            proteinbar.Progress = 0;
            proteinbar.ProgressTo(0, 250, Easing.Linear);
            proteinbar.ProgressColor = Color.FromHex("#4c7e90");
            proteinbar.WidthRequest = 50;

            proteinLabel = new Label();
            proteinLabel.Text = "PROTEIN";
            proteinLabel.TextColor = Color.Black;
            proteinLabel.FontSize = 10;
            proteinLabel.HorizontalOptions = LayoutOptions.Center;

            proteinStack = new StackLayout();
            proteinStack.HorizontalOptions = LayoutOptions.End;
            proteinStack.Children.Add(proteinInLabel);
            proteinStack.Children.Add(proteinbar);
            proteinStack.Children.Add(proteinLabel);

            mprogressStack.Children.Add(carbStack);
            mprogressStack.Children.Add(proteinStack);
            mprogressStack.Children.Add(fatStack);
            mprogressStack.Spacing = 50;

            progressStack.Children.Add(progressRing);
            progressStack.Children.Add(caloriesLabel);

            dateselectionlayout = new StackLayout();
            dateselectionlayout.HorizontalOptions = LayoutOptions.FillAndExpand;
            dateselectionlayout.Orientation = StackOrientation.Horizontal;

            leftarrow = new Image();
            leftarrow.HeightRequest = 17;
            leftarrow.WidthRequest = 17;
            leftarrow.HorizontalOptions = LayoutOptions.StartAndExpand;
            leftarrow.Source = "leftarrow.png";

            rightarrow = new Image();
            rightarrow.HeightRequest = 17;
            rightarrow.WidthRequest = 17;
            rightarrow.Source = "rightarrow.png";
            rightarrow.HorizontalOptions = LayoutOptions.EndAndExpand;

            datepicker = new DatePicker();
            datepicker.HeightRequest = 50;
            datepicker.WidthRequest = 100;
            datepicker.DateSelected += Datepicker_DateSelected;
            if (Device.OS == TargetPlatform.iOS)
            {
                datepicker.WidthRequest = 120;
            }
            datepicker.FontSize = 22;
            datepicker.HorizontalOptions = LayoutOptions.CenterAndExpand;
            dateselectionlayout.Margin = new Thickness(10);
            dateselectionlayout.Children.Add(leftarrow);
            dateselectionlayout.Children.Add(datepicker);
            dateselectionlayout.Children.Add(rightarrow);

            baddicon = new Image();
            baddicon.WidthRequest = 30;
            baddicon.HeightRequest = 30;
            baddicon.Margin = new Thickness(0, 0, 5, 0);
            baddicon.HorizontalOptions = LayoutOptions.EndAndExpand;
            baddicon.Source = "plus.png";

            laddicon = new Image();
            laddicon.WidthRequest = 30;
            laddicon.HeightRequest = 30;
            laddicon.Margin = new Thickness(0, 0, 5, 0);
            laddicon.HorizontalOptions = LayoutOptions.EndAndExpand;
            laddicon.Source = "plus.png";

            daddicon = new Image();
            daddicon.WidthRequest = 30;
            daddicon.HeightRequest = 30;
            daddicon.Margin = new Thickness(0, 0, 5, 0);
            daddicon.HorizontalOptions = LayoutOptions.EndAndExpand;
            daddicon.Source = "plus.png";

            breakfaststack = new StackLayout();
            breakfaststack.HeightRequest = 75;
            breakfaststack.HorizontalOptions = LayoutOptions.FillAndExpand;
            breakfaststack.Orientation = StackOrientation.Horizontal;
            breakfaststack.Margin = new Thickness(5);
            breakfaststack.BackgroundColor = Color.FromHex("#E0E0E0");
            breakfasticon = new Image();
            breakfasticon.Source = "toaster.png";
            breakfasticon.Margin = new Thickness(5, 0, 0, 0);
            breakfasticon.HeightRequest = 45;
            breakfasticon.WidthRequest = 45;
            breakfasticon.HorizontalOptions = LayoutOptions.StartAndExpand;
            breakfastlbl = new Label();
            breakfastlbl.HorizontalOptions = LayoutOptions.CenterAndExpand;
            breakfastlbl.VerticalTextAlignment = TextAlignment.Center;
            breakfastlbl.Text = "Add breakfast";
            breakfastlbl.TextColor = Color.Black;
            breakfastlbl.FontSize = 20;
            breakfaststack.Children.Add(breakfasticon);
            breakfaststack.Children.Add(breakfastlbl);
            breakfaststack.Children.Add(baddicon);

            lunchstack = new StackLayout();
            lunchstack.HeightRequest = 75;
            lunchstack.HorizontalOptions = LayoutOptions.FillAndExpand;
            lunchstack.Orientation = StackOrientation.Horizontal;
            lunchstack.Margin = new Thickness(5, 0, 5, 5);
            lunchstack.BackgroundColor = Color.FromHex("#E0E0E0");
            lunchicon = new Image();
            lunchicon.HorizontalOptions = LayoutOptions.StartAndExpand;
            lunchicon.Source = "lunchbox.png";
            lunchicon.Margin = new Thickness(5, 0, 0, 0);
            lunchicon.HeightRequest = 45;
            lunchicon.WidthRequest = 45;
            lunchlbl = new Label();
            lunchlbl.HorizontalOptions = LayoutOptions.CenterAndExpand;
            lunchlbl.VerticalTextAlignment = TextAlignment.Center;
            lunchlbl.Text = "Add lunch";
            lunchlbl.TextColor = Color.Black;
            lunchlbl.FontSize = 20;
            lunchstack.Spacing = 70;
            lunchstack.Children.Add(lunchicon);
            lunchstack.Children.Add(lunchlbl);
            lunchstack.Children.Add(laddicon);

            dinnerstack = new StackLayout();
            dinnerstack.HeightRequest = 75;
            dinnerstack.HorizontalOptions = LayoutOptions.FillAndExpand;
            dinnerstack.Orientation = StackOrientation.Horizontal;
            dinnerstack.Margin = new Thickness(5, 0, 5, 5);
            dinnerstack.BackgroundColor = Color.FromHex("#E0E0E0");
            dinnericon = new Image();
            dinnericon.HorizontalOptions = LayoutOptions.StartAndExpand;
            dinnericon.Source = "dinner.png";
            dinnericon.Margin = new Thickness(5, 0, 0, 0);
            dinnericon.HeightRequest = 45;
            dinnericon.WidthRequest = 45;
            dinnerlbl = new Label();
            dinnerlbl.VerticalTextAlignment = TextAlignment.Center;
            dinnerlbl.Text = "Add dinner";
            dinnerlbl.TextColor = Color.Black;
            dinnerlbl.FontSize = 20;
            dinnerstack.Spacing = 70;
            dinnerstack.Children.Add(dinnericon);
            dinnerstack.Children.Add(dinnerlbl);
            dinnerstack.Children.Add(daddicon);

            finalprogresslayout = new StackLayout();
            finalprogresslayout.Margin = new Thickness(5);
            finalprogresslayout.BackgroundColor = Color.FromHex("#f1f1f1");

            parentStack = new StackLayout();

            finalprogresslayout.Children.Add(progressStack);
            finalprogresslayout.Children.Add(mprogressStack);
            parentStack.Children.Add(finalprogresslayout);
            parentStack.Children.Add(dateselectionlayout);
            parentStack.Children.Add(breakfaststack);
            parentStack.Children.Add(lunchstack);
            parentStack.Children.Add(dinnerstack);

            breakfaststack.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => NavigateToNutritionItemPage(1)) });
            lunchstack.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => NavigateToNutritionItemPage(2)) });
            dinnerstack.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => NavigateToNutritionItemPage(3)) });

            leftarrow.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnLeftArrowClick()) });
            rightarrow.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnRightArrowClick()) });
            finalprogresslayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnParentStackClick()) });

            scrollView.Content = parentStack;

            Content = scrollView;
        }

        private void Datepicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            InitValues();
        }

        private void InitValues()
        {
            //ApiHelper.APICalls.getFoodItemsFromAPI();
            var xfoodItems = new NutritionDBRepository().GetAll().Where(x => x.dateTime == datepicker.Date).ToList();

            CalculateNutrients calculate = new CalculateNutrients();

            double cal = 0;
            double carbs = 0;
            double proteins = 0;
            double fat = 0;
            double bmiValue = 0;

            foreach (var item in xfoodItems)
            {
                cal = cal + (Convert.ToDouble(item.Calories) * item.Quantity);
                carbs = carbs + (Convert.ToDouble(item.Carbohydrates) * item.Quantity);
                proteins = proteins + (Convert.ToDouble(item.Protein) * item.Quantity);
                fat = fat + (Convert.ToDouble(item.Fat) * item.Quantity);
            }

            bmiValue = calculate.CalculateBMI(userProfile.Height, userProfile.Weight);

            double totalcalorie = Convert.ToDouble(calculate.CalculateTotalCalorie(userProfile.Age, userProfile.Weight));

            double totalcarbs = Convert.ToDouble(calculate.CalculateTotalCarbs(Convert.ToString(totalcalorie)));

            double totalfat = Convert.ToDouble(calculate.CalculateTotalFat(Convert.ToString(totalcalorie)));

            double totalprotein = Convert.ToDouble(calculate.CalculateTotalProtein(userProfile.Weight));

            if (totalcarbs > carbs)
            {
                carbInLabel.Text = (totalcarbs - carbs).ToString("0.00") + " remaining";
                carbbar.ProgressColor = Color.FromHex("#4c7e90");
            }
            else if (carbs > totalcarbs)
            {
                carbInLabel.Text = (carbs - totalcarbs).ToString("0.00") + " exceeded";
                carbbar.ProgressColor = Color.Red;
            }
            else if (carbs == totalcarbs)
            {
                carbInLabel.Text = (totalcarbs).ToString("0.00") + " sufficient";
                carbbar.ProgressColor = Color.Green;
            }

            if (totalfat > fat)
            {
                fatInLabel.Text = (totalfat - fat).ToString("0.00") + " remaining";
                fatbar.ProgressColor = Color.FromHex("#4c7e90");
            }
            else if (fat > totalfat)
            {
                fatInLabel.Text = (fat - totalfat).ToString("0.00") + " exceeded";
                fatbar.ProgressColor = Color.Red;
            }
            else if (fat == totalfat)
            {
                fatInLabel.Text = (fat).ToString("0.00") + " sufficient";
                fatbar.ProgressColor = Color.Green;
            }

            if (totalprotein > proteins)
            {
                proteinInLabel.Text = (totalprotein - proteins).ToString("0.00") + " remaining";
                proteinbar.ProgressColor = Color.FromHex("#4c7e90");
            }
            else if (proteins > totalprotein)
            {
                proteinInLabel.Text = (proteins - totalprotein).ToString("0.00") + " exceeded";
                proteinbar.ProgressColor = Color.Red;
            }
            else if (proteins == totalprotein)
            {
                proteinInLabel.Text = (proteins).ToString("0.00") + " sufficient";
                proteinbar.ProgressColor = Color.Green;
            }

            if(totalcalorie > cal)
            {
                progressRing.RingProgressColor = Color.FromHex("#4c7e90");
            }
            else if(cal > totalcalorie)
            {
                progressRing.RingProgressColor = Color.Red;
            }
            else if(cal == totalcalorie)
            {
                progressRing.RingProgressColor = Color.Green;
            }

            progressRing.AnimatedProgress = cal / totalcalorie;
            carbbar.ProgressTo(carbs / totalcarbs, 250, Easing.Linear);
            fatbar.ProgressTo(fat / totalfat, 250, Easing.Linear);
            proteinbar.ProgressTo(proteins / totalprotein, 250, Easing.Linear);
        }

        private void OnRightArrowClick()
        {
            DateTime date = datepicker.Date;
            DateTime dateTime = date.AddDays(1);
            datepicker.Date = dateTime.Date;

            InitValues();
        }

        private void OnLeftArrowClick()
        {
            DateTime date =   datepicker.Date;
            DateTime dateTime = date.AddDays(-1);
            datepicker.Date = dateTime.Date;

            InitValues();
        }

        private void NavigateToNutritionItemPage(int i)
        {
           Navigation.PushAsync(new NutritionItemPage(i, datepicker.Date));
        }

        private void OnParentStackClick()
        {
            Navigation.PushAsync(new NutritionDetailsPage(datepicker.Date));
        }
    }
}
