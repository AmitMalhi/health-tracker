﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.NutritionValues.NutritionHelperClass;
using HealthTrackerApp.Profile.Pages;
using ProgressRingControl.Forms.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.NutritionValues.Pages
{
    class NutritionDetailsPage : BasePage
    {
        private ProgressRing calorieprogressBar, carbRing, fatRing, proteinRing;
        private ScrollView scrollView;
        private StackLayout parentstackLayout, progressStack, carbStack, fatStack, proteinStack;
        private Label caloriesLabel, carbLabel, fatLabel, proteinLabel, BMILabel, statusLabel;
        private string initcalorie, totalcalorie, initcarbs, totalcarbs, initfat, totalfat, initprotein, totalprotein, statusValue;
        private UserProfile userProfile;
        private double bmiValue;
        private DateTime date;

        public NutritionDetailsPage(DateTime date)
        {
            this.Title = "Nutrients Detail";
            this.date = date;
            NavigationPage.SetHasBackButton(this, true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            InitValues();

            InitUI();
        }

        private void InitValues()
        {
            userProfile = new UserProfileDBRepository().GetFirstOrDefault();

            var xfoodItems = new NutritionDBRepository().GetAll().Where(x => x.dateTime == date).ToList();

            CalculateNutrients calculate = new CalculateNutrients();

            double cal = 0;
            double carbs = 0;
            double proteins = 0;
            double fat = 0;
            bmiValue = 0;

            foreach (var item in xfoodItems)
            {
                cal = cal + Convert.ToDouble(item.Calories);
                carbs = carbs + Convert.ToDouble(item.Carbohydrates);
                proteins = proteins + Convert.ToDouble(item.Protein);
                fat = fat + Convert.ToDouble(item.Fat);
            }

            bmiValue = calculate.CalculateBMI(userProfile.Height, userProfile.Weight);

            statusValue = calculate.CalculateStatus(bmiValue);

            initcalorie = cal.ToString();
            totalcalorie = calculate.CalculateTotalCalorie(userProfile.Age, userProfile.Weight);

            initcarbs = carbs.ToString();
            totalcarbs = calculate.CalculateTotalCarbs(totalcalorie);

            initfat = fat.ToString();
            totalfat = calculate.CalculateTotalFat(totalcalorie);

            initprotein = proteins.ToString();
            totalprotein = calculate.CalculateTotalProtein(userProfile.Weight);
        }

        private void InitUI()
        {
            scrollView = new ScrollView();
            scrollView.BackgroundColor = Color.FromHex("#f1f1f1");

            var formattedString = new FormattedString();
            var span = new Span { Text = "BMI: ", FontAttributes = FontAttributes.Bold, ForegroundColor = Color.Black, FontSize = 80};
            formattedString.Spans.Add(span);
            formattedString.Spans.Add(new Span { Text = bmiValue.ToString(), ForegroundColor = Color.FromHex("#4c7e90"), FontAttributes = FontAttributes.Bold, FontSize = 80 });

            BMILabel = new Label();
            BMILabel.FormattedText = formattedString;
            BMILabel.Margin = new Thickness(10, 0, 10, 10);
            BMILabel.HorizontalTextAlignment = TextAlignment.Center;
            BMILabel.FontSize = 80;

            statusLabel = new Label();
            statusLabel.Text = "This is considered " + statusValue;
            statusLabel.TextColor = Color.Black;
            statusLabel.FontSize = 15;
            statusLabel.HorizontalTextAlignment = TextAlignment.Center;
            statusLabel.Margin = new Thickness(0, 0, 0, 10);

            calorieprogressBar = new ProgressRing();
            calorieprogressBar.RingBaseColor = Color.DarkGray;
            calorieprogressBar.RingProgressColor = Color.FromHex("#4c7e90");
            calorieprogressBar.RingThickness = 5;
            calorieprogressBar.Margin = new Thickness(10);
            calorieprogressBar.HeightRequest = 75;
            calorieprogressBar.WidthRequest = 75;
            calorieprogressBar.Progress = 0;
            calorieprogressBar.AnimatedProgress = Convert.ToDouble(initcalorie)/ Convert.ToDouble(totalcalorie);
            calorieprogressBar.StartProgressToAnimation();

            caloriesLabel = new Label();
            caloriesLabel.Text = "Calories : " + initcalorie + " (" + totalcalorie + " max)" + " calorie";
            caloriesLabel.FontSize = 15;
            caloriesLabel.TextColor = Color.Black;
            caloriesLabel.VerticalTextAlignment = TextAlignment.Center;
            caloriesLabel.HorizontalTextAlignment = TextAlignment.Center;

            progressStack = new StackLayout();
            progressStack.Orientation = StackOrientation.Horizontal;
            progressStack.Children.Add(calorieprogressBar);
            progressStack.Children.Add(caloriesLabel);

            carbRing = new ProgressRing();
            carbRing.RingBaseColor = Color.DarkGray;
            carbRing.RingProgressColor = Color.FromHex("#4c7e90");
            carbRing.RingThickness = 5;
            carbRing.Margin = new Thickness(10);
            carbRing.HeightRequest = 75;
            carbRing.WidthRequest = 75;
            carbRing.Progress = 0;
            carbRing.AnimatedProgress = Convert.ToDouble(initcarbs) / Convert.ToDouble(totalcarbs);
            carbRing.StartProgressToAnimation();

            carbLabel = new Label();
            carbLabel.Text = "Carbs : " + initcarbs + " (" + totalcarbs + " max)" + " gm";
            carbLabel.FontSize = 15;
            carbLabel.TextColor = Color.Black;
            carbLabel.VerticalTextAlignment = TextAlignment.Center;
            carbLabel.HorizontalTextAlignment = TextAlignment.Center;

            carbStack = new StackLayout();
            carbStack.Orientation = StackOrientation.Horizontal;
            carbStack.Children.Add(carbRing);
            carbStack.Children.Add(carbLabel);

            fatRing = new ProgressRing();
            fatRing.RingBaseColor = Color.DarkGray;
            fatRing.RingProgressColor = Color.FromHex("#4c7e90");
            fatRing.RingThickness = 5;
            fatRing.Margin = new Thickness(10);
            fatRing.HeightRequest = 75;
            fatRing.WidthRequest = 75;
            fatRing.Progress = 0;
            fatRing.AnimatedProgress = Convert.ToDouble(initfat) / Convert.ToDouble(totalfat);
            fatRing.StartProgressToAnimation();

            fatLabel = new Label();
            fatLabel.Text = "Fat : " + initfat + " (" + totalfat + " max)" + " gm";
            fatLabel.TextColor = Color.Black;
            fatLabel.FontSize = 15;
            fatLabel.VerticalTextAlignment = TextAlignment.Center;
            fatLabel.HorizontalTextAlignment = TextAlignment.Center;

            fatStack = new StackLayout();
            fatStack.Orientation = StackOrientation.Horizontal;
            fatStack.Children.Add(fatRing);
            fatStack.Children.Add(fatLabel);

            proteinRing = new ProgressRing();
            proteinRing.RingBaseColor = Color.DarkGray;
            proteinRing.RingProgressColor = Color.FromHex("#4c7e90");
            proteinRing.RingThickness = 5;
            proteinRing.Margin = new Thickness(10);
            proteinRing.HeightRequest = 75;
            proteinRing.WidthRequest = 75;
            proteinRing.Progress = 0;
            proteinRing.AnimatedProgress = Convert.ToDouble(initprotein) / Convert.ToDouble(totalprotein);
            proteinRing.StartProgressToAnimation();

            proteinLabel = new Label();
            proteinLabel.Text = "Protein : " + initprotein + " (" + totalprotein + " max)" + " gm";
            proteinLabel.TextColor = Color.Black;
            proteinLabel.FontSize = 15;
            proteinLabel.VerticalTextAlignment = TextAlignment.Center;
            proteinLabel.HorizontalTextAlignment = TextAlignment.Center;

            proteinStack = new StackLayout();
            proteinStack.Orientation = StackOrientation.Horizontal;
            proteinStack.Children.Add(proteinRing);
            proteinStack.Children.Add(proteinLabel);

            parentstackLayout = new StackLayout();
            parentstackLayout.Margin = new Thickness(10);

            parentstackLayout.Children.Add(BMILabel);
            parentstackLayout.Children.Add(statusLabel);
            parentstackLayout.Children.Add(progressStack);
            parentstackLayout.Children.Add(carbStack);
            parentstackLayout.Children.Add(proteinStack);
            parentstackLayout.Children.Add(fatStack);

            scrollView.Content = parentstackLayout;

            this.Content = scrollView;
        }
    }
}
