﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.NutritionValues.Models
{
    public class FoodItems
    {
        [PrimaryKey]
        public string xid { get; set; } 
        public int Foodid { get; set; }
        public string Item { get; set; }
        public string Calories { get; set; }
        public string Fat { get; set; }
        public string Carbohydrates { get; set; }
        public string Protein { get; set; }
        public double Quantity { get; set; }
        public DateTime dateTime { get; set; }

        public FoodItems (string xid, string Item, string Calories, string Fat, string Carbohydrates, string Protein)
        {
            this.Item = Item;
            this.Calories = Calories;
            this.Carbohydrates = Carbohydrates;
            this.Fat = Fat;
            this.Protein = Protein;
            this.xid = xid;
            this.Quantity = 0;
        }

        public FoodItems(string xid, string Item, string Calories, string Fat, string Carbohydrates, string Protein, int FoodId, DateTime dateTime, double quantity)
        {
            this.Item = Item;
            this.Calories = Calories;
            this.Carbohydrates = Carbohydrates;
            this.Fat = Fat;
            this.Protein = Protein;
            this.Quantity = quantity;
            this.Foodid = FoodId;
            this.dateTime = dateTime;
            this.xid = xid;
        }

        public FoodItems()
        {

        }
    }
}
