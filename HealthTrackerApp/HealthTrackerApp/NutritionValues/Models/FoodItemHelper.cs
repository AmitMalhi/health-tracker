﻿using Newtonsoft.Json;
using SQLite;
using System.Collections.Generic;

namespace HealthTrackerApp.NutritionValues.Models
{
    public class NutrientsHelper
    {
        [JsonProperty(PropertyName = "rows")]
        public List<FoodItemHelper> foodItemHelper { get; set; }
    }

    public class FoodItemHelper
    {
        public string id { get; set; }
        public string item { get; set; }
        public string calorie { get; set; }
        public string fat { get; set; }
        public string carbs { get; set; }
        public string protein { get; set; }

        public FoodItemHelper(string xid, string Item, string Calories, string Fat, string Carbohydrates, string Protein)
        {
            id = xid;
            item = Item;
            calorie = Calories;
            carbs = Carbohydrates;
            fat = Fat;
            protein = Protein;
        }

        public FoodItemHelper()
        {

        }
    }
}
