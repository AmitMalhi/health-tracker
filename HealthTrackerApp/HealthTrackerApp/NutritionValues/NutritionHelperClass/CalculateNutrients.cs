﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.NutritionValues.NutritionHelperClass
{
    public class CalculateNutrients
    {
        public double CalculateBMI(string height, string weight)
        {
            return Math.Round(Convert.ToDouble(weight) / (Convert.ToDouble(height) / 100 * Convert.ToDouble(height) / 100), 1);
        }

        public string CalculateTotalCarbs(string totalcalorie)
        {
            return (Convert.ToDouble(totalcalorie) / 4).ToString();
        }

        public string CalculateTotalFat(string totalcalorie)
        {
            double calorie = Convert.ToDouble(totalcalorie);

            if (calorie > 0 && calorie < 1500)
            {
                return 50.ToString();
            }
            else if (calorie >= 1500 && calorie < 2500)
            {
                return 67.ToString();
            }
            else if (calorie >= 2500)
            {
                return 83.ToString();
            }
            else
            {
                return null;
            }
        }

        public string CalculateTotalProtein(string weight)
        {
            return (0.8 * Convert.ToInt32(weight)).ToString();
        }

        public string CalculateTotalCalorie(string age, string weight)
        {
            int ageFactor = Convert.ToInt32(age);
            int wt = Convert.ToInt32(weight);

            if (ageFactor > 3 && ageFactor < 9)
            {
                return (22.7 * wt + 495).ToString();
            }
            else if (ageFactor >= 10 && ageFactor < 17)
            {
                return (17.5 * wt + 651).ToString();
            }
            else if (ageFactor >= 18 && ageFactor < 29)
            {
                return (15.3 * wt + 679).ToString();
            }
            else if (ageFactor >= 30 && ageFactor < 60)
            {
                return (11.6 * wt + 879).ToString();
            }
            else if (ageFactor > 60)
            {
                return (13.5 * wt + 487).ToString();
            }
            else
            {
                return null;
            }
        }

        public string CalculateStatus(double bmiValue)
        {
            string statusValue = string.Empty;

            if (bmiValue > 0 && bmiValue <= 18.5)
            {
                statusValue = "underweight";
            }
            else if (bmiValue > 18.5 && bmiValue <= 24.9)
            {
                statusValue = "normal weight";
            }
            else if (bmiValue > 25 && bmiValue <= 29.9)
            {
                statusValue = "overweight";
            }
            else if (bmiValue > 30)
            {
                statusValue = "obesity";
            }

            return statusValue;
        }
    }
}
