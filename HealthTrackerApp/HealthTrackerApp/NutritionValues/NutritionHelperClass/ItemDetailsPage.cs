﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.NutritionValues.Models;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.NutritionValues.NutritionHelperClass
{
    public class ItemDetailsPage : BasePage
    {
        FoodItems foodItems;
        private StackLayout stacklayout;
        private Entry quantity;
        private Label calorie, protein, carbs, fat, item;
        Button save, delete;
        private Image deletefooditem, savefooditem;
        private StackLayout buttonstack;
        private ScrollView scrollView;
        string emptySpace = "  ";

        public ItemDetailsPage(FoodItems foodItems)
        {
            NavigationPage.SetHasBackButton(this, true);
            this.Title = "Item Detail";

            this.foodItems = foodItems;

            stacklayout = new StackLayout();
            quantity = new Entry();
            save = new Button();
            delete = new Button();
            calorie = new Label();
            protein = new Label();
            carbs = new Label();
            fat = new Label();
            item = new Label();
            deletefooditem = new Image();
            savefooditem = new Image();
            scrollView = new ScrollView();
            buttonstack = new StackLayout();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            setupView();
        }

        private void setupView()
        {
            item.Text = emptySpace + foodItems.Item;
            item.TextColor = Color.Black;
            item.FontSize = 20;
            item.BackgroundColor = Color.FromHex("E0E0E0");
            item.HeightRequest = 60;
            item.Margin = new Thickness(8);
            item.HorizontalOptions = LayoutOptions.FillAndExpand;
            item.VerticalTextAlignment = TextAlignment.Center;
            item.HorizontalTextAlignment = TextAlignment.Start;

            quantity.Placeholder = "Quantity";
            quantity.Text = foodItems.Quantity.ToString();
            quantity.TextColor = Color.Black;
            quantity.FontSize = 20;
            quantity.BackgroundColor = Color.FromHex("E0E0E0");
            quantity.HeightRequest = 60;
            quantity.Margin = new Thickness(8, 0, 8, 8);
            quantity.HorizontalOptions = LayoutOptions.FillAndExpand;
            quantity.Keyboard = Keyboard.Numeric;
            quantity.TextChanged += Quantity_TextChanged;

            calorie.Text = emptySpace + "Calories: " + foodItems.Calories + " cal";
            calorie.TextColor = Color.Black;
            calorie.FontSize = 20;
            calorie.BackgroundColor = Color.FromHex("E0E0E0");
            calorie.HeightRequest = 60;
            calorie.Margin = new Thickness(8, 0, 8, 8);
            calorie.HorizontalOptions = LayoutOptions.FillAndExpand;
            calorie.VerticalTextAlignment = TextAlignment.Center;
            calorie.HorizontalTextAlignment = TextAlignment.Start;

            protein.Text = emptySpace + "Protein: " + foodItems.Protein + " gms";
            protein.TextColor = Color.Black;
            protein.FontSize = 20;
            protein.BackgroundColor = Color.FromHex("E0E0E0");
            protein.HeightRequest = 60;
            protein.Margin = new Thickness(8, 0, 8, 8);
            protein.HorizontalOptions = LayoutOptions.FillAndExpand;
            protein.VerticalTextAlignment = TextAlignment.Center;
            protein.HorizontalTextAlignment = TextAlignment.Start;

            carbs.Text = emptySpace + "Carbs: " + foodItems.Carbohydrates + " gms";
            carbs.TextColor = Color.Black;
            carbs.FontSize = 20;
            carbs.BackgroundColor = Color.FromHex("E0E0E0");
            carbs.HeightRequest = 60;
            carbs.Margin = new Thickness(8, 0, 8, 8);
            carbs.HorizontalOptions = LayoutOptions.FillAndExpand;
            carbs.VerticalTextAlignment = TextAlignment.Center;
            carbs.HorizontalTextAlignment = TextAlignment.Start;

            fat.Text = emptySpace + "Fat: " + foodItems.Fat + " gms";
            fat.TextColor = Color.Black;
            fat.FontSize = 20;
            fat.BackgroundColor = Color.FromHex("E0E0E0");
            fat.HeightRequest = 60;
            fat.Margin = new Thickness(8, 0, 8, 8);
            fat.HorizontalOptions = LayoutOptions.FillAndExpand;
            fat.VerticalTextAlignment = TextAlignment.Center;
            fat.HorizontalTextAlignment = TextAlignment.Start;

            save.Text = "SAVE";
            save.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            save.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
            save.VerticalOptions = LayoutOptions.EndAndExpand;
            save.HorizontalOptions = LayoutOptions.FillAndExpand;
            save.FontSize = 18;
            save.Clicked += Save_Clicked;

            delete.Text = "DELETE";
            delete.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            delete.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
            delete.VerticalOptions = LayoutOptions.EndAndExpand;
            delete.HorizontalOptions = LayoutOptions.FillAndExpand;
            delete.FontSize = 18;
            delete.Clicked += Delete_Clicked;

            StackLayout btnStack = new StackLayout();
            btnStack.Children.Add(delete);
            btnStack.Children.Add(save);
            btnStack.HorizontalOptions = LayoutOptions.FillAndExpand;
            btnStack.VerticalOptions = LayoutOptions.EndAndExpand;
            btnStack.Orientation = StackOrientation.Horizontal;
            btnStack.Spacing = 2;

            stacklayout.Children.Add(item);
            stacklayout.Children.Add(quantity);
            stacklayout.Children.Add(calorie);
            stacklayout.Children.Add(protein);
            stacklayout.Children.Add(carbs);
            stacklayout.Children.Add(fat);
            stacklayout.Children.Add(btnStack);

            scrollView.Content = stacklayout;

            Content = scrollView;
        }

        private void Save_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(quantity.Text))
            {


                double qt = Convert.ToDouble(quantity.Text);
                FoodItems fd = new FoodItems(foodItems.xid, foodItems.Item, foodItems.Calories, foodItems.Fat, foodItems.Carbohydrates, foodItems.Protein, foodItems.Foodid, foodItems.dateTime, qt);
                new NutritionDBRepository().InsertOrReplace(fd);
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Alert", "Quantity cannot be empty", "OK");
            }
        }

        private async void Delete_Clicked(object sender, EventArgs e)
        {
            bool deleteitem = await DisplayAlert("Alert", "Are you sure to want to delete?", "Yes", "No");
            if (deleteitem)
            {
                new NutritionDBRepository().Delete(foodItems.xid);
                await Navigation.PopAsync();
            }
        }

        private void Quantity_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (e.NewTextValue != null)
                {
                    if (string.IsNullOrEmpty(quantity.Text))
                    {
                        calorie.Text = "Calories: " + 0 + " cal";
                        protein.Text = "Protein: " + 0 + " gms";
                        carbs.Text = "Carbs: " + 0 + " gms";
                        fat.Text = "Fat: " + 0 + " gms";
                    }
                    else
                    {
                        try
                        {
                            var q = Convert.ToDouble(quantity.Text);

                            calorie.Text = "Calories: " + (Convert.ToDouble(foodItems.Calories) * q) + " cal";
                            protein.Text = "Protein: " + (Convert.ToDouble(foodItems.Protein) * q) + " gms";
                            carbs.Text = "Carbs: " + (Convert.ToDouble(foodItems.Carbohydrates) * q) + " gms";
                            fat.Text = "Fat: " + (Convert.ToDouble(foodItems.Fat) * q) + " gms";
                        }
                        catch (Exception ex)
                        {
                            DisplayAlert("Alert", "please enter the valid inputs", "OK");
                            quantity.Text = "";
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
          
        }
    }
}
