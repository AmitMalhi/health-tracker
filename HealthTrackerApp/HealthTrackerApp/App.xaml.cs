﻿using DLToolkit.Forms.Controls;
using HealthTrackerApp.Login.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.PreferencesUtility;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace HealthTrackerApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            FlowListView.Init();
            //MainPage = new MainPage();
            //NavigationPage navigationPage = new NavigationPage(new LoginPage());
            //MainPage = navigationPage;

            // set the page according to the Auth

            string Auth = UserPreferences.getInstance.GetString("AuthToken");

            if (!string.IsNullOrEmpty(Auth))
            {
                NavigationPage navigationPage = new NavigationPage(new MyMasterDetailPage());
                MainPage = navigationPage;
            }
            else
            {
                NavigationPage navigationPage = new NavigationPage(new LoginPage());
                MainPage = navigationPage;
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
