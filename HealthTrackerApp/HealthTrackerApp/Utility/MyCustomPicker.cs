﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Utility
{
    public class MyCustomPicker : Picker
    {
        public string Tag;

        public MyCustomPicker(string tag)
        {
            this.Tag = tag;
        }
    }
}
