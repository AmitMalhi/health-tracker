﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace HealthTrackerApp.Utility
{
    public class BaseModel
    {
        [PrimaryKey]
         public int id { get; set; }
         public string Title { get; set; }
    }
}
