﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace HealthTrackerApp.Utility
{
   public class Validations
    {
        public const string MatchEmailPattern =
           @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
    + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
    + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
    + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        public static bool IsMailValid(string mail)
        {
            if (mail != null) return Regex.IsMatch(mail, MatchEmailPattern);
            else return false;
        }

        public static bool IsPasswordValid(string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(password))
                {

                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
