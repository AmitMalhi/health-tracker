﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Utility
{
    public class MyUtility
    {
        //Color Code
        public static string MenuBackgroundColor = "#f1f1f1";
        public static string StatusBarBackgroundColor = "#4c7e90";
        public static string StatusBarTextColor = "#fbfbfb";
        public static string MenuTextColor = "#4a4a4a";

        //Reminder Minutes
        public static int reminderTime = 5;
    }
}
