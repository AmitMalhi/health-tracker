﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Utility
{
    public class MySwitch : Switch
    {
        public int Tag;

        public MySwitch(int tag)
        {
            this.Tag = tag;
        }
    }
}
