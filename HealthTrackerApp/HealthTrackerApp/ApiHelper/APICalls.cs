﻿ using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.ImageGallery.Models;
using HealthTrackerApp.NutritionValues.Models;
using HealthTrackerApp.NutritionValues.Pages;
using HealthTrackerApp.Reminders.Models;
using HealthTrackerApp.Reports.Models;
using HealthTrackerApp.Surveys.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HealthTrackerApp.ApiHelper
{
    public class APICalls
    {
        public static List<Dialogue> mDialogueList = new List<Dialogue>();
        public static List<ImageGalleryModel> listImageGallery = new List<ImageGalleryModel>();
        public static List<DocumentGalleryModel> documentList = new List<DocumentGalleryModel>();
        public static List<DialogueOption> mSurveyOption = new List<DialogueOption>();
        public static List<Reminder> reminders = new List<Reminder>();
        public static List<int> pickerList = new List<int>();
        public static List<ReportTabs> reportTabs = new List<ReportTabs>();
        public static List<SurveyFolder> surveyFolderList = new List<SurveyFolder>();
        public static ObservableCollection<FoodItemHelper> foodItems = new ObservableCollection<FoodItemHelper>();

        //  set the private Custructor so that no other class can create the object of this class
        private APICalls()
        {

        }

        // make the property private so that no other class can access it directly and static so that object can created only once
        private static APICalls instance;

        // initialise the instnce only if it is null 
        public static APICalls Instance {

            get
            {
                if (instance == null)
                {
                    instance = new APICalls();
                }
                return instance;
            }
        }
      
        // return true only if username and password are valid
        public bool AuthenticateUser(string username,string pwd)
        {
            if(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(pwd))
            {
                return false;
            }
            return (username.ToLower() == "Gopal@meetappevent.com".ToLower() && pwd.ToLower() == "amit".ToLower());
            
        }

        public static List<Survey> GetSurveyUnderFoler(string group)
        {
            return GetAllSurveys().Where(x => x.GroupName == group).ToList();
        }

        public static List<Survey> GetAllSurveys()
        {
            var mSurveyList = new List<Survey>();
            mSurveyList.Add(new Survey(1990, "Routine Survey", GetDialogues(1990), "Routine Survey"));
            mSurveyList.Add(new Survey(1991,"Explain your Routine Diet", GetDialogues(1991), "Diet Plan"));
            mSurveyList.Add(new Survey(1992,"How about your Phycial activities" ,GetDialogues(1992), "Physical health"));
            mSurveyList.Add(new Survey(1993, "Hows your Stress level",GetDialogues(1993), "Work Activities"));
            mSurveyList.Add(new Survey(1994,"Did you ate properly" ,GetDialogues(1994), "Diet Plan"));
            mSurveyList.Add(new Survey(1995,"Tell your taste buds" ,GetDialogues(1995), "Diet Plan"));
            mSurveyList.Add(new Survey(1996,"Eating best Practices" ,GetDialogues(1996), "Diet Plan"));
            mSurveyList.Add(new Survey(1997,"Workouts and fitness" ,GetDialogues(1997), "Physical health"));
            mSurveyList.Add(new Survey(1998,"Yoga and Meditation" ,GetDialogues(1998), "Physical health"));
            mSurveyList.Add(new Survey(1999, "Document Survey Item", GetDialogues(1998), "File"));

            foreach (var item in mSurveyList)
            {
                var SurveyRepo = new SurveyDBRepository();
                var existingItem = SurveyRepo.Get(x => x.id == item.id);
                if (existingItem!=null)
                {
                    item.IsAnswered = existingItem.IsAnswered;
                }
                new SurveyDBRepository().InsertOrReplace(item);
            }

            var dbSurveyList = new SurveyDBRepository().GetAll();

            return dbSurveyList;
        }

        //Dummy Data
        public static List<Dialogue> GetDialogues(int SurveyId)
        {
            var surveyDialogues= GetAllDialogues().Where(x => x.ParentSurveyId == SurveyId).ToList();
            for (int i = 0; i < surveyDialogues.Count; i++)
            {
                surveyDialogues.ElementAt(i).IndexId = i;
            }
            return surveyDialogues;
        }

        public static List<Dialogue> GetAllDialogues()
        {
            mDialogueList.Clear();

            /**
             * ROutine SUrvey
             */

            mDialogueList.Add(new Dialogue(151, "At what time you had breakfast today.", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/breakfasttiming.jpg?alt=media&token=49dd1e93-cb41-4d21-af2c-392f4c652157", "Having the Meals speacially breakfast at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(152, "What did you had in breakfast?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/breakfast.jpg?alt=media&token=72411ec6-a630-4cef-a10b-6dadadefd7ac", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "MultipleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(153, "At what time you had Lunch today ?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/lunchtime.jpg?alt=media&token=d2bcdd77-1165-4cc6-8749-42bf6f28ceb4", "",
                "DateTimeTypeQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(154, "What you had in Lunch?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/lunch.jpg?alt=media&token=9f784a8f-1409-4f6c-a092-7fbf30750d81", "Having Balanced Diet in Afternoon maintains the Energy Level and digestive balance in your body through out the day",
                "MultipleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(155, "At what time you had Dinner today ?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/dinnertime.png?alt=media&token=d268c9ec-1b5f-4eb9-b983-e23a5dd361ef", "The Ideal time to have dinner is generally before 7 pm as body tend to rejuvinate the body if dinner is at time",
                "DateTimeTypeQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(156, "What did you had in Dinner?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/dinner.jpg?alt=media&token=534473fa-5779-4b2e-867a-c220fd5c42ad", "",
                "MultipleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(157, "How much KM walk you had today?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/walk.jpg?alt=media&token=fb6bc78b-a797-4130-8fd0-7827357c5868", "Physical excercise is equally important as having a balanced diet as it fills your body with oxygen and toxins are thrown out of body through sweating",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(158, "How you are feeling today in terms of health?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/feeling-healthy.jpg?alt=media&token=1e09a67c-5390-4714-8707-ecbb1fc6bd83", "",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(159, "How much glass of water you had today ? ", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/waterglasses.jpg?alt=media&token=ac47ef67-e1a9-4514-8af3-0cc3801353ff", "Consuming plenty amount of water is very important in your routine as it keeps your body Hydrated",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(160, "Did you had any thing apart from your regular diet ?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/snacks.jpg?alt=media&token=98c925d4-eb38-4e41-bfd4-7d16e88d87b9", "",
                "TextAnswer", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(161, "About how many alcoholic drinks do you have today?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/alcohol.jpg?alt=media&token=f5bdbd89-cd56-4ead-aaa1-73c531264d04", "Consuming a lot of alcohol directly effects your Liver making it more fatty.",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(162, "Did you had green tea ?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/Green-Tea-800x416.jpg?alt=media&token=f66c3a91-bc70-4042-892a-1d6b0ad0acc9", "Green Tea is enriched with Anti-oxidents and it throws out the excessive toxins from your body.",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(163, "Please upload picture of your Right hand", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/Righthand.jpg?alt=media&token=7be9a1b8-200d-47d9-bad5-9a9335f03755", "Let's Capture the pictures of different body parts and observe the improvement on regular basis",
                "PictureTypeQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(164, "Please upload picture of your Left Hand", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/Lefthand.png?alt=media&token=b0aa2441-238d-4f84-a804-a9a6d7a18ad6", "",
                "PictureTypeQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(165, "How many Milk teas you had today? ", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/milk-tea-recipe.jpg?alt=media&token=0aab638b-41a6-4168-b171-2776e049d881", "",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(166, "What kind of beverages in whole day?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/mocktail.jpg?alt=media&token=4637f5b4-7f2e-43a0-997e-4784c64c4e17", "",
                "TextAnswer", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(167, "What are the rate of flakes on body? ", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/flakes.jpg?alt=media&token=d4acee63-40ef-422b-9c95-8832ff53b46c", "",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));

            mDialogueList.Add(new Dialogue(168, "What is Emotional Stress level?", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/stress.jpg?alt=media&token=d3726d64-18e9-45a3-b32f-b2aab9b0c86a", "",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));
            mDialogueList.Add(new Dialogue(169, "What is Itching level on Body", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/flakes.jpg?alt=media&token=d4acee63-40ef-422b-9c95-8832ff53b46c", "",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));
            mDialogueList.Add(new Dialogue(170, "Any Infection on body", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/flakes.jpg?alt=media&token=d4acee63-40ef-422b-9c95-8832ff53b46c", "",
                "SingleChoiceQuestion", DateTime.Now, true, 1990));




            /**
             *Survey 1 
             */
            mDialogueList.Add(new Dialogue(101, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1991));
            mDialogueList.Add(new Dialogue(102, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1991));
            mDialogueList.Add(new Dialogue(103, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1991));
            mDialogueList.Add(new Dialogue(104, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1991));
            mDialogueList.Add(new Dialogue(106, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1991));
            mDialogueList.Add(new Dialogue(107, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1991));

            mDialogueList.Add(new Dialogue(108, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1992));
            mDialogueList.Add(new Dialogue(109, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1992));
            mDialogueList.Add(new Dialogue(110, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1992));
            mDialogueList.Add(new Dialogue(111, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1992));
            mDialogueList.Add(new Dialogue(112, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1992));
            mDialogueList.Add(new Dialogue(113, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1992));


            mDialogueList.Add(new Dialogue(114, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1993));
            mDialogueList.Add(new Dialogue(115, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1993));
            mDialogueList.Add(new Dialogue(116, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1993));
            mDialogueList.Add(new Dialogue(117, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1993));
            mDialogueList.Add(new Dialogue(118, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1993));
            mDialogueList.Add(new Dialogue(119, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1993));

            mDialogueList.Add(new Dialogue(120, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1994));
            mDialogueList.Add(new Dialogue(121, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1994));
            mDialogueList.Add(new Dialogue(122, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1994));
            mDialogueList.Add(new Dialogue(123, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1994));
            mDialogueList.Add(new Dialogue(124, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1994));
            mDialogueList.Add(new Dialogue(125, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1994));

            mDialogueList.Add(new Dialogue(126, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1995));
            mDialogueList.Add(new Dialogue(127, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1995));
            mDialogueList.Add(new Dialogue(128, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1995));
            mDialogueList.Add(new Dialogue(129, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1995));
            mDialogueList.Add(new Dialogue(130, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1995));
            mDialogueList.Add(new Dialogue(131, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1995));

            mDialogueList.Add(new Dialogue(132, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1996));
            mDialogueList.Add(new Dialogue(133, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1996));
            mDialogueList.Add(new Dialogue(134, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1996));
            mDialogueList.Add(new Dialogue(135, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1996));
            mDialogueList.Add(new Dialogue(136, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1996));
            mDialogueList.Add(new Dialogue(137, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1996));
            mDialogueList.Add(new Dialogue(174, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
               "DocumentTypeQuestion", DateTime.Now.AddDays(-2), true, 1996));


            mDialogueList.Add(new Dialogue(173, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
               "DocumentTypeQuestion", DateTime.Now.AddDays(-2), true, 1997));
            mDialogueList.Add(new Dialogue(138, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1997));
            mDialogueList.Add(new Dialogue(139, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1997));
            mDialogueList.Add(new Dialogue(140, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1997));
            mDialogueList.Add(new Dialogue(141, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1997));
            mDialogueList.Add(new Dialogue(142, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1997));
            mDialogueList.Add(new Dialogue(143, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1997));

            mDialogueList.Add(new Dialogue(144, "What all did you have in Breakfast today?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "SingleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1998));
            mDialogueList.Add(new Dialogue(145, "Did you consume anything today which is Fried or contains a lot of Oil?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Fried and Oily food may affect your liver and make it fatty and liver is the one which manages your overall health.",
                "MultipleChoiceQuestion", DateTime.Now.AddDays(-2), true, 1998));
            mDialogueList.Add(new Dialogue(146, "What were the things included in your Bed time meal?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "",
                "TextAnswer", DateTime.Now.AddDays(-2), true, 1998));
            mDialogueList.Add(new Dialogue(147, "Did you consume more than 20ml of alcohol in previous 3 days?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome",
                "PictureTypeQuestion", DateTime.Now.AddDays(-2), true, 1998));
            mDialogueList.Add(new Dialogue(148, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
                "DateTimeTypeQuestion", DateTime.Now.AddDays(-2), true, 1998));
            mDialogueList.Add(new Dialogue(172, "At What time did you consumed the Multivitamin Shake prescribed?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "Consuming the Prescribed Multivitamins and Medication at proper time is required as it balances your body's dietry cycle",
               "DocumentTypeQuestion", DateTime.Now.AddDays(-2), true, 1998));
            mDialogueList.Add(new Dialogue(149, "Did you liked the survey ?", "https://meetappdev.blob.core.windows.net/appl1/t_a0a5884c-7538-4b87-880d-f9e337827baf.jpg", "While breakfast is commonly referred to as the most important meal of the day, some epidemiological research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metabolic syndrome Rate it out from 5",
                "RatingTypeQuestion", DateTime.Now.AddDays(-2), true, 1998));

            mDialogueList.Add(new Dialogue(171, "Add Document from local Directory", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/breakfasttiming.jpg?alt=media&token=49dd1e93-cb41-4d21-af2c-392f4c652157", "By this we can upload the document as PDF for survey such that we can reflect it in the Image Gallery Tab", 
                "DocumentTypeQuestion", DateTime.Now, true, 1999));

            return mDialogueList;
        }

        public static List<SurveyFolder> GetSurveyFolder()
        {
            surveyFolderList.Clear();
            surveyFolderList.Add(new SurveyFolder("Survey 1"));
            surveyFolderList.Add(new SurveyFolder("Survey 2"));
            return surveyFolderList;
        }

        // return the list of image to the image gallery
        public static List<ImageGalleryModel> GetImageGalleryModels()
        {
            listImageGallery.Clear();
            listImageGallery.Add(new ImageGalleryModel(1, "Cycling", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/ImageList%2FCycling.jpg?alt=media&token=39151f7b-37ba-4bbc-9346-6aaf5118fa90"));
            listImageGallery.Add(new ImageGalleryModel(2, "Jogging", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/ImageList%2Fjogging.jpg?alt=media&token=baa3c003-9190-433c-8de8-4fb183f98d32"));
            listImageGallery.Add(new ImageGalleryModel(3, "Swimming", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/ImageList%2Fswimming.jpg?alt=media&token=ade3adf6-9bb6-4abf-b4c7-3160afde1685"));
            //listImageGallery.Add(new ImageGalleryModel(4, "Lunch", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/ImageList%2Fimage_0.jpg?alt=media&token=98dc880f-440c-4f03-8233-58447673cc47"));
            //listImageGallery.Add(new ImageGalleryModel(5, "Dinner", "https://firebasestorage.googleapis.com/v0/b/healthtracker-7e0f9.appspot.com/o/ImageList%2Fimg_20181205081702.jpg?alt=media&token=cc3d9687-ffe3-4adb-8002-8900df1f2f0c"));
            return listImageGallery;
        }

        public static List<DocumentGalleryModel> GetDocuments()
        {
            documentList.Clear();
            documentList.Add(new DocumentGalleryModel(1, "Document 1", "https://drive.google.com/file/d/0BxTJVzais59BbFNfbV9QUTFldk0/view?usp=sharing"));
            documentList.Add(new DocumentGalleryModel(2, "Document 2", "https://drive.google.com/file/d/0BxTJVzais59BbFNfbV9QUTFldk0/view?usp=sharing"));
            documentList.Add(new DocumentGalleryModel(3, "Document 3", "https://drive.google.com/file/d/0BxTJVzais59BbFNfbV9QUTFldk0/view?usp=sharing"));
            documentList.Add(new DocumentGalleryModel(4, "Document 4", "https://drive.google.com/file/d/0BxTJVzais59BbFNfbV9QUTFldk0/view?usp=sharing"));
            documentList.Add(new DocumentGalleryModel(5, "Document 5", "https://drive.google.com/file/d/0BxTJVzais59BbFNfbV9QUTFldk0/view?usp=sharing"));
            return documentList;
        }

        public static List<DialogueOption> GetSurveyOptions(int QuestionId)
        {
            mSurveyOption.Clear();
            mSurveyOption.Add(new DialogueOption(1, 101, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(2, 101, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(3, 101, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(4, 101, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(5, 101, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(6, 101, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(7, 101, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(8, 101, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(9, 102, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(10, 102, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(11, 102, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(12, 102, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(13, 102, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(14, 102, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(15, 102, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(16, 102, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(17, 102, false, "Veg Soup", "numeric"));

            mSurveyOption.Add(new DialogueOption(18, 108, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(19, 108, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(20, 108, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(21, 108, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(22, 108, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(23, 108, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(24, 108, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(25, 108, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(26, 109, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(27, 109, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(28, 109, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(29, 109, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(30, 109, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(31, 109, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(32, 109, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(33, 109, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(34, 109, false, "Veg Soup", "numeric"));

            mSurveyOption.Add(new DialogueOption(35, 114, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(36, 114, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(37, 114, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(38, 114, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(39, 114, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(40, 114, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(41, 114, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(42, 114, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(43, 115, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(44, 115, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(45, 115, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(46, 115, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(47, 115, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(48, 115, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(49, 115, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(50, 115, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(51, 115, false, "Veg Soup", "numeric"));

            mSurveyOption.Add(new DialogueOption(52, 120, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(53, 120, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(54, 120, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(55, 120, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(56, 120, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(57, 120, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(58, 120, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(59, 120, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(60, 121, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(61, 121, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(62, 121, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(63, 121, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(64, 121, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(65, 121, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(66, 121, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(67, 121, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(68, 121, false, "Veg Soup", "numeric"));

            mSurveyOption.Add(new DialogueOption(69, 126, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(70, 126, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(71, 126, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(72, 126, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(73, 126, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(74, 126, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(75, 126, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(76, 126, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(77, 127, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(78, 127, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(79, 127, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(80, 127, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(81, 127, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(82, 127, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(83, 127, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(84, 127, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(85, 127, false, "Veg Soup", "numeric"));

            mSurveyOption.Add(new DialogueOption(86, 132, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(87, 132, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(88, 132, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(89, 132, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(90, 132, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(91, 132, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(92, 132, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(93, 132, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(94, 133, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(95, 133, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(96, 133, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(97, 133, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(98, 133, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(99, 133, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(100, 133, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(110, 133, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(111, 133, false, "Veg Soup", "numeric"));

            mSurveyOption.Add(new DialogueOption(112, 138, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(113, 138, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(114, 138, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(115, 138, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(116, 138, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(117, 138, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(118, 138, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(119, 138, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(120, 139, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(121, 139, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(122, 139, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(123, 139, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(124, 139, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(125, 139, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(126, 139, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(127, 139, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(128, 139, false, "Veg Soup", "numeric"));

            mSurveyOption.Add(new DialogueOption(130, 144, false, "Yes", "numeric"));
            mSurveyOption.Add(new DialogueOption(131, 144, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(132, 144, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(133, 144, false, "No", "numeric"));
            mSurveyOption.Add(new DialogueOption(134, 144, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(135, 144, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(136, 144, false, "No", "null"));
            mSurveyOption.Add(new DialogueOption(137, 144, false, "A little like chips or wafers", "numeric"));
            mSurveyOption.Add(new DialogueOption(138, 145, false, "Cornflakes with milk", "null"));
            mSurveyOption.Add(new DialogueOption(139, 145, false, "Oats with milk", "null"));
            mSurveyOption.Add(new DialogueOption(140, 145, false, "Bread with omlette", "null"));
            mSurveyOption.Add(new DialogueOption(141, 145, false, "Stuffed parantha and curd", "null"));
            mSurveyOption.Add(new DialogueOption(142, 145, false, "Egg", "numeric"));
            mSurveyOption.Add(new DialogueOption(143, 145, false, "Boiled Chicken", "null"));
            mSurveyOption.Add(new DialogueOption(144, 145, false, "Coffe with Bread", "numeric"));
            mSurveyOption.Add(new DialogueOption(145, 145, false, "Parantha with tea", "numeric"));
            mSurveyOption.Add(new DialogueOption(146, 145, false, "Veg Soup", "numeric"));


            mSurveyOption.Add(new DialogueOption(150, 152, false, "Bread with Omelette", "numeric"));
            mSurveyOption.Add(new DialogueOption(151, 152, false, "Porridge", "numeric"));
            mSurveyOption.Add(new DialogueOption(152, 152, false, "Poha", "numeric"));
            mSurveyOption.Add(new DialogueOption(153, 152, false, "Kellogs Cornflakes", "numeric"));
            mSurveyOption.Add(new DialogueOption(154, 152, false, "Prantha's", "numeric"));

            mSurveyOption.Add(new DialogueOption(155, 154, false, "Rice with veges", "numeric"));
            mSurveyOption.Add(new DialogueOption(156, 154, false, "Chapati with Veges", "numeric"));
            mSurveyOption.Add(new DialogueOption(157, 154, false, "Pizza", "numeric"));
            mSurveyOption.Add(new DialogueOption(158, 154, false, "Chicken", "numeric"));

            mSurveyOption.Add(new DialogueOption(159, 156, false, "Chicken", "numeric"));
            mSurveyOption.Add(new DialogueOption(160, 156, false, "Rice", "numeric"));
            mSurveyOption.Add(new DialogueOption(161, 156, false, "Chapati", "numeric"));


            mSurveyOption.Add(new DialogueOption(162, 157, false, "5 kms", "null"));
            mSurveyOption.Add(new DialogueOption(163, 157, false, "7kms", "null"));
            mSurveyOption.Add(new DialogueOption(164, 157, false, "10kms", "null"));
            mSurveyOption.Add(new DialogueOption(165, 157, false, "12+ kms", "null"));


            mSurveyOption.Add(new DialogueOption(166, 158, false, "Better", "null"));
            mSurveyOption.Add(new DialogueOption(167, 158, false, "About the Same", "null"));
            mSurveyOption.Add(new DialogueOption(168, 158, false, "Worse", "null"));

            mSurveyOption.Add(new DialogueOption(169, 159, false, "4 Glasses", "null"));
            mSurveyOption.Add(new DialogueOption(170, 159, false, "6 Glasses", "null"));
            mSurveyOption.Add(new DialogueOption(171, 159, false, "8 Glasses", "null"));
            mSurveyOption.Add(new DialogueOption(172, 159, false, "10 Glasses", "null"));

            mSurveyOption.Add(new DialogueOption(173, 159, false, "4 Glasses", "null"));
            mSurveyOption.Add(new DialogueOption(174, 159, false, "6 Glasses", "null"));
            mSurveyOption.Add(new DialogueOption(175, 159, false, "8 Glasses", "null"));
            mSurveyOption.Add(new DialogueOption(176, 159, false, "10 Glasses", "null"));

            mSurveyOption.Add(new DialogueOption(176, 161, false, "0", "null"));
            mSurveyOption.Add(new DialogueOption(177, 161, false, "1-4", "null"));
            mSurveyOption.Add(new DialogueOption(178, 161, false, "5-8", "null"));
            mSurveyOption.Add(new DialogueOption(179, 161, false, "9-12", "null"));
            mSurveyOption.Add(new DialogueOption(180, 161, false, "13-16", "null"));
            mSurveyOption.Add(new DialogueOption(181, 161, false, "More than 16", "null"));

            mSurveyOption.Add(new DialogueOption(182, 162, false, "Yes", "null"));
            mSurveyOption.Add(new DialogueOption(183, 162, false, "No", "null"));

            mSurveyOption.Add(new DialogueOption(184, 165, false, "1", "null"));
            mSurveyOption.Add(new DialogueOption(185, 165, false, "2", "null"));
            mSurveyOption.Add(new DialogueOption(186, 165, false, "3", "null"));

            mSurveyOption.Add(new DialogueOption(187, 167, false, "Better", "null"));
            mSurveyOption.Add(new DialogueOption(188, 167, false, "Good", "null"));
            mSurveyOption.Add(new DialogueOption(189, 167, false, "Normal", "null"));
            mSurveyOption.Add(new DialogueOption(190, 167, false, "Worse", "null"));

            mSurveyOption.Add(new DialogueOption(191, 168, false, "High", "null"));
            mSurveyOption.Add(new DialogueOption(192, 168, false, "Medium", "null"));
            mSurveyOption.Add(new DialogueOption(193, 168, false, "Low", "null"));

            mSurveyOption.Add(new DialogueOption(194, 169, false, "High", "null"));
            mSurveyOption.Add(new DialogueOption(195, 169, false, "Medium", "null"));
            mSurveyOption.Add(new DialogueOption(196, 169, false, "Low", "null"));

            mSurveyOption.Add(new DialogueOption(197, 170, false, "Yes", "null"));
            mSurveyOption.Add(new DialogueOption(198, 170, false, "No", "null"));



            return mSurveyOption.Where(x => x.DialogueId == QuestionId).ToList();
        }

        public static List<Reminder> GetReminders()
        {
            reminders.Clear();
            reminders.Add(new Reminder(1,"Weekly inhaler intake", DateTime.Now, false,false));
            reminders.Add(new Reminder(2,"Doctors follow up", DateTime.Now, false,false));
            reminders.Add(new Reminder(3,"Monnthly Vaccination", DateTime.Now, false, false));
            reminders.Add(new Reminder(4,"Medicine", DateTime.Now, false, false));
            reminders.Add(new Reminder(5,"Blood Pressure", DateTime.Now, false, false)); 
            return reminders;
        }

        public static List<int> GetPickerValue()
        {
            pickerList.Clear();
            for (int i = 1; i <= 20; i++)
            {
                pickerList.Add(i);
            }
            return pickerList;
        }

        public static List<ReportTabs> GetReportTabs()
        {
            reportTabs.Clear();
            reportTabs.Add(new ReportTabs("Last 30 days", "Normal"));
            reportTabs.Add(new ReportTabs("For a certain period", "DateType"));
            return reportTabs;
        }


        public static async Task<NutrientsHelper> getFoodItemsFromAPI()
        {
            HttpClient client = new HttpClient();
            string URL = "http://gsx2json.com/api?id=1mI7X5JkjoSf5m6_8kLnWqWnTyK32lWJAxOiKOWV4cBY";
            var response = await client.GetAsync(string.Format(URL));
            string result = await response.Content.ReadAsStringAsync();
            NutrientsHelper root = JsonConvert.DeserializeObject<NutrientsHelper>(result);

            if (root != null && root.foodItemHelper.Count > 0)
            {
                return root;
            }
            else
            {
                return null;
            }
        }

        //public static ObservableCollection<FoodItemHelper> GetFoodItems()
        //{
        //    foodItems.Clear();

        //    foodItems.Add(new FoodItemHelper("1", "Boiled Egg with yolk", "78", "5", "0.6", "6"));
        //    foodItems.Add(new FoodItemHelper("2", "Boiled Egg without yolk", "17", "0.05", "0.2", "3.6"));
        //    foodItems.Add(new FoodItemHelper("3", "Banana", "105", "0.4", "27", "1.3"));
        //    foodItems.Add(new FoodItemHelper("4", "Apple", "95", "0.3", "25", "0.5"));
        //    foodItems.Add(new FoodItemHelper("5", "Poha", "270", "7.03", "46.05", "5.74"));
        //    foodItems.Add(new FoodItemHelper("6", "Parantha", "230", "8", "37", "2"));
        //    foodItems.Add(new FoodItemHelper("7", "Curd Bowl", "210", "8.82", "7.24", "24.68"));
        //    foodItems.Add(new FoodItemHelper("8", "Butter Tsp", "102", "11.52", "0.01", "0.12"));
        //    foodItems.Add(new FoodItemHelper("9", "Milk Glass", "146", "7.93", "11.03", "7.86"));
        //    foodItems.Add(new FoodItemHelper("10", "Bread", "69", "0.86", "13.16", "1.19"));

        //    foodItems.Add(new FoodItemHelper("11", "Roti", "106", "0.52", "22.32", "3.84"));
        //    foodItems.Add(new FoodItemHelper("12", "Dal Bowl", "198", "6.32", "26.18", "10.36"));
        //    foodItems.Add(new FoodItemHelper("13", "Rajmah Bowl", "240", "7", "32", "12"));
        //    foodItems.Add(new FoodItemHelper("14", "Channa Bowl", "125", "5", "14", "6"));
        //    foodItems.Add(new FoodItemHelper("15", "Kala Channa Bowl", "75", "2", "29", "14"));
        //    foodItems.Add(new FoodItemHelper("16", "Aaloo Sabzi Bowl", "96", "2.89", "17.52", "2.34"));
        //    foodItems.Add(new FoodItemHelper("17", "Sabzi Salad Bowl", "460", "15.5", "70", "12"));
        //    foodItems.Add(new FoodItemHelper("18", "White Rice Bowl", "204", "0.44", "44.08", "4.2"));
        //    foodItems.Add(new FoodItemHelper("19", "Brown Rice Bowl", "215", "1.74", "44.42", "4.99"));
        //    foodItems.Add(new FoodItemHelper("20", "Cooked Rice Bowl", "213", "1.69", "43.67", "4.17"));

        //    foodItems.Add(new FoodItemHelper("21", "Basmati Rice Bowl", "160", "0", "36", "3"));
        //    foodItems.Add(new FoodItemHelper("22", "Rice with Vegetables Bowl", "170", "2.36", "39.92", "4.37"));
        //    foodItems.Add(new FoodItemHelper("23", "Onion Paratha", "260", "11", "36", "5"));
        //    foodItems.Add(new FoodItemHelper("24", "Methi Paratha", "290", "11", "39", "8"));
        //    foodItems.Add(new FoodItemHelper("25", "Aloo Paratha", "310", "12", "45", "5"));
        //    foodItems.Add(new FoodItemHelper("26", "Plain Paratha", "280", "14", "35", "5"));
        //    foodItems.Add(new FoodItemHelper("27", "Onion Paratha", "260", "11", "36", "5"));
        //    foodItems.Add(new FoodItemHelper("28", "Paneer Paratha", "265", "13", "28", "9"));
        //    foodItems.Add(new FoodItemHelper("29", "Dhal Paratha", "200", "5", "34", "8"));
        //    foodItems.Add(new FoodItemHelper("30", "Flakey Paratha", "255", "6", "43", "6"));

        //    foodItems.Add(new FoodItemHelper("31", "1/2 Small Chicken Breast", "164", "6.48", "0", "24.82"));
        //    foodItems.Add(new FoodItemHelper("32", "1/2 Skinless Chicken Breast", "130", "1.46", "0", "27.52"));
        //    foodItems.Add(new FoodItemHelper("33", "1 Medium Grilled Chicken", "147", "8.36", "0", "16.79"));
        //    foodItems.Add(new FoodItemHelper("34", "1 Small Chicken Thigh", "135", "8.54", "0", "13.67"));
        //    foodItems.Add(new FoodItemHelper("35", "1 Chicken Breast (4 oz)", "130", "3", "0", "26"));
        //    foodItems.Add(new FoodItemHelper("36", "100 gm Boneless Skinless Chicken Breasts", "100", "2.5", "0", "20"));
        //    foodItems.Add(new FoodItemHelper("37", "1 Small Chicken Drumstick", "81", "4.2", "0", "10.18"));
        //    foodItems.Add(new FoodItemHelper("38", "1 medium Rotisserie Chicken", "147", "8.36", "0", "16.79"));
        //    foodItems.Add(new FoodItemHelper("39", "1 Cup Roasted Broiled or Baked Chicken", "254", "9.92", "0", "38.73"));
        //    foodItems.Add(new FoodItemHelper("40", "Chicken Meat (3 oz)", "202", "10.11", "0", "25.87"));

        //    foodItems.Add(new FoodItemHelper("41", "1 fillet Baked or Broiled Fish", "142", "3.89", "0.37", "24.79"));
        //    foodItems.Add(new FoodItemHelper("42", "1 fillet Cooked Fish", "127", "1.38", "0", "26.33"));
        //    foodItems.Add(new FoodItemHelper("43", "1 fillet Fried Battered Fish", "267", "15.24", "8.99", "22.41"));
        //    foodItems.Add(new FoodItemHelper("44", "1 fillet Grilled Fish", "123", "1.33", "0.31", "25.53"));
        //    foodItems.Add(new FoodItemHelper("45", "Tilapia (Fish 1 oz)", "27", "0.48", "0", "5.69"));
        //    foodItems.Add(new FoodItemHelper("46", "1 fillet Fried Floured or Breaded Fish", "285", "14.9", "11.05", "25.57"));
        //    foodItems.Add(new FoodItemHelper("47", "Baked or Broiled Cod (4 oz)", "138", "4.07", "0.46", "23.71"));
        //    foodItems.Add(new FoodItemHelper("48", "Salmon (4 oz)", "166", "6.72", "0", "24.52"));
        //    foodItems.Add(new FoodItemHelper("49", "1 large fillet Tilapia", "90", "1.5", "0", "20"));
        //    foodItems.Add(new FoodItemHelper("50", "Fish (1 oz)", "24", "0.26", "0", "5.03"));

        //    return foodItems;
        //}
    } 
}
