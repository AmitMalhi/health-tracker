﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Reports.Models;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HealthTrackerApp.Reports.Pages
{
    public class ReportsPage : TabbedPage
    {
        List<ReportTabs> pages = new List<ReportTabs>();

        public ReportsPage()
        {
            Title = "Reports";

            pages = APICalls.GetReportTabs();

            foreach (var item in pages)
            {
                Children.Add(new WebViewReportPage(item.Title, item.Type));
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
        }
    }
}
