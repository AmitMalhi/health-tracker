﻿using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Utility;
using System;
using Xamarin.Forms;

namespace HealthTrackerApp.Reports.Pages
{
    public class WebViewReportPage : BasePage
    {
        private WebView webView;
        private DatePicker minDatePicker, maxDatePicker;
        private Label minLabel, maxLabel, emptyLabel;
        private StackLayout minPickerStack, maxPickerStack, pickerStack, stackLayout;
        private string Type;
        long lastPress;
        public WebViewReportPage(string title, string type)
        {
            Icon = "diagram.png";
            Title = title;
            this.Type = type;
            webView = new WebView();
            minDatePicker = new DatePicker();
            maxDatePicker = new DatePicker();
            minLabel = new Label();
            maxLabel = new Label();
            emptyLabel = new Label();
            minPickerStack = new StackLayout();
            maxPickerStack = new StackLayout();
            pickerStack = new StackLayout();
            stackLayout = new StackLayout();
            maxDatePicker.DateSelected += MaxDatePicker_DateSelected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            minLabel.Text = "From :";
            minLabel.FontSize = 17;
            minLabel.TextColor = Color.Black;
            minLabel.VerticalTextAlignment = TextAlignment.Center;
            maxLabel.Text = "To :";
            maxLabel.FontSize = 17;
            maxLabel.TextColor = Color.Black;
            maxLabel.VerticalTextAlignment = TextAlignment.Center;

            minPickerStack.Orientation = StackOrientation.Horizontal;
            minPickerStack.Children.Add(minLabel);
            minPickerStack.Children.Add(minDatePicker);

            maxPickerStack.Orientation = StackOrientation.Horizontal;
            maxPickerStack.Children.Add(maxLabel);
            maxPickerStack.Children.Add(maxDatePicker);

            pickerStack.Orientation = StackOrientation.Horizontal;
            pickerStack.HorizontalOptions = LayoutOptions.Center;
            pickerStack.Children.Add(minPickerStack);
            pickerStack.Children.Add(maxPickerStack);
            pickerStack.Spacing = 15;
            if (Device.OS == TargetPlatform.iOS)
            {
                pickerStack.Margin = new Thickness(0, 10, 0, 0);
            }

            emptyLabel.Text = "Select date to load report";
            emptyLabel.HorizontalOptions = LayoutOptions.Center;
            emptyLabel.VerticalOptions = LayoutOptions.Center;

            webView.HorizontalOptions = LayoutOptions.FillAndExpand;
            webView.VerticalOptions = LayoutOptions.FillAndExpand;
            stackLayout.Orientation = StackOrientation.Vertical;

            if (Type == "Normal")
            {
                webView.Source = "http://meetappdev.azurewebsites.net/Notification/ViewResponses?auth=lxEfMYcnVRGiS2wnI%2b8RzNa1DHdO8J1UBBCxexdvhEtG0l4CNjfbP9wKxMppRN8afDp9fFGYeTrGUAL3TnBEtA%3d%3d&Id=30583&participantId=206878&message=";
            }
            else if (Type == "DateType")
            {
                stackLayout.Children.Add(pickerStack);
                webView.Source = "http://meetappdev.azurewebsites.net/Notification/ViewResponses?auth=lxEfMYcnVRGiS2wnI%2b8RzNa1DHdO8J1UBBCxexdvhEtG0l4CNjfbP9wKxMppRN8afDp9fFGYeTrGUAL3TnBEtA%3d%3d&Id=30582&participantId=206878&message=";
            }
            stackLayout.Children.Add(webView);
           
            this.Content = stackLayout;

            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
        }

        private void MaxDatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            if (Type == "DateType")
            {
                var minDate = minDatePicker.Date.ToString();
                var maxDate = maxDatePicker.Date.ToString();

                if (true) //Hit API with mindate and maxdate and get response
                {
                    webView.Source = "http://meetappdev.azurewebsites.net/Notification/ViewResponses?auth=lxEfMYcnVRGiS2wnI%2b8RzNa1DHdO8J1UBBCxexdvhEtG0l4CNjfbP9wKxMppRN8afDp9fFGYeTrGUAL3TnBEtA%3d%3d&Id=30582&participantId=206878&message=";
                }
                else
                {
                    DisplayAlert("Alert", "Something went wrong !", "Camcel");
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {

            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 5000)
            {
                DisplayAlert("", "Press back again to exit", "OK");
                lastPress = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
