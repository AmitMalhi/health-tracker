﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Reports.Models
{
    public class ReportTabs
    {
        public string Title { get; set; }
        public string Type { get; set; }

        public ReportTabs(string Title, string Type)
        {
            this.Title = Title;
            this.Type = Type;
        }
    }
}
