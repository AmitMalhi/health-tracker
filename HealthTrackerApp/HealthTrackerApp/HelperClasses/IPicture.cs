﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.HelperClasses
{
   public interface IPicture
    {
        void SavePictureToDisk(string filename, byte[] imageData);
    }
}
