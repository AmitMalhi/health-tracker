﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.HelperClasses
{
    public interface IShowProgressDialog
    {
        void showDialog(string message);
        void dissmissDialog();
    }
}
