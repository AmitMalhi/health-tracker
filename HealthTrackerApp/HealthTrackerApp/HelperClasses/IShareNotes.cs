﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.HelperClasses
{
    public interface IShareNotes
    {
        void OpenShareIntent(string texttoshare);
    }
}
