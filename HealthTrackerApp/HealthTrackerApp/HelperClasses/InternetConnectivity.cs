﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.HelperClasses
{
    public class InternetConnectivity
    {
        //Checks Internet Connectivity, if there is internet connection return true else false
        public static bool IsInternetConnected
        {
            get
            {
                return CrossConnectivity.Current.IsConnected;
            }
        }
    }
}
