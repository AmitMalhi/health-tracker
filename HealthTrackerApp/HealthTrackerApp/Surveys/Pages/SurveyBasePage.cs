﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace HealthTrackerApp.Surveys.Pages
{
    //----->@Amit
    public class SurveyBasePage : BasePage
    {
        public Image surveyIcon;
        protected Label surveyTitle, surveyDescription;
        protected Button Bckbutton, Nxtbutton;
        protected StackLayout btnStack, hdrStack, hdrDesStack, optionStack, pageStack;
        protected ScrollView scrollView;
        protected ToolbarItem surveyIndex;
        protected string surveyIndexNumber;
        //protected TapGestureRecognizer imageTapGestureRecognizer;
        int totalSurveyItems;

        public SurveyBasePage()
        {
            try
            {
                Title = "Survey";
                NavigationPage.SetBackButtonTitle(this, "");
                totalSurveyItems = SurveyHelperClass.FreshSurveyCount;
                surveyIndex = new ToolbarItem();
                UpdateSurveyCounter();
                surveyIndexNumber = " ";
                ToolbarItems.Add(surveyIndex);
                SetUI();
            }
            catch (System.Exception ex)
            {
               
            }
        }

        //Method to update survey count
        protected void UpdateSurveyCounter()
        {
            surveyIndex.Text = surveyIndexNumber + "/" + totalSurveyItems;
        }

        private void SetUI()
        {
            try
            {
                
                //Survey Icon
                surveyIcon = new Image();
                surveyIcon.HeightRequest = 50;
                surveyIcon.WidthRequest = 50;
                surveyIcon.Margin = new Thickness(0, 0, 5, 0);
                surveyIcon.Source = "survey.png";
                surveyIcon.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnSurveyIconClicked()) });

                // gesture recognizer for the image click
                //imageTapGestureRecognizer = new TapGestureRecognizer();
                //imageTapGestureRecognizer.Tapped += ImageTapGestureRecognizer_Tapped;
                //surveyIcon.GestureRecognizers.Add(imageTapGestureRecognizer);

                //SurveyTitle
                surveyTitle = new Label();
                surveyTitle.FontSize = 20;
                surveyTitle.TextColor = Color.FromHex(MyUtility.MenuTextColor);
                surveyTitle.Margin = new Thickness(5, 0, 0, 0);
                surveyTitle.Text = "What did you have in breakfast today ?";

                //Stack containing Icon and Title
                hdrStack = new StackLayout();
                hdrStack.Orientation = StackOrientation.Horizontal;
                hdrStack.Children.Add(surveyIcon);
                hdrStack.Children.Add(surveyTitle);

                //Survey Description
                surveyDescription = new Label();
                surveyDescription.FontSize = 15;
                surveyDescription.TextColor = Color.FromHex(MyUtility.MenuTextColor);
                surveyDescription.Text = "While breaskfast is refered as the most important meal of the day. Some research indicates that having breakfast high in rapidly available carbohydrates increases the risk of metablic syndrom.";
                surveyDescription.Margin = new Thickness(0, 5, 0, 0);

                //Stack containing headerstack(Icon and Title) and survey description
                hdrDesStack = new StackLayout();
                hdrDesStack.Orientation = StackOrientation.Vertical;
                hdrDesStack.Children.Add(hdrStack);
                hdrDesStack.Children.Add(surveyDescription);
                hdrDesStack.Margin = new Thickness(10, 10, 10, 5);

                //Stack containing options(RadioButtons/CheckBoxes/TextBox/DateTimePicker)
                optionStack = new StackLayout();
                optionStack.Margin = new Thickness(10, 10, 10, 5);

                //Stack containing headerdescriptionstack(Icon + title + Description + option)
                pageStack = new StackLayout();
                pageStack.Children.Add(hdrDesStack);
                pageStack.Children.Add(optionStack);

                //All the content of the page is added to scroll (Icon + title + Description + option)
                scrollView = new ScrollView();
                scrollView.Content = pageStack;

                //Back Button 
                Bckbutton = new Button();
                Bckbutton.Text = "Back";
                Bckbutton.FontSize = 18;
                Bckbutton.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                Bckbutton.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
                Bckbutton.HorizontalOptions = LayoutOptions.FillAndExpand;

                //Next Button
                Nxtbutton = new Button();
                Nxtbutton.Text = "Next";
                Nxtbutton.FontSize = 18;
                Nxtbutton.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                Nxtbutton.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
                Nxtbutton.HorizontalOptions = LayoutOptions.FillAndExpand;

                //Button stack (Back Button + Next Button)
                btnStack = new StackLayout();
                btnStack.Children.Add(Bckbutton);
                btnStack.Children.Add(Nxtbutton);
                btnStack.Orientation = StackOrientation.Horizontal;
                btnStack.HorizontalOptions = LayoutOptions.FillAndExpand;
                btnStack.VerticalOptions = LayoutOptions.EndAndExpand;
            }
            catch (System.Exception ex)
            {
                
            }
        }

        public virtual void OnSurveyIconClicked()
        {
            
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        public void OnAnimationStarted(bool isPopAnimation)
        {
            //throw new NotImplementedException();
        }

        public void OnAnimationFinished(bool isPopAnimation)
        {
            //throw new NotImplementedException();
        }
    }
}
