﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace HealthTrackerApp.Surveys.Pages
{
    public class MainCarouselPage :  CarouselPage
    {
        //public static readonly BindableProperty IsSwippingProperty = BindableProperty.Create(nameof(IsSwipping), typeof(bool), typeof(MainCarouselPage), default(bool));
        private List<Dialogue> mSurveyList;
        private bool ReturnFromOnAppearing;
        public MainCarouselPage(int SurveyId)
        {
            this.Title = "Survey";
            SurveyHelperClass.RefereshDialoguesList(SurveyId);
            //IsSwipping = false;
            Device.BeginInvokeOnMainThread(() =>
            { DependencyService.Get<IShowProgressDialog>().showDialog("Loading..."); });
            

            if (InternetConnectivity.IsInternetConnected)  //Check Internet Connection
            {
                mSurveyList = APICalls.GetDialogues(SurveyId); //API
            }
            else
            {
                DisplayAlert("No Internet", "", "OK");
            }
            MessagingCenter.Subscribe<InitCarauselUI>(this, "SetUI", (message) =>
            {
                OnAppearing();
            });


            MessagingCenter.Subscribe<InitCarauselUI>(this, "SetOnAppearCheck", (message) => {

                ReturnFromOnAppearing = true;
            });

           
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            try
            {


                if (ReturnFromOnAppearing)
                {
                    // ReturnFromOnAppearing = false;
                    return;
                }

                if (mSurveyList != null && mSurveyList.Count != 0)
                {
                    for (int i = 0; i < mSurveyList.Count; i++)
                    {
                        SurveyBasePage contentPage = null;

                        var typeQuestion = mSurveyList.ElementAt(i).SurveyType;

                        var mBindinCtx = mSurveyList.ElementAt(i);

                        if (typeQuestion == "SingleChoiceQuestion")
                        {
                            contentPage = new SingleChoiceQuestion(mSurveyList.ElementAt(i).IndexId, mBindinCtx);
                        }
                        else if (typeQuestion == "MultipleChoiceQuestion")
                        {
                            contentPage = new MultipleChoiceQuestion(mSurveyList.ElementAt(i).IndexId, mBindinCtx);
                        }
                        else if (typeQuestion == "TextAnswer")
                        {
                            contentPage = new TextAnswer(mSurveyList.ElementAt(i).IndexId, mBindinCtx);
                        }
                        else if (typeQuestion == "PictureTypeQuestion")
                        {
                            contentPage = new PictureTypeQuestion(mSurveyList.ElementAt(i).IndexId, mBindinCtx);
                        }
                        else if (typeQuestion == "DateTimeTypeQuestion")
                        {
                            contentPage = new DateTimeTypeQuestion(mSurveyList.ElementAt(i).IndexId, mBindinCtx);
                        }
                        else if (typeQuestion == "RatingTypeQuestion")
                        {
                            contentPage = new RatingTypeQuestion(mSurveyList.ElementAt(i).IndexId, mBindinCtx);
                        }
                        else if (typeQuestion == "DocumentTypeQuestion")
                        {
                            contentPage = new DocumentTypeQuestion(mSurveyList.ElementAt(i).IndexId, mBindinCtx);
                        }
                        Children.Add(contentPage);
                    }
                }
                else
                {
                    DisplayAlert("No Survey Question", "", "OK");
                }

                //Device.BeginInvokeOnMainThread(() =>
                //{ DependencyService.Get<IShowProgressDialog>().dissmissDialog(); });
            }
            catch (Exception ex)
            {

            }
        }

        //public bool IsSwipping
        //{
        //    get { return (bool)GetValue(IsSwippingProperty); }
        //    set { SetValue(IsSwippingProperty, value); }
        //}

    }   
}
