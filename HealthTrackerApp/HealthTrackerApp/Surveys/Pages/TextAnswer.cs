﻿
using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using HealthTrackerApp.Utility;
using Plugin.InputKit.Shared.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using static HealthTrackerApp.Surveys.Models.DialogueOption;

namespace HealthTrackerApp.Surveys.Pages
{
    public class TextAnswer : SurveyBasePage
    {
        private StackLayout stackLayout, editorstack;
        private CustomEditor editor;
        private Dialogue mCurrentSurveyItem;
        private Dialogue dialogue;

        public TextAnswer(int IndexId,Dialogue mModel)
        {
            try
            {
                NavigationPage.SetHasBackButton(this, false);
                surveyIndexNumber = Convert.ToString(IndexId + 1);
                UpdateSurveyCounter();
                this.BindingContext = mModel;

                mCurrentSurveyItem = this.BindingContext as Dialogue;

               dialogue = new DialogueDBRepository().GetAll().Where(x=>x.id==mCurrentSurveyItem.id).FirstOrDefault();
                SetUI();
                BindUI();
            }
            catch (Exception ex)
            {
               
            }
        }

        private void BindUI()
        {
            try
            {
                surveyTitle.SetBinding(Label.TextProperty, "Title");
                surveyDescription.SetBinding(Label.TextProperty, "Description");
                surveyIcon.SetBinding(Image.SourceProperty, "Imageurl");
            }
            catch (Exception)
            {

            }
        }


        private void SetUI()
        {
            try
            {
                double editorHeight = App.Current.MainPage.Height / 1.8;

                editor = new CustomEditor();
                editor.Placeholder = "Write text here...";
                editor.HorizontalOptions = LayoutOptions.FillAndExpand;
                editor.VerticalOptions = LayoutOptions.FillAndExpand;
                editor.HeightRequest = editorHeight;
                editor.BackgroundColor = Color.White;

                editorstack = new StackLayout();
                editorstack.Children.Add(editor);
                editorstack.Padding = new Thickness(1, 1, 1, 1);
                editorstack.Margin = new Thickness(10, 0, 10, 0);
                editorstack.BackgroundColor = Color.Black;

                optionStack.Children.Add(editorstack);

                stackLayout = new StackLayout();
                stackLayout.Children.Add(scrollView);
                stackLayout.Children.Add(btnStack);
                this.Content = stackLayout;

                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    Bckbutton.IsVisible = false;
                else
                    Bckbutton.IsVisible = true;

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))

                    Nxtbutton.Text = "Submit";
                else
                    Nxtbutton.Text = "Next";

                Nxtbutton.Clicked += Nxtbutton_Clicked;
                Bckbutton.Clicked += Bckbutton_Clicked;

                if(dialogue!=null && !string.IsNullOrEmpty(dialogue.TextAnswer))
                editor.Text = dialogue.TextAnswer;
                

                //If Answered
                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                {
                    editor.TextColor = Color.Black;
                    editor.BackgroundColor = Color.FromHex(MyUtility.MenuBackgroundColor);
                    editor.IsEnabled = false;
                }

                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    Nxtbutton.IsVisible = false;
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    DependencyService.Get<IShowProgressDialog>().dissmissDialog();
                }));
            }
            catch (Exception)
            {

            }
        }

        private void Bckbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    return;
                var previousSurveyItem = SurveyHelperClass.GetPreviousSurveyItem(mCurrentSurveyItem.IndexId);
                MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(previousSurveyItem.IndexId);
            }
            catch (Exception ex)
            {
               
            }
        }

        private void Nxtbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if(!string.IsNullOrWhiteSpace(editor.Text) && mCurrentSurveyItem.IsMandatory)
                {
                    SaveAnswerToDatabase();

                    if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                    {
                        //APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered = true;
                        //DisplayAlert("", "Thanks for your feedback", "OK");
                        var obj = APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId);
                        obj.IsAnswered = true;
                        new SurveyDBRepository().Update(obj);
                        DisplayAlert("", "Thanks for your feedback", "OK");

                        if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                        {
                            Nxtbutton.IsVisible = false;
                        }
                    }
                    else
                    {
                        MoveToNextSurveyItem();
                    }
                }
                else
                {
                    DisplayAlert("Alert", "Please enter text", "OK");
                }
            }
            catch (Exception)
            {
               
            }
        }

        private void SaveAnswerToDatabase()
        {
            var textAnswer = editor.Text;
            mCurrentSurveyItem.TextAnswer = textAnswer;

            //Save Reponses to a list for final Submission of result
            if (!SurveyHelperClass.surveyResponseList.surveyResponse.Any(x => x.NotificationId == mCurrentSurveyItem.id))
            {
                SurveyHelperClass.surveyResponseList.surveyResponse.Add(new SurveyDialogueModel(mCurrentSurveyItem.id, null, textAnswer));
            }
            else
            {
                var obj = SurveyHelperClass.surveyResponseList.surveyResponse.FirstOrDefault(x => x.NotificationId == mCurrentSurveyItem.id);
                obj.OptionId = null;
                obj.TextFeedback = textAnswer;
            }

            new DialogueDBRepository().InsertOrReplace(mCurrentSurveyItem);
        }

        private void MoveToNextSurveyItem()
        {
            var nextSurveyItem = SurveyHelperClass.GetNextSurveyItem(mCurrentSurveyItem.IndexId);
            MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(nextSurveyItem.IndexId);
        }

        public override void OnSurveyIconClicked()
        {
            Navigation.PushAsync(new ImageDetailsPage(mCurrentSurveyItem.Imageurl, " "));
        }
    }
}
