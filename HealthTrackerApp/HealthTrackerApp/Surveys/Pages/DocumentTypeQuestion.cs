﻿using Firebase.Storage;
using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HealthTrackerApp.Surveys.Pages
{
    public class DocumentTypeQuestion : SurveyBasePage
    {
        private StackLayout attachStack, mStackLayout, stackLayout, docNameStack;
        private Dialogue mCurrentSurveyItem;
        private Dialogue dialogue;
        private Label attachLabel, docName;
        private Image attachImage, crossImage;
        private byte[] docBytes;
        private string fileName, uploadedDoc;
        private bool isUploaded = false;

        public DocumentTypeQuestion(int IndexId, Dialogue mModel)
        {
            try
            {
                NavigationPage.SetHasBackButton(this, false);

                surveyIndexNumber = Convert.ToString(IndexId + 1);
                UpdateSurveyCounter();
                this.BindingContext = mModel;
                mCurrentSurveyItem = this.BindingContext as Dialogue;

                dialogue = new DialogueDBRepository().GetAll().Where(x => x.id == mCurrentSurveyItem.id).FirstOrDefault();
                
                SetUI();
                
                BindUI();
            }
            catch (Exception ex)
            {

            }
        }
        private void BindUI()
        {
            try
            {
                surveyTitle.SetBinding(Label.TextProperty, "Title");
                surveyDescription.SetBinding(Label.TextProperty, "Description");
                surveyIcon.SetBinding(Image.SourceProperty, "Imageurl");
            }
            catch (Exception)
            {

            }
        }
        private void SetUI()
        {
            attachStack = new StackLayout();
            mStackLayout = new StackLayout();
            attachStack.Orientation = StackOrientation.Vertical;
            attachLabel = new Label();
            attachLabel.Text = "Attach PDF";
            attachLabel.FontSize = 18;
            attachImage = new Image();
            attachImage.Source = "attach.png";
            attachImage.HeightRequest = 30;
            attachImage.WidthRequest = 30;
            attachImage.HorizontalOptions = LayoutOptions.Start;
            attachStack.Margin = new Thickness(10, 20, 0, 0);
            attachStack.Children.Add(attachLabel);
            attachStack.Children.Add(attachImage);
            attachStack.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnAttachClick()) });

            docName = new Label();
            docName.Margin = new Thickness(10, 20, 0, 0);
            docName.FontSize = 20;
            docName.TextColor = Color.Black;

            mStackLayout.Orientation = StackOrientation.Vertical;
            mStackLayout.Spacing = 18;
            mStackLayout.Children.Add(attachStack);

            optionStack.Children.Add(mStackLayout);
            stackLayout = new StackLayout();
            stackLayout.Children.Add(scrollView);
            stackLayout.Children.Add(btnStack);
            this.Content = stackLayout;

            if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                Bckbutton.IsVisible = false;
            else
                Bckbutton.IsVisible = true;

            if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))

                Nxtbutton.Text = "Submit";
            else
                Nxtbutton.Text = "Next";

            Nxtbutton.Text = "UPLOAD";

            Nxtbutton.Clicked += Nxtbutton_Clicked;
            Bckbutton.Clicked += Bckbutton_Clicked;

            if (dialogue != null && !string.IsNullOrEmpty(dialogue.TextAnswer))
            {
                string[] words = dialogue.TextAnswer.Split(' ');
                SetAttachDocUI(words[0]);
                isUploaded = true;
                Nxtbutton.Text = "Next";
            }
            if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
            {
                mStackLayout.IsEnabled = false;
                mStackLayout.Opacity = 0.5f;
                Nxtbutton.Text = "Next";
            }
            if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
            {
                Nxtbutton.IsVisible = false;
            }
        }

        private async void OnAttachClick()
        {
            try
            {
                FileData fileData = await CrossFilePicker.Current.PickFile();

                docBytes = fileData.DataArray;

                if (fileData == null)
                    return; // user canceled file picking

                fileName = fileData.FileName;
                //string contents = System.Text.Encoding.UTF8.GetString(fileData.DataArray);

                SetAttachDocUI(fileData.FileName);
            }
            catch (Exception ex)
            {
                await DisplayAlert("", "Something went wrong !", "Ok");
            }
        }

        private void SetAttachDocUI(string fileName)
        {
            mStackLayout.Children.Remove(attachStack);

            docNameStack = new StackLayout();
            docNameStack.WidthRequest = 250;
            docNameStack.HeightRequest = 250;
            docNameStack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            docNameStack.Orientation = StackOrientation.Horizontal;

            docName.Text = fileName;

            crossImage = new Image();
            crossImage.Source = "cancel.png";
            crossImage.HeightRequest = 20;
            crossImage.HeightRequest = 20;
            crossImage.VerticalOptions = LayoutOptions.Start;
            crossImage.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnCancelClick()) });

            docNameStack.Children.Add(docName);
            docNameStack.Children.Add(crossImage);
            mStackLayout.Children.Add(docNameStack);
        }

        private void OnCancelClick()
        {
            mStackLayout.Children.Remove(docNameStack);
            mStackLayout.Children.Add(attachStack);
            docBytes = null;
        }

        private void Bckbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    return;
                var previousSurveyItem = SurveyHelperClass.GetPreviousSurveyItem(mCurrentSurveyItem.IndexId);
                MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(previousSurveyItem.IndexId);
            }
            catch (Exception ex)
            {

            }
        }

        private async void Nxtbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (!APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                {
                    if(!isUploaded)
                    {
                        Device.BeginInvokeOnMainThread(new Action(() =>
                        {
                            DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
                        }));

                        string doc_name = string.Format("doc_{0}.pdf", DateTime.UtcNow.ToString("yyyyMMddhhmmssfff"));
                        uploadedDoc = await new FirebaseStorage("healthtracker-7e0f9.appspot.com").Child("DocList").Child(doc_name).PutAsync(new MemoryStream(docBytes), new System.Threading.CancellationToken(false), "application/pdf");
                        isUploaded = true;

                        mStackLayout.IsEnabled = false;
                        mStackLayout.Opacity = 0.5f;

                        Device.BeginInvokeOnMainThread(new Action(() =>
                        {
                            DependencyService.Get<IShowProgressDialog>().dissmissDialog();
                        }));

                        if (!string.IsNullOrEmpty(uploadedDoc))
                        {
                            SaveOptionsToDatabase(uploadedDoc);

                            if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                            {
                                var obj = APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId);
                                obj.IsAnswered = true;
                                new SurveyDBRepository().Update(obj);
                                await DisplayAlert("", "Thanks for your feedback", "OK");

                                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                                {
                                    Nxtbutton.IsVisible = false;
                                }
                            }
                            else
                            {
                                Nxtbutton.Text = "Next";
                                MoveToNextSurveyItem();
                            }
                        }
                        else
                        {
                            await DisplayAlert("", "No Attachment", "OK");
                        }
                    }
                    else
                    {
                        Nxtbutton.Text = "Next";
                        MoveToNextSurveyItem();
                    }
                }
                else
                {
                    Nxtbutton.Text = "Next";
                    MoveToNextSurveyItem();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveOptionsToDatabase(string uploadedDocURL)
        {
            mCurrentSurveyItem.TextAnswer = fileName + " " + uploadedDocURL;
            //Save Reponses to a list for final Submission of result
            if (!SurveyHelperClass.surveyResponseList.surveyResponse.Any(x => x.NotificationId == mCurrentSurveyItem.id))
            {
                SurveyHelperClass.surveyResponseList.surveyResponse.Add(new SurveyDialogueModel(mCurrentSurveyItem.id, null, uploadedDocURL));
            }
            else
            {
                var obj = SurveyHelperClass.surveyResponseList.surveyResponse.FirstOrDefault(x => x.NotificationId == mCurrentSurveyItem.id);
                obj.OptionId = null;
                obj.TextFeedback = uploadedDocURL;
            }

            new DialogueDBRepository().InsertOrReplace(mCurrentSurveyItem);
        }

        private void MoveToNextSurveyItem()
        {
            var nextSurveyItem = SurveyHelperClass.GetNextSurveyItem(mCurrentSurveyItem.IndexId);
            MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(nextSurveyItem.IndexId);
        }

        public override void OnSurveyIconClicked()
        {
            Navigation.PushAsync(new ImageDetailsPage(mCurrentSurveyItem.Imageurl, " "));
        }
    }
}
