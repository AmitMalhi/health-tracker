﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Linq;
using Xamarin.Forms;

namespace HealthTrackerApp.Surveys.Pages
{
    public class DateTimeTypeQuestion : SurveyBasePage
    {
        private DatePicker datePicker;
        private TimePicker timePicker;
        private StackLayout datePickerStack, timePickerStack, stackLayout;
        private Image dateImage, timeImage;
        private Dialogue mCurrentSurveyItem;
        private Dialogue dialogue;
        private string date, time;
        public DateTimeTypeQuestion(int IndexId, Dialogue mModel)
        {
            try
            {
                NavigationPage.SetHasBackButton(this, false);

                surveyIndexNumber = Convert.ToString(IndexId + 1);
                UpdateSurveyCounter();
                this.BindingContext = mModel;
                mCurrentSurveyItem = this.BindingContext as Dialogue;

                dialogue = new DialogueDBRepository().GetAll().Where(x => x.id == mCurrentSurveyItem.id).FirstOrDefault();
                
                SetUI();
                BindUI();
            }
            catch (Exception ex)
            {

            }
        }
        private void BindUI()
        {
            try
            {
                surveyTitle.SetBinding(Label.TextProperty, "Title");
                surveyDescription.SetBinding(Label.TextProperty, "Description");
                surveyIcon.SetBinding(Image.SourceProperty, "Imageurl");
            }
            catch (Exception)
            {

            }
        }
        
        private void SetUI()
        {
            try
            {
                datePicker = new DatePicker();
                timePicker = new TimePicker();
                datePickerStack = new StackLayout();
                timePickerStack = new StackLayout();

                dateImage = new Image();
                dateImage.HeightRequest = 30;
                dateImage.Source = "date.png";
                dateImage.HorizontalOptions = LayoutOptions.Start;

                timeImage = new Image();
                timeImage.Source = "Reminder.png";
                timeImage.HeightRequest = 30;
                timeImage.HorizontalOptions = LayoutOptions.Start;

                timePicker.Time = DateTime.Now.TimeOfDay;
                timePicker.Format = "HH:mm";

                datePickerStack.Children.Add(dateImage);
                datePickerStack.Children.Add(datePicker);

                timePickerStack.Children.Add(timeImage);
                timePickerStack.Children.Add(timePicker);

                optionStack.Children.Add(datePickerStack);
                optionStack.Children.Add(timePickerStack);
                optionStack.Spacing = 20;

                stackLayout = new StackLayout();
                stackLayout.Children.Add(scrollView);
                stackLayout.Children.Add(btnStack);
                this.Content = stackLayout;

                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    Bckbutton.IsVisible = false;
                else
                    Bckbutton.IsVisible = true;

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))

                    Nxtbutton.Text = "Submit";
                else
                    Nxtbutton.Text = "Next";

                Nxtbutton.Clicked += Nxtbutton_Clicked;
                Bckbutton.Clicked += Bckbutton_Clicked;

                if (dialogue != null && !string.IsNullOrEmpty(dialogue.TextAnswer))
                {
                    var textansresult = dialogue.TextAnswer;
                    var spaceindex = textansresult.IndexOf(" ");
                    date = textansresult.Substring(0, spaceindex);
                    time = textansresult.Substring(spaceindex + 1);
                    datePicker.Date = Convert.ToDateTime(date);
                    string[] values = time.Split(':');
                    TimeSpan ts = new TimeSpan(Convert.ToInt32(values[0]), Convert.ToInt32(values[1]), 0);
                    timePicker.Time = ts;
                }

                //If Answered
                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                {
                    datePicker.IsEnabled = false;
                    timePicker.IsEnabled = false;
                }
                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    Nxtbutton.IsVisible = false;
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    DependencyService.Get<IShowProgressDialog>().dissmissDialog();
                }));
            }
            catch (Exception ex)
            {

            }
        }

        private void Bckbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    return;
                var previousSurveyItem = SurveyHelperClass.GetPreviousSurveyItem(mCurrentSurveyItem.IndexId);
                MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(previousSurveyItem.IndexId);
            }
            catch (Exception ex)
            {

            }
        }

        private void Nxtbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                SaveDateTimeToDatabase();

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    //APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered = true;
                    //DisplayAlert("", "Thanks for your feedback", "OK");
                    var obj = APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId);
                    obj.IsAnswered = true;
                    new SurveyDBRepository().Update(obj);
                    DisplayAlert("", "Thanks for your feedback", "OK");

                    if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                    {
                        Nxtbutton.IsVisible = false;
                    }
                }
                else
                {
                    MoveToNextSurveyItem();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveDateTimeToDatabase()
        {
            string selectedDateTime = datePicker.Date.ToString("MM/dd/yyyy") + " " + timePicker.Time.Hours + ":" + timePicker.Time.Minutes;
            mCurrentSurveyItem.TextAnswer = selectedDateTime;

            //Save Reponses to a list for final Submission of result
            if (!SurveyHelperClass.surveyResponseList.surveyResponse.Any(x => x.NotificationId == mCurrentSurveyItem.id))
            {
                SurveyHelperClass.surveyResponseList.surveyResponse.Add(new SurveyDialogueModel(mCurrentSurveyItem.id, null, selectedDateTime));
            }
            else
            {
                var obj = SurveyHelperClass.surveyResponseList.surveyResponse.FirstOrDefault(x => x.NotificationId == mCurrentSurveyItem.id);
                obj.OptionId = null;
                obj.TextFeedback = selectedDateTime;
            }

            new DialogueDBRepository().InsertOrReplace(mCurrentSurveyItem);
        }

        private void MoveToNextSurveyItem()
        {
            var nextSurveyItem = SurveyHelperClass.GetNextSurveyItem(mCurrentSurveyItem.IndexId);
            MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(nextSurveyItem.IndexId);
        }

        public override void OnSurveyIconClicked()
        {
            Navigation.PushAsync(new ImageDetailsPage(mCurrentSurveyItem.Imageurl, " "));
        }
    }
}
