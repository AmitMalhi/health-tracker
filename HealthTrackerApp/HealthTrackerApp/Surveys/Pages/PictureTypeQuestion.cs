﻿using Firebase.Storage;
using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using HealthTrackerApp.Utility;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Surveys.Pages
{
    public class PictureTypeQuestion : SurveyBasePage
    {
        StackLayout stackLayout, attachStack, mStackLayout;
        Label attachLabel;
        Image attachImage, showImage, crossImage;
        List<MyImage> imageURL, finalimageURL;
        private const string CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY = "Choose from Gallery";
        private const string CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO = "Take Photo";
        private const string CONST_TAKE_PHOTO_FOLDER_NAME = "Feed_Photos";
        public byte[] imageAsBytes;
        private Dialogue mCurrentSurveyItem;
        private string imageName;
        private string imageUrl;
        private StackLayout mfinalstack, _addimagestack, imageStack;
        private TapGestureRecognizer removephtoGestureRecognizer = new TapGestureRecognizer();
        private List<string> listfirebaseURL;
        private bool uploadcheck = false;
        private List<MyImage> dialogue;
        List<string> dbUrl;

        public PictureTypeQuestion(int IndexId, Dialogue mModel)
        {
            try
            {
                NavigationPage.SetHasBackButton(this, false);

                surveyIndexNumber = Convert.ToString(IndexId + 1);
                UpdateSurveyCounter();
                this.BindingContext = mModel;
                mCurrentSurveyItem = this.BindingContext as Dialogue;

                dialogue = new DialogueImageDBRepository().GetAll().Where(x => x.dialogueId == mCurrentSurveyItem.id).ToList();

                SetUI();
                if(dialogue.Count != 0)
                {
                    Nxtbutton.Text = "Next";
                    uploadcheck = true;
                }
                BindUI();
            }
            catch (Exception ex)
            {

            }
        }
        private void BindUI()
        {
            try
            {
                surveyTitle.SetBinding(Label.TextProperty, "Title");
                surveyDescription.SetBinding(Label.TextProperty, "Description");
                surveyIcon.SetBinding(Image.SourceProperty, "Imageurl");
            }
            catch (Exception ex)
            {

            }
        }


        private void SetUI()
        {
            try
            {
                listfirebaseURL = new List<string>();
                imageURL = new List<MyImage>();
                finalimageURL = new List<MyImage>();
                imageStack = new StackLayout();
                attachStack = new StackLayout();
                mStackLayout = new StackLayout();
                attachStack.Orientation = StackOrientation.Vertical;
                attachLabel = new Label();
                attachLabel.Text = "Attach File";
                attachLabel.FontSize = 18;
                attachImage = new Image();
                attachImage.Source = "attach.png";
                attachImage.HeightRequest = 30;
                attachImage.WidthRequest = 30;
                attachImage.HorizontalOptions = LayoutOptions.Start;
                attachStack.Margin = new Thickness(10, 20, 0, 0);
                attachStack.Children.Add(attachLabel);
                attachStack.Children.Add(attachImage);
                attachStack.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnAttachClick()) });

                mStackLayout.Orientation = StackOrientation.Vertical;
                mStackLayout.Spacing = 18;
                mStackLayout.Children.Add(attachStack);
                mStackLayout.Children.Add(imageStack);
                
                optionStack.Children.Add(mStackLayout);

                var tapGestureRecognizer = new TapGestureRecognizer();
                tapGestureRecognizer.Tapped += OnAttachImageClick;
                attachImage.GestureRecognizers.Add(tapGestureRecognizer);

                stackLayout = new StackLayout();
                stackLayout.Children.Add(scrollView);
                stackLayout.Children.Add(btnStack);
                this.Content = stackLayout;

                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    Bckbutton.IsVisible = false;
                else
                    Bckbutton.IsVisible = true;

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))

                    Nxtbutton.Text = "Submit";
                else
                    Nxtbutton.Text = "Next";

                Nxtbutton.Text = "UPLOAD";
                Nxtbutton.Clicked += Nxtbutton_Clicked;
                Bckbutton.Clicked += Bckbutton_Clicked;

                if (dialogue != null && dialogue.Count>0)
                {
                    ShowAttachedPhotoOnPage(dialogue);
                }
                if(APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                {
                    mStackLayout.IsEnabled = false;
                    mStackLayout.Opacity = 0.5f;
                    Nxtbutton.Text = "Next";
                }
                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    Nxtbutton.IsVisible = false;
                }
            }
            catch (Exception)
            {

            }
        }

        private void OnAttachClick()
        {
            
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    DependencyService.Get<IShowProgressDialog>().dissmissDialog();
                }));
            }
            catch (Exception ex)
            {

            }
        }

        private void Bckbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    return;
                var previousSurveyItem = SurveyHelperClass.GetPreviousSurveyItem(mCurrentSurveyItem.IndexId);
                MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(previousSurveyItem.IndexId);
            }
            catch (Exception ex)
            {

            }
        }

        private async void Nxtbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if(!APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                {
                    if (finalimageURL.Count == 0 && mCurrentSurveyItem.IsMandatory)
                    {
                        await DisplayAlert("Alert", "Please attach an image", "OK");
                    }
                    else
                    {
                        if (!uploadcheck)
                        {
                            Upload();

                            mStackLayout.IsEnabled = false;
                            mStackLayout.Opacity = 0.5f;

                            if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                            {
                                Nxtbutton.Text = "Submit";
                            }
                            else
                            {
                                Nxtbutton.Text = "Next";
                            }

                            SaveImageToDatabase();

                            return;
                        }

                        if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                        {
                            var obj = APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId);
                            obj.IsAnswered = true;
                            new SurveyDBRepository().Update(obj);
                            await DisplayAlert("", "Thanks for your feedback", "OK");

                            if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                            {
                                Nxtbutton.IsVisible = false;
                            }
                        }
                        else
                        {
                            MoveToNextSurveyItem();
                        }
                    }
                }
                else
                {
                    MoveToNextSurveyItem();
                }
            }
            catch (Exception)
            {

            }
        }

        // check the list to upload the image on the firebase 
        private void Upload()
        {
            //int i = 0;

            if (finalimageURL.Count == 0)
            {
                DisplayAlert("Alert", "Please select image", "OK");
            }

            if (!InternetConnectivity.IsInternetConnected)
            {
                DisplayAlert("Alert", "Please Check your Internet connection!", "Ok");
                return;
            }

            foreach (var item in finalimageURL)
            {
                if (item.image == null)
                {
                    DisplayAlert("Alert", "Please enter text or select image", "OK");
                    return;
                }

                else if (item.image != null)
                {
                    UploadImagetoFirebaseAsync(new MemoryStream(item.image));
                }
                else
                {
                    return;
                }
            }
        }

        // Upload the images to the firebase logic 
        private async void UploadImagetoFirebaseAsync(Stream item)
        {
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));

            double date = DateTime.Now.Millisecond;

            string img_name = string.Format("img_{0}.jpg", DateTime.UtcNow.ToString("yyyyMMddhhmmssfff"));

            listfirebaseURL.Add(await new FirebaseStorage("healthtracker-7e0f9.appspot.com").Child("PictureTypeQuestionImage").Child(img_name).PutAsync(item, new System.Threading.CancellationToken(false), "image/jpeg"));

            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));

            if (finalimageURL.Count == listfirebaseURL.Count)
            {
                uploadcheck = true;
            }
        }

        private void SaveImageToDatabase()
        {
            StringBuilder sbUrl = new StringBuilder();
            foreach (var item in listfirebaseURL)
            {
                sbUrl.Append(item);
                if (item != listfirebaseURL.Last())
                {
                    sbUrl.Append(item);
                }
            }

            mCurrentSurveyItem.TextAnswer = sbUrl.ToString();
            //Save to DB
            new DialogueDBRepository().Update(mCurrentSurveyItem);

            //Save Reponses to a list for final Submission of result
            if (!SurveyHelperClass.surveyResponseList.surveyResponse.Any(x => x.NotificationId == mCurrentSurveyItem.id))
            {
                SurveyHelperClass.surveyResponseList.surveyResponse.Add(new SurveyDialogueModel(mCurrentSurveyItem.id, listfirebaseURL, string.Empty));
            }
            else
            {
                var obj = SurveyHelperClass.surveyResponseList.surveyResponse.FirstOrDefault(x => x.NotificationId == mCurrentSurveyItem.id);
                obj.OptionId = listfirebaseURL;
                obj.TextFeedback = string.Empty;
            }

            foreach (var item in finalimageURL)
            {
                MyImage myImage = new MyImage(item.imageid, mCurrentSurveyItem.id, item.image);
                new DialogueImageDBRepository().Save(myImage);
            }
            
        }

        private void MoveToNextSurveyItem()
        {
            var nextSurveyItem = SurveyHelperClass.GetNextSurveyItem(mCurrentSurveyItem.IndexId);
            MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(nextSurveyItem.IndexId);
        }

        private async void OnAttachImageClick(object sender, EventArgs e)
        {
            try
            {
                MediaFile mediaFile = null;
                imageName = "";

                double ht1 = App.Current.MainPage.Height;

                string action = await DisplayActionSheet("Add Photo", "Cancel", null, CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO, CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY);

                if (action != "Cancel")
                {
                    var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                    var status2 = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                    if (status != PermissionStatus.Granted || status2 != PermissionStatus.Granted)
                    {
                        var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                        status = results[Permission.Camera];
                        status2 = results[Permission.Storage];
                    }

                    if (status != PermissionStatus.Granted || status2 != PermissionStatus.Granted)
                        return;
                }

                switch (action)
                {
                    case CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY: //Select Photo from Gallery
                        if (!CrossMedia.Current.IsPickPhotoSupported)
                        {
                            await DisplayActionSheet("", "", "Photo Permission", "Ok");

                            return;
                        }
                        mediaFile = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions { CompressionQuality = 50 , PhotoSize = PhotoSize.Medium });

                        if (mediaFile != null)
                        {
                            //Covert selected image file to bytes
                            imageAsBytes = ConvertToBytes(mediaFile);
                            if (imageAsBytes != null)
                            {
                                imageURL.Clear();
                                double id = DateTime.Now.Ticks;
                                MyImage myImage = new MyImage(id, mCurrentSurveyItem.id, imageAsBytes);
                                imageURL.Add(myImage);
                            }
                            imageName = string.Format("img_{0}_{1}.png", DateTime.UtcNow.ToString("yyyyMMddhhmmss"), mCurrentSurveyItem.id);
                        }
                        else
                            return;
                        break;

                    case CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO: //Take a Photo
                        bool test = await CrossMedia.Current.Initialize();

                        if (test && (!CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsCameraAvailable))
                        {
                            await DisplayActionSheet("", "", "No Camera", "Ok");

                            return;
                        }
                        try
                        {
                            //Take photo from camera
                            mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions { DefaultCamera = CameraDevice.Rear, SaveToAlbum = true, PhotoSize = PhotoSize.Medium, CompressionQuality = 50, Directory = CONST_TAKE_PHOTO_FOLDER_NAME, Name = imageName });

                            if (mediaFile != null)
                            {
                                //Covert selected image file to bytes
                                imageAsBytes = ConvertToBytes(mediaFile);
                                if (imageAsBytes != null)
                                {
                                    imageURL.Clear();
                                    double id = DateTime.Now.Ticks;
                                    MyImage myImage = new MyImage(id, mCurrentSurveyItem.id, imageAsBytes);
                                    imageURL.Add(myImage);
                                }
                            }
                            else
                                return;

                            imageName = string.Format("img_{0}_{1}.png", DateTime.UtcNow.ToString("yyyyMMddhhmmss"), mCurrentSurveyItem.id);

                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }
                        break;

                    default:
                        imageAsBytes = null;
                        break;
                }
                ShowAttachedPhotoOnPage(imageURL);
            }
            catch (Exception ex)
            {

            }
        }

        private void ShowAttachedPhotoOnPage(List<MyImage> _imagelist)
        {
            finalimageURL.AddRange(_imagelist);
            foreach (var item in _imagelist)
            {
                try
                {
                    _addimagestack = new StackLayout();
                    _addimagestack.WidthRequest = 250;
                    _addimagestack.HeightRequest = 250;
                    _addimagestack.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    _addimagestack.Orientation = StackOrientation.Horizontal;

                    showImage = new Image();
                    showImage.Aspect = Aspect.AspectFit;
                    showImage.WidthRequest = 200;
                    showImage.HeightRequest = 200;
                    showImage.HorizontalOptions = LayoutOptions.Center;
                    showImage.Source = ImageSource.FromStream(() => new MemoryStream(item.image));

                    crossImage = new Image();
                    crossImage.Source = "cancel.png";
                    crossImage.HeightRequest = 40;
                    crossImage.HeightRequest = 40;
                    crossImage.VerticalOptions = LayoutOptions.Start;
                    //crossImage.HorizontalOptions = LayoutOptions.Start;
                    crossImage.ClassId = Convert.ToString(item.imageid);
                    // crossImage.Margin = new Thickness(0, 0, 40, 0);


                    _addimagestack.Children.Add(showImage);
                    _addimagestack.Children.Add(crossImage);

                    //add tap gesture to remove image
                    removephtoGestureRecognizer.Tapped += RemoveImageEventGesture;
                    crossImage.GestureRecognizers.Add(removephtoGestureRecognizer);

                    imageStack.Children.Add(_addimagestack);
                }
                catch (Exception ex)
                {

                }
            }

        }

        private void RemoveImageEventGesture(object sender, EventArgs e)
        {
            try
            {
                var crossObj = sender as Image;
                var removalIndex = finalimageURL.FindIndex(x => x.imageid.ToString() == crossObj.ClassId);
                finalimageURL.RemoveAt(removalIndex);
                imageStack.Children.RemoveAt(removalIndex);

            }
            catch (Exception ex)
            {

            }
        }

        //void RemovePhoto(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //Remove image layout
        //        attachStack.Children.Remove(showImage);
        //        attachStack.Children.Remove(crossImage);
        //        imageAsBytes = null;
        //        //add attach button to layout
        //        attachStack.Children.Add(attachLabel);
        //        attachStack.Children.Add(attachImage);
        //        attachStack.Margin = new Thickness(10, 20, 0, 0);
        //        var tapGestureRecognizer = new TapGestureRecognizer();
        //        //tapGestureRecognizer.Tapped += OnAttachStackClick;
        //        //attachStack.GestureRecognizers.Add(tapGestureRecognizer);

        //        Nxtbutton.Text = "UPLOAD";
        //        mCurrentSurveyItem.TextAnswer = string.Empty;
        //        SaveImageToDatabase();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //   }

        public static byte[] ConvertToBytes(MediaFile input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.GetStream().CopyTo(ms);
                input.Dispose();
                return ms.ToArray();
            }
        }

        public override void OnSurveyIconClicked()
        {
            Navigation.PushAsync(new ImageDetailsPage(mCurrentSurveyItem.Imageurl, " "));
        }
    }
}
