﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using static HealthTrackerApp.Surveys.Models.DialogueOption;

namespace HealthTrackerApp.Surveys.Pages
{
    public class RatingTypeQuestion : SurveyBasePage
    {
        private StackLayout sliderstack, stackLayout;
        private Dialogue mCurrentSurveyItem;
        private Slider slider;
        private Label label, minlbl, maxlbl;
        private double StepValue = 1.0;
        private Dialogue dialogue;

        public RatingTypeQuestion(int IndexId, Dialogue mModel)
        {
            try
            {
                NavigationPage.SetHasBackButton(this, false);

                surveyIndexNumber = Convert.ToString(IndexId + 1);
                UpdateSurveyCounter();

                this.BindingContext = mModel;

                mCurrentSurveyItem = this.BindingContext as Dialogue;

                dialogue = new DialogueDBRepository().GetAll().Where(x => x.id == mCurrentSurveyItem.id).FirstOrDefault();

                SetUI();
                BindUI();
            }
            catch (Exception ex)
            {

            }
        }
        private void BindUI()
        {
            try
            {
                surveyTitle.SetBinding(Label.TextProperty, "Title");
                surveyDescription.SetBinding(Label.TextProperty, "Description");
                surveyIcon.SetBinding(Image.SourceProperty, "Imageurl");
            }
            catch (Exception)
            {

            }
        }
        private void SetUI()
        {
            try
            {
                slider = new Slider();
                slider.Maximum = 5.0f;
                slider.Minimum = 1.0f;
                slider.Value = 0.0f;
                slider.MaximumTrackColor = Color.FromHex(MyUtility.MenuTextColor);
                slider.MinimumTrackColor = Color.FromHex(MyUtility.MenuTextColor);
                slider.ThumbColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                slider.Margin = new Thickness(10, 0, 10, 0);
                slider.WidthRequest = Application.Current.MainPage.Width - 80;
                slider.ValueChanged += Slider_ValueChanged;

                minlbl = new Label();
                maxlbl = new Label();
                minlbl.Text = Convert.ToString(slider.Minimum);
                maxlbl.Text = Convert.ToString(slider.Maximum);
                minlbl.TextColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                maxlbl.TextColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);

                sliderstack = new StackLayout();
                sliderstack.Orientation = StackOrientation.Horizontal;
                sliderstack.VerticalOptions = LayoutOptions.FillAndExpand;
                sliderstack.Children.Add(minlbl);
                sliderstack.Children.Add(slider);
                sliderstack.Children.Add(maxlbl);

                label = new Label();
                label.HorizontalOptions = LayoutOptions.Center;
                label.TextColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                label.Text = String.Format("Rating : {0}", slider.Minimum);

                optionStack.Children.Add(sliderstack);
                optionStack.Children.Add(label);
                optionStack.VerticalOptions = LayoutOptions.CenterAndExpand;

                stackLayout = new StackLayout();
                stackLayout.Children.Add(scrollView);
                stackLayout.Children.Add(btnStack);
                this.Content = stackLayout;

                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    Bckbutton.IsVisible = false;
                else
                    Bckbutton.IsVisible = true;

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))

                    Nxtbutton.Text = "Submit";
                else
                    Nxtbutton.Text = "Next";

                Nxtbutton.Clicked += Nxtbutton_Clicked;
                Bckbutton.Clicked += Bckbutton_Clicked;

                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                {
                    slider.Value = Convert.ToDouble(dialogue.TextAnswer);
                    slider.IsEnabled = false;
                }
                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    Nxtbutton.IsVisible = false;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var newStep = Math.Round(e.NewValue / StepValue);
            label.Text = String.Format("Rating : {0}", newStep);
            slider.Value = newStep * StepValue;
        }

        private void Bckbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    return;
                var previousSurveyItem = SurveyHelperClass.GetPreviousSurveyItem(mCurrentSurveyItem.IndexId);
                MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(previousSurveyItem.IndexId);
            }
            catch (Exception ex)
            {

            }
        }

        private void Nxtbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                SaveOptionsToDatabase();

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    //All Responses Answer
                    //var x = new SurveyOptionDBRepository().GetAll();
                    //var y = new SurveyDBRepository().GetAll();

                    //Response to submit but it is answer is adding to list after every next click todo
                    //var z = SurveyHelperClass.surveyResponseList;
                    var obj=APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId);
                    obj.IsAnswered = true;
                    new SurveyDBRepository().Update(obj);
                    DisplayAlert("", "Thanks for your feedback", "OK");

                    if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                    {
                        Nxtbutton.IsVisible = false;
                    }
                }
                else
                {
                    MoveToNextSurveyItem();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SaveOptionsToDatabase()
        {
            var sliderValue = slider.Value;
            mCurrentSurveyItem.TextAnswer = Convert.ToString(sliderValue);

            //Save Reponses to a list for final Submission of result
            if (!SurveyHelperClass.surveyResponseList.surveyResponse.Any(x => x.NotificationId == mCurrentSurveyItem.id))
            {
                SurveyHelperClass.surveyResponseList.surveyResponse.Add(new SurveyDialogueModel(mCurrentSurveyItem.id, null, Convert.ToString(sliderValue)));
            }
            else
            {
                var obj = SurveyHelperClass.surveyResponseList.surveyResponse.FirstOrDefault(x => x.NotificationId == mCurrentSurveyItem.id);
                obj.OptionId = null;
                obj.TextFeedback = Convert.ToString(sliderValue);
            }

            new DialogueDBRepository().InsertOrReplace(mCurrentSurveyItem);
        }
        private void MoveToNextSurveyItem()
        {
            var nextSurveyItem = SurveyHelperClass.GetNextSurveyItem(mCurrentSurveyItem.IndexId);
            MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(nextSurveyItem.IndexId);
        }

        public override void OnSurveyIconClicked()
        {
            Navigation.PushAsync(new ImageDetailsPage(mCurrentSurveyItem.Imageurl, " "));
        }
    }
}
