﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using HealthTrackerApp.Utility;
using Plugin.InputKit.Shared.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static HealthTrackerApp.Surveys.Models.DialogueOption;

namespace HealthTrackerApp.Surveys.Pages
{
    public class MultipleChoiceQuestion : SurveyBasePage
    {
        private StackLayout stackLayout;
        private Dialogue mCurrentSurveyItem;
        private List<DialogueOption> mSurveyOptions;
        private List<string> mResponses = new List<string>();
        private List<int> mPickerValue;
        private MyCustomPicker picker;
        private CheckBox checkBox;
        private List<MyCustomPicker> mPickersList = new List<MyCustomPicker>();
        private Dictionary<string, int> mPickerValues = new Dictionary<string, int>();
        private List<DialogueOption> dialogueOption;

        public MultipleChoiceQuestion(int IndexId,Dialogue mModel)
        {
            try
            {
                NavigationPage.SetHasBackButton(this, false);

                surveyIndexNumber = Convert.ToString(IndexId + 1);
                UpdateSurveyCounter();

                this.BindingContext = mModel;

                mCurrentSurveyItem = this.BindingContext as Dialogue;

                dialogueOption = new SurveyOptionDBRepository().GetAll().Where(x => x.DialogueId == mCurrentSurveyItem.id).ToList();

                SetUI();
                BindUI();
            }
            catch (Exception ex)
            {
               
            }
        }

        private void BindUI()
        {
            try
            {
                surveyTitle.SetBinding(Label.TextProperty, "Title");
                surveyDescription.SetBinding(Label.TextProperty, "Description");
                surveyIcon.SetBinding(Image.SourceProperty, "Imageurl");
            }
            catch (Exception)
            {
                
            }
        }

        private void SetUI()
        {
            try
            {
                mSurveyOptions = new List<DialogueOption>();
                mSurveyOptions = APICalls.GetSurveyOptions(mCurrentSurveyItem.id);
                mPickerValue = APICalls.GetPickerValue();

                StackLayout mStackLayout = new StackLayout();
                mStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;
                mStackLayout.Orientation = StackOrientation.Vertical;

                foreach (var option in mSurveyOptions)
                {
                    StackLayout stackLayout = new StackLayout();
                    stackLayout.Orientation = StackOrientation.Horizontal;

                    checkBox = new CheckBox(option.Title, option.id);
                    checkBox.Type = CheckBox.CheckType.Check;
                    checkBox.TextFontSize = 15;
                    checkBox.WidthRequest = 600;
                    checkBox.BorderColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                    checkBox.Color = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                    checkBox.CheckChanged += CheckBox_CheckChanged;

                    picker = new MyCustomPicker(option.id.ToString());
                    picker.Title = "0";
                    picker.WidthRequest = 40;
                    picker.IsEnabled = false;
                    picker.ItemsSource = mPickerValue;
                    picker.HorizontalOptions = LayoutOptions.End;
                    picker.SelectedIndexChanged += PickerValueChanged;
                    mPickersList.Add(picker);

                    Image image = new Image();
                    image.Source = "caretdown.png";

                    StackLayout pickerStack = new StackLayout();
                    pickerStack.Orientation = StackOrientation.Horizontal;
                    pickerStack.Children.Add(picker);
                    pickerStack.Children.Add(image);

                    stackLayout.Children.Add(checkBox);
                    if (option.Type == "numeric")
                    {
                        stackLayout.Children.Add(pickerStack);
                    }

                    if (dialogueOption!=null &&  dialogueOption.Count != 0 && dialogueOption.Exists(x => x.id == checkBox.Key))
                    {
                        checkBox.IsChecked = true;
                        mResponses.Add(checkBox.Key.ToString());
                        mPickersList.FirstOrDefault(x => x.Tag == checkBox.Key.ToString()).SetValue(Picker.SelectedItemProperty,dialogueOption.FirstOrDefault(x=>x.id==checkBox.Key).CountValue);
                    }


                    //If answered
                    if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                    {
                        checkBox.IsEnabled = false;
                        picker.IsEnabled = false;
                    }

                    mStackLayout.Children.Add(stackLayout);
                    optionStack.Orientation = StackOrientation.Horizontal;
                    optionStack.Children.Add(mStackLayout);
                }


                stackLayout = new StackLayout();
                stackLayout.Children.Add(scrollView);
                stackLayout.Children.Add(btnStack);
                this.Content = stackLayout;

                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    Bckbutton.IsVisible = false;
                else
                    Bckbutton.IsVisible = true;

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))

                    Nxtbutton.Text = "Submit";
                else
                    Nxtbutton.Text = "Next";

                Nxtbutton.Clicked += Nxtbutton_Clicked;
                Bckbutton.Clicked += Bckbutton_Clicked;

                if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    Nxtbutton.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
               
            }
        }

        private void PickerValueChanged(object sender, EventArgs e)
        {
            var myPicker = sender as MyCustomPicker;
            if (!mPickerValues.ContainsKey(myPicker.Tag))
                mPickerValues.Add(myPicker.Tag, Convert.ToInt32(myPicker.SelectedItem));
            else
                mPickerValues[myPicker.Tag] = Convert.ToInt32(myPicker.SelectedItem);
        }

        private void CheckBox_CheckChanged(object sender, EventArgs e)
        {
            var checkBox = sender as CheckBox;
            mPickersList.Where(x => x.Tag == checkBox.Key.ToString()).FirstOrDefault().IsEnabled = checkBox.IsChecked;
            if (checkBox.IsChecked)
            {
                mResponses.Add(checkBox.Key.ToString());
            }
            else
            {
                mResponses.Remove(checkBox.Key.ToString());
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    DependencyService.Get<IShowProgressDialog>().dissmissDialog();
                }));
            }
            catch (Exception ex)
            {
                
            }
           
        }

        private void Bckbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    return;
                var previousSurveyItem = SurveyHelperClass.GetPreviousSurveyItem(mCurrentSurveyItem.IndexId);
                MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(previousSurveyItem.IndexId);
            }
            catch (Exception ex)
            {
                
            }
        }

        private void Nxtbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (mResponses.Count > 0)
                {
                    SaveOptionsToDatabase();

                    if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                    {
                        //APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered = true;
                        //DisplayAlert("", "Thanks for your feedback", "OK");
                        var obj = APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId);
                        obj.IsAnswered = true;
                        new SurveyDBRepository().Update(obj);
                        DisplayAlert("", "Thanks for your feedback", "OK");

                        if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                        {
                            Nxtbutton.IsVisible = false;
                        }
                    }
                    else
                    {
                        MoveToNextSurveyItem();
                    }
                }
                else
                {
                    DisplayAlert("Alert", "Please select any option", "OK");
                }
            }
            catch (Exception)
            {

            }
        }
        private void SaveOptionsToDatabase()
        {

            List<string> mMultiOptions = new List<string>();
            foreach (var optionItem in mResponses)
            {

                var OptionCount = mPickerValues.ContainsKey(optionItem) ? mPickerValues[optionItem] : 0;
                var mSurveyOption = new DialogueOption(Convert.ToInt32(optionItem), mCurrentSurveyItem.id, true, mSurveyOptions.Where(x => x.id == Convert.ToInt32(optionItem)).ToList().FirstOrDefault().Title, mSurveyOptions.Where(x => x.id == Convert.ToInt32(optionItem)).ToList().FirstOrDefault().Type, OptionCount);

                //Save Reponses to a list for final Submission of result
                mMultiOptions.Add(optionItem);
                new SurveyOptionDBRepository().InsertOrReplace(mSurveyOption);
            }

           
            if (!SurveyHelperClass.surveyResponseList.surveyResponse.Any(x => x.NotificationId == mCurrentSurveyItem.id))
            {
                SurveyHelperClass.surveyResponseList.surveyResponse.Add(new SurveyDialogueModel(mCurrentSurveyItem.id, mMultiOptions, string.Empty));
            }
            else
            {
                var obj=SurveyHelperClass.surveyResponseList.surveyResponse.FirstOrDefault(x => x.NotificationId == mCurrentSurveyItem.id);
                obj.OptionId = mMultiOptions;
                obj.TextFeedback = string.Empty;
            }
        }
        private void MoveToNextSurveyItem()
        {
            var nextSurveyItem = SurveyHelperClass.GetNextSurveyItem(mCurrentSurveyItem.IndexId);
            MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(nextSurveyItem.IndexId);
        }

        public override void OnSurveyIconClicked()
        {
            Navigation.PushAsync(new ImageDetailsPage(mCurrentSurveyItem.Imageurl, " "));
        }
    }
}
