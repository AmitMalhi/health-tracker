﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HealthTrackerApp.Surveys.Pages
{
    public class SurveyFolderPage : BasePage
    {
        private ListView surveyListView;
        private List<Survey> mSurveyList;
        public static MainCarouselPage surveyCarauselPage;

        public SurveyFolderPage(List<Survey> mSurveyList)
        {
            this.Title = mSurveyList.FirstOrDefault().GroupName;
            this.mSurveyList = mSurveyList;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            #region ListView
            surveyListView = new ListView
            {
                BackgroundColor = Color.Transparent,
                ItemsSource = mSurveyList,
                ItemTemplate = new DataTemplate(() =>
                {
                    var label = new Label();
                    label.SetBinding(Label.TextProperty, "Title");
                    label.FontSize = 19;
                    label.TextColor = Color.Black;
                    label.Margin = new Thickness(0, 10, 10, 10);

                    var image = new Image();
                    image.Source = "survey.png";
                    image.HeightRequest = 40;
                    image.WidthRequest = 40;
                    image.Margin = new Thickness(10, 10, 10, 10);

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                                image,
                                label
                            }
                        }
                    };
                })
            };
            #endregion ListView

            surveyListView.ItemSelected += SurveyListView_ItemSelected;

            this.Content = surveyListView;
        }

        private void SurveyListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                
                    Device.BeginInvokeOnMainThread(async() =>
                    {
                        var TappedSurvey = e.SelectedItem as Survey;
                      await Task.Run( () => {
                          MenuPage.surveyCarauselPage = new MainCarouselPage(TappedSurvey.id);
                          MessagingCenter.Send(new InitCarauselUI(), "SetUI");
                      });

                      await  Navigation.PushAsync(MenuPage.surveyCarauselPage, false);

                    });
               
            }
            catch (Exception ex)
            {

            }
        }
    }
}
