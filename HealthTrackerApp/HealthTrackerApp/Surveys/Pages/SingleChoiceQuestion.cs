﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Models;
using HealthTrackerApp.Utility;
using Plugin.InputKit.Shared.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace HealthTrackerApp.Surveys.Pages
{
    public class SingleChoiceQuestion : SurveyBasePage
    {
        private StackLayout stackLayout;
        private RadioButtonGroupView radioButtonGroupView;
        private Dialogue mCurrentSurveyItem;
        private List<DialogueOption> mSurveyOptions;
        private List<int> mPickerValue;
        private RadioButton radioButton;
        private MyCustomPicker picker;
        private Dictionary<string, int> mPickerValues = new Dictionary<string, int>();
        private List<MyCustomPicker> mPickersList = new List<MyCustomPicker>();
        private List<DialogueOption> dialogueOption;

        public SingleChoiceQuestion(int indexId,Dialogue mModel)
        {
            try
            {
                NavigationPage.SetHasBackButton(this, false);
                surveyIndexNumber = Convert.ToString(indexId + 1);
                UpdateSurveyCounter();
                this.BindingContext = mModel;
                mCurrentSurveyItem = this.BindingContext as Dialogue;
                dialogueOption = new SurveyOptionDBRepository().GetAll().Where(x=>x.DialogueId == mCurrentSurveyItem.id).ToList();
                
                SetUI();
                BindUI();
            }
            catch (Exception)
            {

            }
        }

        private void SetUI()
        {
            try
            {
                mSurveyOptions = new List<DialogueOption>();
                mSurveyOptions = APICalls.GetSurveyOptions(mCurrentSurveyItem.id);
                mPickerValue = APICalls.GetPickerValue();
                
                radioButtonGroupView = new RadioButtonGroupView();
                radioButtonGroupView.SelectedItemChanged += RadioGroupValueChanged;

                StackLayout mStackLayout = new StackLayout();
                mStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;
                mStackLayout.Orientation = StackOrientation.Vertical;
              
                

                foreach (var option in mSurveyOptions)
                {
                    //Radio Button UI
                    radioButton = new RadioButton();
                    radioButton.Text = option.Title;
                    radioButton.TextFontSize = 15;
                    radioButton.CircleColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                    radioButton.Color = Color.FromHex(MyUtility.StatusBarBackgroundColor);
                    radioButton.WidthRequest = 600;
                    radioButtonGroupView.Spacing = 20;
                    radioButtonGroupView.Children.Add(radioButton);

                    picker = new MyCustomPicker(option.id.ToString());
                    picker.Title = "0";
                    picker.WidthRequest = 40;
                    picker.ItemsSource = mPickerValue;
                    picker.HorizontalOptions = LayoutOptions.End;
                    picker.IsEnabled = false;
                    picker.SelectedIndexChanged += PickerValueChanged;
                    mPickersList.Add(picker);

                    Image image = new Image();
                    image.Source = "caretdown.png";

                    StackLayout pickerStack = new StackLayout();
                    pickerStack.Orientation = StackOrientation.Horizontal;
                    pickerStack.Children.Add(picker);
                    pickerStack.Children.Add(image);
                    pickerStack.HeightRequest = 50;
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        pickerStack.HeightRequest = 40;
                    }

                    StackLayout emptyStack = new StackLayout();
                    emptyStack.BackgroundColor = Color.Transparent;
                    emptyStack.HeightRequest = 50;
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        emptyStack.HeightRequest = 40;
                    }

                    if (option.Type == "numeric")
                    {
                        mStackLayout.Children.Add(pickerStack);
                    }
                    else if(option.Type == "null")
                    {
                        mStackLayout.Children.Add(emptyStack);
                    }

                    //If Answered
                    if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered)
                    {
                        radioButton.IsEnabled = false;
                        picker.IsEnabled = false;
                    }
                }

                if (dialogueOption.Count != 0)
                {
                    var OptionIndex = mSurveyOptions.FindIndex(x => x.id == dialogueOption.FirstOrDefault().id);
                    radioButtonGroupView.SelectedIndex = OptionIndex;
                    if(dialogueOption.FirstOrDefault().Type == "numeric")
                    {
                        mPickersList.ElementAt(OptionIndex).SetValue(Picker.SelectedItemProperty, dialogueOption.FirstOrDefault().CountValue);
                    }
                }

                //Add radio buttons to option Stack
                optionStack.Orientation = StackOrientation.Horizontal;
                optionStack.Children.Add(radioButtonGroupView);
                optionStack.Children.Add(mStackLayout);

                //Add scrollview and buttonstack to the page
                stackLayout = new StackLayout();
                stackLayout.Children.Add(scrollView);
                stackLayout.Children.Add(btnStack);
                this.Content = stackLayout;

                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    Bckbutton.IsVisible = false;
                else
                    Bckbutton.IsVisible = true;

                if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))

                    Nxtbutton.Text = "Submit";
                else
                    Nxtbutton.Text = "Next";

                if(APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                {
                    Nxtbutton.IsVisible = false;
                }

                Nxtbutton.Clicked += Nxtbutton_Clicked;
                Bckbutton.Clicked += Bckbutton_Clicked;

            }
            catch (Exception ex)
            {

            }
        }

        private void RadioGroupValueChanged(object sender, EventArgs e)
        {
            var groupView = sender as RadioButtonGroupView;
            var selectedId = mSurveyOptions.ElementAt(groupView.SelectedIndex).id.ToString();

            foreach(var pickerItem in mPickersList)
            {
                if (pickerItem.Tag == selectedId)
                    pickerItem.IsEnabled = true;
                else
                    pickerItem.IsEnabled = false;
            }
        }

        private void PickerValueChanged(object sender, EventArgs e)
        {
            var myPicker = sender as MyCustomPicker;
            if (!mPickerValues.ContainsKey(myPicker.Tag))
                mPickerValues.Add(myPicker.Tag, Convert.ToInt32(myPicker.SelectedItem));
            else
                mPickerValues[myPicker.Tag] = Convert.ToInt32(myPicker.SelectedItem);
        }

        private void BindUI()
        {
            try
            {
                surveyTitle.SetBinding(Label.TextProperty, "Title");
                surveyDescription.SetBinding(Label.TextProperty, "Description");
                surveyIcon.SetBinding(Image.SourceProperty, "Imageurl");
            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    DependencyService.Get<IShowProgressDialog>().dissmissDialog();
                }));
            }
            catch (Exception ex)
            {

            }
        }

        private void Bckbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (SurveyHelperClass.IsFirstSurveyItem(mCurrentSurveyItem.IndexId))
                    return;
                var previousSurveyItem = SurveyHelperClass.GetPreviousSurveyItem(mCurrentSurveyItem.IndexId);
                MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(previousSurveyItem.IndexId);
            }
            catch (Exception ex)
            {
                
            }
        }

        private void Nxtbutton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                if (radioButtonGroupView.SelectedIndex != -1 && mCurrentSurveyItem.IsMandatory)
                {
                    SaveOptionToDatabase();

                    if (SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                    {
                        var obj = APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId);
                        obj.IsAnswered = true;
                        new SurveyDBRepository().Update(obj);
                        DisplayAlert("", "Thanks for your feedback", "OK");

                        if (APICalls.GetAllSurveys().FirstOrDefault(x => x.id == mCurrentSurveyItem.ParentSurveyId).IsAnswered && SurveyHelperClass.IsLastSurveyItem(mCurrentSurveyItem.IndexId))
                        {
                            Nxtbutton.IsVisible = false;
                        }
                    }
                    else
                    {
                        MoveToNextSurveyItem();
                    }
                }
                else
                {
                    DisplayAlert("Alert", "Please select any option", "OK");
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void SaveOptionToDatabase()
        {
            var selectedOption = mSurveyOptions.ElementAt(radioButtonGroupView.SelectedIndex);
            var selectedOptionCount = mPickerValues.ContainsKey(selectedOption.id.ToString())? mPickerValues[selectedOption.id.ToString()]:0;
            var surveyOption = new DialogueOption(selectedOption.id, mCurrentSurveyItem.id, true, selectedOption.Title, selectedOption.Type, selectedOptionCount);

            //Save Reponses to a list for final Submission of result
            if (!SurveyHelperClass.surveyResponseList.surveyResponse.Any(x => x.NotificationId == mCurrentSurveyItem.id))
            {
                SurveyHelperClass.surveyResponseList.surveyResponse.Add(new SurveyDialogueModel(mCurrentSurveyItem.id, new List<string> { selectedOption.id.ToString() }, string.Empty));
            }
            else
            {
                var obj = SurveyHelperClass.surveyResponseList.surveyResponse.FirstOrDefault(x => x.NotificationId == mCurrentSurveyItem.id);
                obj.OptionId = new List<string> { selectedOption.id.ToString() };
                obj.TextFeedback = string.Empty;
            }

            new SurveyOptionDBRepository().InsertOrReplace(surveyOption);
        }

        private void MoveToNextSurveyItem()
        {
            var nextSurveyItem = SurveyHelperClass.GetNextSurveyItem(mCurrentSurveyItem.IndexId);
            MenuPage.surveyCarauselPage.CurrentPage = MenuPage.surveyCarauselPage.Children.ToList().ElementAt(nextSurveyItem.IndexId);
        }

        public override void OnSurveyIconClicked()
        {

            Navigation.PushAsync(new ImageDetailsPage(mCurrentSurveyItem.Imageurl, " "));
        }
    }
}
