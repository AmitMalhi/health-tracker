﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HealthTrackerApp.Surveys.Models.DialogueOption;

namespace HealthTrackerApp.Surveys
{
    //----->@Amit
    public class SurveyHelperClass
    {
        private static List<Dialogue> mDialogueList;
        public static SurveyResponseList surveyResponseList = new SurveyResponseList();

        public static void RefereshDialoguesList(int SurveyId)
        {
            mDialogueList = APICalls.GetDialogues(SurveyId);
        }

        public static int FreshSurveyCount
        {
            get
            {

                return mDialogueList.Count;
            }
        }
        //Property to return First Survey Item from the list
        public static Dialogue FirstSurveyItem
        {
            get
            {
                return mDialogueList.FirstOrDefault();
            }
        }

        //Method to return next survey item
        public static Dialogue GetNextSurveyItem(int index)
        {
            return mDialogueList.ElementAt(index+1);
        }

        //Method to return previous survey item
        public static Dialogue GetPreviousSurveyItem(int index)
        {
            return mDialogueList.ElementAt(index - 1);
        }

        //Method to check is last survey item
        public static bool IsLastSurveyItem(int index) {
            if (index == (mDialogueList.Count-1))
                return true;
            return false;
        }

        //Method to check is first survey item
        public static bool IsFirstSurveyItem(int index)
        {
            if (index == 0)
                return true;
            return false;
        }

        //Method to save responses
        //public static void SaveResponse(int notificationId, string type, List<int> selectedIds, string Textfeedback)
        //{
            //List<int> selectedOptions = new List<int>();
            //if (type.Equals("ChoiceAction") || type.Equals("CorrectAnswerChoice"))
            //{
            //    SQLClient.getInstance().IsSelectedSingleNotificationOption(notificationId, int.Parse(selectedIds[0]));
            //    selectedOptions.Add(int.Parse(selectedIds[0]));
            //}
            //else if (type.Equals("MultipleChoiceAction"))
            //{
            //    foreach (var x in selectedIds)
            //    {
            //        // Store the status of selected rows in local Db, This code will run in case of Multi
            //        SQLClient.getInstance().IsSelectedMultipleNotificationOption(notificationId, Int32.Parse(x), 1);
            //        selectedOptions.Add(int.Parse(x));
            //    }
            //}
            //else
            //{
            //    SQLClient.getInstance().IsTextAnswerNotification(notificationId, Textfeedback);
            //}
            //surveyResponseList.questionnaireResponse.Add(new SurveyDialogueModel(notificationId, selectedOptions, Textfeedback));
        //}
    }
}
