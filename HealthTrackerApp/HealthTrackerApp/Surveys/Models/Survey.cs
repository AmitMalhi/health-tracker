﻿using HealthTrackerApp.Utility;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Surveys.Models
{
    public class Survey  : BaseModel
    {
        
        
        [Ignore]
        public List<Dialogue> mSurvey { get; set; }
        public string GroupName { get; set; }
        public bool IsAnswered { get; set; }

        public Survey(int SurveyId, string title, List<Dialogue> mSurvey, string GroupName)
        {
            this.id = SurveyId;
            this.Title = title;
            this.mSurvey = mSurvey;
            this.GroupName = GroupName;
        }
        public Survey()
        {

        }
    }
}
