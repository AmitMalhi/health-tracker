﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Surveys.Models
{
    public class SurveyFolder
    {
        public string FolderName { get; set; }

        public SurveyFolder(string FolderName)
        {
            this.FolderName = FolderName;
        }
    }
}
