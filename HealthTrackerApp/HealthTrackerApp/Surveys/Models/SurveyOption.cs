﻿using HealthTrackerApp.Utility;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Surveys.Models
{
    public class DialogueOption : BaseModel
    {
        public bool IsSelected { get; set; }
        public int DialogueId { get; set; }
        public string Type { get; set; }
        public int CountValue { get; set; }

        public DialogueOption(int id, int DialogueId, bool IsSelected, string Title, string type, int CountValue = 0)
        {
            this.id = id;
            this.DialogueId = DialogueId;
            this.IsSelected = IsSelected;
            this.Title = Title;
            this.Type = type;
            this.CountValue = CountValue;
        }

        public DialogueOption(int id)
        {

        }

        public DialogueOption()
        {

        }
    }
}
