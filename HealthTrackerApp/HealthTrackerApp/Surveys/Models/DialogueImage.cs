﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Surveys.Models
{
    public class DialogueImage
    {
        [PrimaryKey][AutoIncrement]
        public int id { get; set; }
        public int DialogueId { get; set; }
        public string ImageName { get; set; }
        public byte[] Image { get; set; }

        public DialogueImage(int DialogueId, string ImageName, byte[] Image)
        {
            this.DialogueId = DialogueId;
            this.ImageName = ImageName;
            this.Image = Image;
        }

        public DialogueImage()
        {

        }
    }
}
