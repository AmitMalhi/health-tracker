﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Surveys.Models
{
    public class SurveyDialogueModel
    {
        public int NotificationId { get; set; }

        public List<string> OptionId { get; set; }

        public string TextFeedback { get; set; }

        public SurveyDialogueModel(int _NotificationId, List<string> _OptionId, string _TextFeedback)
        {
            this.NotificationId = _NotificationId;
            this.OptionId = _OptionId;
            this.TextFeedback = _TextFeedback;
        }
        public SurveyDialogueModel()
        {

        }
    }

    public class SurveyResponseList
    {
        [JsonProperty("questionnaireResponse")]
        public List<SurveyDialogueModel> surveyResponse = new List<SurveyDialogueModel>();
    }
}
