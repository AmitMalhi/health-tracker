﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Surveys.Models
{
    public class MyImage
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        public double imageid { get; set; }

        public byte[] image {get; set;}

        public int dialogueId { get; set; }

        public MyImage(double id, int dialogueId,  byte[] image)
        {
            this.imageid = id;
            this.dialogueId = dialogueId;
            this.image = image;
        }
        public MyImage()
        {

        }
    }
}
