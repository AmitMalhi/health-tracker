﻿using HealthTrackerApp.Utility;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Surveys.Models
{
    public class Dialogue : BaseModel
    {
        public string Imageurl { get; set; }
        public string Description { get; set; }
        public string SurveyType { get; set; }
        public DateTime Created { get; set; }
        public bool IsMandatory { get; set; }
        [Ignore]
        public List<string> Responses { get; set; }
        public string TextAnswer { get; set; }
        public string GroupName { get; set; }
        public int IndexId { get; set; }
        public int ParentSurveyId { get; set; }

        public Dialogue(int DialogId, string Title, string Imageurl, string Description, string SurveyType, DateTime Created, bool IsMandatory,int ParentSurveyId, string GroupName = "")
        {
            this.id = DialogId;
            this.Title = Title;
            this.Imageurl = Imageurl;
            this.Description = Description;
            this.SurveyType = SurveyType;
            this.Created = Created;
            this.IsMandatory = IsMandatory;
            this.ParentSurveyId = ParentSurveyId;
            this.GroupName = GroupName;
        }

        public Dialogue()
        {

        }
    }
}
