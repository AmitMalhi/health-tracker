﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.PreferencesUtility
{
    //----->@Amit
    public class UserPreferences
    {
        private static UserPreferences instance;
        internal static object setInstance;

        private UserPreferences()
        {

        }

        //Todo
        public static UserPreferences getInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserPreferences();
                }
                return instance;
            }
        }


        //Set Value to the corresponding key in Shared Preference
        public void SetString(string key, string value)
        {
            //Check wether there is a value correponding to that Key, If value is present Remove Key Add key again
            if (!string.IsNullOrEmpty(GetString(key)))
                App.Current.Properties.Remove(key);
            App.Current.Properties.Add(key, value);
        }

        public void RemoveKey(string key)
        {
            App.Current.Properties.Remove(key);
        }

        //Gets Value for the corresponding key from the Shared Preference
        public string GetString(string key)
        {
            if (App.Current.Properties.ContainsKey(key))
            {
                string value = App.Current.Properties[key] == null ? null : Convert.ToString(App.Current.Properties[key]);
                return value;
            }
            return null;
        }

        //Set Value to the corresponding key in Shared Preference
        public void SetInt(string key, int value)
        {
            //Check wether there is a value correponding to that Key, If value is present Remove Key Add key again
            if (!string.IsNullOrEmpty(GetString(key)))
                App.Current.Properties.Remove(key);
            App.Current.Properties.Add(key, value);
        }


        //Gets Value for the corresponding key from the Shared Preference
        public int GetInt(string key)
        {
            if (App.Current.Properties.ContainsKey(key))
            {
                int value = App.Current.Properties[key] == null ? 0 : Convert.ToInt32(App.Current.Properties[key]);
                return value;
            }
            return 0;
        }

        //Set Value to the corresponding key in Shared Preference
        public void SetBool(string key, bool value)
        {
            //Check wether there is a value correponding to that Key, If value is present Remove Key Add key again
            if (!string.IsNullOrEmpty(GetString(key)))
                App.Current.Properties.Remove(key);
            App.Current.Properties.Add(key, value);
        }


        //Gets Value for the corresponding key from the Shared Preference
        public bool GetBool(string key)
        {
            if (App.Current.Properties.ContainsKey(key))
            {
                bool value = App.Current.Properties[key] == null ? false : Convert.ToBoolean(App.Current.Properties[key]);
                return value;
            }
            return false;
        }

        //Set Value to the corresponding key in Shared Preference
        public void SetDate(string key, DateTime value)
        {
            //Check wether there is a value correponding to that Key, If value is present Remove Key Add key again
            if (!string.IsNullOrEmpty(GetString(key)))
                App.Current.Properties.Remove(key);
            App.Current.Properties.Add(key, value);
        }


        //////Gets Value for the corresponding key from the Shared Preference
        //public DateTime GetDate(string key)
        //{
        //    if (App.Current.Properties.ContainsKey(key))
        //    {
        //        DateTime value = App.Current.Properties[key] == null ? null : Convert.ToDateTime(App.Current.Properties[key]);
        //        return value;
        //    }
        //    return null;
        //}
    }
}
