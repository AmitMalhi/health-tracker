﻿using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Prescriptions.Models
{
    public class Prescription : BaseModel
    {
      
        public string imgURL { get; set; }
      

        public Prescription(int id, string imgURL)
        {
            this.id = id;
            this.imgURL = imgURL;
          
        }

        public Prescription()
        {

        }
    }
}
