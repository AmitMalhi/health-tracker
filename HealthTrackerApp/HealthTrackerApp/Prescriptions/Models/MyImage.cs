﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Prescriptions.Models
{
    public class MyImage
    {
        public double imageid { get; set; }

        public byte[] image {get; set;}

        public MyImage(double id, byte[] image)
        {
            this.imageid = id;
            this.image = image;
        }
       // public MyImage(int id)
    }
}
