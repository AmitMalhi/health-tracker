﻿using Firebase.Storage;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Prescriptions.Models;
using HealthTrackerApp.Utility;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HealthTrackerApp.Prescriptions.Pages
{
    class PrescriptionPage : BasePage
    {
        private Label mLblPrescription, mlblattach;
        private CustomEditor mEditorPrescription;
        private StackLayout mStackLayout, attachstack, meditorstack, mStackButton,imageStack;
        private Image attachimage, showImage, crossImage;
        private ScrollView scrollView;
        private ListView imagelistview;
        public byte[] imageAsBytes;
        private const string CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY = "Choose from Gallery";
        private const string CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO = "Take Photo";
        private const string CONST_TAKE_PHOTO_FOLDER_NAME = "Feed_Photos";
        private Button upload_Btn;
        private List<string> listfirebaseURL;
        private string imageName = "";
        private MediaFile mediaFile;
        private BoxView mEntryBoxView;
        private TapGestureRecognizer removephtoGestureRecognizer;
        private List<MyImage> imageUrl, finalimageUrl;
        private TapGestureRecognizer AddGalleryImageGestureRecognizer;
        private StackLayout mfinalstack, _addimagestack;
        long lastPress;
        // Constructor
        public PrescriptionPage()
        {
            Title = "Prescription";
            InitializeUiComponents();
            AddGesturesToUiComponents();
        }

      
        /**
         * 
         */ 
        private void InitializeUiComponents()
        {
            listfirebaseURL = new List<string>();
            AddGalleryImageGestureRecognizer = new TapGestureRecognizer();
            removephtoGestureRecognizer = new TapGestureRecognizer();
            mfinalstack = new StackLayout();
            mEditorPrescription = new CustomEditor();
            mLblPrescription = new Label();
            mStackLayout = new StackLayout();
            attachimage = new Image();
            attachstack = new StackLayout();
            imageStack = new StackLayout();
            upload_Btn = new Button();
            mEntryBoxView = new BoxView();
            finalimageUrl = new List<MyImage>();
            scrollView = new ScrollView();
            mStackButton = new StackLayout();
            meditorstack = new StackLayout();
            mlblattach = new Label();
            imagelistview = new ListView();
            crossImage = new Image();
            imageUrl = new List<MyImage>();

        }

        /**
         * 
         */ 
        private void AddGesturesToUiComponents()
        {

            AddGalleryImageGestureRecognizer.Tapped += AddImageGestureRecognizer_TappedAsync;
            attachimage.GestureRecognizers.Add(AddGalleryImageGestureRecognizer);
            upload_Btn.Clicked += Upload_Btn_Clicked;

        }

        protected override void OnAppearing()
        {

            SetUI();
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
          
        }

    
        // set up all the views on the UI
        private void SetUI()
        {
            // set the Label to the UI
            mLblPrescription.HorizontalOptions = LayoutOptions.Start;
            mLblPrescription.VerticalOptions = LayoutOptions.Start;
            mLblPrescription.Margin = new Thickness(10, 20, 10, 0);
            mLblPrescription.Text = "Please provide the prescription in the following text area or click a picture of prescription.";
            mLblPrescription.FontSize = 18;

            // set the Editor to the UI
            mEditorPrescription.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEditorPrescription.HeightRequest = 200;
            mEditorPrescription.Placeholder = "Write here...";
            mEditorPrescription.BackgroundColor = Color.White;

            // editor stack adding the mEditor 
            meditorstack.Children.Add(mEditorPrescription);
            meditorstack.Padding = new Thickness(1, 1, 1, 1);
            meditorstack.Margin = new Thickness(10, 0, 10, 0);
            meditorstack.BackgroundColor = Color.Black;

            // setting Upload button image to the UI
            attachimage.Source = "attach.png";
            attachimage.HeightRequest = 30;
            attachimage.WidthRequest = 30;
            attachimage.HorizontalOptions = LayoutOptions.Start;

            //label attach to image 
            mlblattach.Text = "Attach Image to Upload";
            mlblattach.HorizontalOptions = LayoutOptions.Start;

            // set image stack to UI for adding image from the gallery
            imageStack.Padding = new Thickness(10, 10, 0, 0);
            imageStack.Orientation = StackOrientation.Vertical;

            // set Upload button for uploading images to the Fireabase
            upload_Btn.Text = "Upload";
            upload_Btn.VerticalOptions = LayoutOptions.EndAndExpand;
            upload_Btn.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            upload_Btn.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
            upload_Btn.FontSize = 18;

            // stack for adding the Label and attach_image to the UI
            attachstack.Margin = new Thickness(10, 20, 0, 0);
            attachstack.Orientation = StackOrientation.Vertical;
            attachstack.Children.Add(attachimage);
            attachstack.Children.Add(mlblattach);

            // set mStacklayout to the UI. It contains childrens as mLblPrescription, meditorstack, attachstack, imageStack
            mStackLayout.Orientation = StackOrientation.Vertical;
            mStackLayout.Spacing = 18;
            mStackLayout.Children.Add(mLblPrescription);
            mStackLayout.Children.Add(meditorstack);
            mStackLayout.Children.Add(attachstack);
            mStackLayout.Children.Add(imageStack);

            // set mStackButton stack for adding the upload button
            mStackButton.Orientation = StackOrientation.Vertical;
            mStackButton.HorizontalOptions = LayoutOptions.FillAndExpand;
            mStackButton.VerticalOptions = LayoutOptions.EndAndExpand;
            mStackButton.Children.Add(upload_Btn);
            
            // setting the scroll view component as the mStackLayout 
            scrollView.Content = mStackLayout;

            /* set mfinalstack
             * It will set the final UI for the Page 
             * in this we add the scroll view and mStackButton to the mfinalstack
             */
            mfinalstack.Children.Add(scrollView);
            mfinalstack.Children.Add(mStackButton);

            Content = mfinalstack;
        }


        // attach image click logic to select the photo from gallery or click the new photo
        private async void AddImageGestureRecognizer_TappedAsync(object sender, EventArgs e)
        {

            /**
             * action contains the results of the action sheet that contains three options =>
             * Take Photo
             * Choose from Gallery
             * Cancel
             */ 
            string action = await DisplayActionSheet("Add Photo", "Cancel", null, CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO, CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY);

            /* if user not choose cancel =>
             * Now if user gives both the permission i.e for camera and storage we either select image from the gallery or click new image
             * if user not provide permission we will return from the functions
             */
            if (action != "Cancel")
            {
                var camerapermissions = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storagepermissions = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (camerapermissions != PermissionStatus.Granted || storagepermissions != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                    camerapermissions = results[Permission.Camera];
                    storagepermissions = results[Permission.Storage];
                }

                if (camerapermissions != PermissionStatus.Granted || storagepermissions != PermissionStatus.Granted)
                    return;
            }
            /* if the user select the Choose from Gallery
             * 
             */
            switch (action)
            {
                case CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY: //Select Photo from Gallery
                    if (!CrossMedia.Current.IsPickPhotoSupported)
                    {
                        await DisplayActionSheet("", "", "Photo Permission", "Ok");

                        return;
                    }

                    /* To choose the image from the gallery we use pick photo async
                     * Photosize.Medium  will gives the 50% size of the orignal image
                     * 
                     */
                    mediaFile = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                    {

                        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium

                    });

                    /* Now if user select the picture i.e if the mediafile != null
                     * we will convert the mediafile to the imageAsBytes
                     * Now we add the bytes to the imageURl 
                     */
                    if (mediaFile != null)
                    {
                        imageAsBytes = ConvertToBytes(mediaFile);
                        if (imageAsBytes != null)
                        {
                            imageUrl.Clear();
                           double id =  DateTime.Now.Ticks;
                            MyImage myImage = new MyImage(id, imageAsBytes);
                            imageUrl.Add(myImage);
                        }

                        imageName = string.Format("img_{0}.jpg", DateTime.UtcNow.ToString("yyyyMMddhhmmss"));
                    }
                    else
                        return;
                    break;

                /* Now if user gonna take the picture from the camera 
                 * 
                 */

                case CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO: //Take a Photo
                    bool test = await CrossMedia.Current.Initialize();

                    if (test && (!CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsCameraAvailable))
                    {
                        await DisplayActionSheet("", "", "No Camera", "Ok");

                        return;
                    }
                    try
                    {
                        /*Take photo from camera
                         * StoreCameraMediaOptions set the rear camera as the default, Photo size  = 50% of the orignal, 
                         */
                        mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions { DefaultCamera = CameraDevice.Rear, SaveToAlbum = true, PhotoSize = PhotoSize.Medium, CompressionQuality = 50, Directory = CONST_TAKE_PHOTO_FOLDER_NAME, Name = imageName });

                       /* we will convert the mediafile to the imageAsBytes
                        *Now we add the bytes to the imageURl
                        * we add the imagebytes to the imageUrl
                        */    
                        if (mediaFile != null)
                        {
                            imageAsBytes = ConvertToBytes(mediaFile);
                            
                            if (imageAsBytes != null)
                            {
                                imageUrl.Clear();
                                double id = DateTime.Now.Ticks;
                                MyImage myImage = new MyImage(id, imageAsBytes);
                                imageUrl.Add(myImage);
                            }
                        }
                        else
                            return;
                     
                        imageName = string.Format("img_{0}.jpg", DateTime.UtcNow.ToString("yyyyMMddhhmmss"));

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }
                    break;

                default:
                    imageAsBytes = null;
                    break;
            }
            // funtion called for showing the selected or clicked image  to the UI
            ShowAttachedPhotoOnPage(imageUrl);
        }

     
        // show the attached image on UI logic
        private void ShowAttachedPhotoOnPage(List<MyImage> _imagelist)
        {
            /* finalimageUrl contains selected image list. it is the final list that maintained for uploading the images to the firebase 
             * everytime user attach the image the finalimageurl gets updated
             */
            finalimageUrl.AddRange(_imagelist);
            
            /* we loop through the _imagelist and update the UI
             * 
             */
            foreach (var item in _imagelist)
            {
                try
                {

                   /*
                    *  initialise _addimagestack and add the attached image and cross image to the _addimagestack 
                    */
                    _addimagestack = new StackLayout();
                    _addimagestack.WidthRequest = 250;
                    _addimagestack.HeightRequest = 250;
                    _addimagestack.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    _addimagestack.Orientation = StackOrientation.Horizontal;

                    /*
                     * initialise showimage the UI
                     */
                    showImage = new Image();
                    showImage.Aspect = Aspect.AspectFit;
                    showImage.WidthRequest = 200;
                    showImage.HeightRequest = 200;
                    showImage.HorizontalOptions = LayoutOptions.Center;
                    // set the source of the showimage 
                    showImage.Source = ImageSource.FromStream(() => new MemoryStream(item.image));

                    crossImage = new Image();
                    crossImage.Source = "cancel.png";
                    crossImage.HeightRequest = 40;
                    crossImage.HeightRequest = 40;
                    crossImage.VerticalOptions = LayoutOptions.Start;
                    //crossImage.HorizontalOptions = LayoutOptions.Start;
                    crossImage.ClassId = Convert.ToString(item.imageid);
                    // crossImage.Margin = new Thickness(0, 0, 40, 0);

                    /* add the showimage and crossimage to the _addimagestack
                     */
                    _addimagestack.Children.Add(showImage);
                    _addimagestack.Children.Add(crossImage);

                    //add tap gesture to remove image by adding gesture to the crossimage
                    removephtoGestureRecognizer.Tapped += RemoveImageEventGesture; 
                    crossImage.GestureRecognizers.Add(removephtoGestureRecognizer);

                    /*
                     * adding _addimage stack to the imagesStack everytime we attach the new photo 
                     */
                    imageStack.Children.Add(_addimagestack);
                }

                catch (Exception ex)
                {

                }
            }
            
        }
        
     
        // check the list to upload the image on the firebase 
        private void Upload_Btn_Clicked(object sender, EventArgs e)
        {
            /*
             *check the count of the finalimageUrl if the count == 0 we will display alert
             */
            if(finalimageUrl.Count == 0)
            {
                DisplayAlert("Alert", "Please select image", "OK");
            }

            if (!InternetConnectivity.IsInternetConnected)
            {
                DisplayAlert("Alert", "Please Check your Internet connection!", "Ok");
                return;
            }

            /*
             * loop through the finalimageUrl check if the item contains image then we will upload the Image to the Firebase 
             */
            foreach (var item in finalimageUrl)
            {
                if (item.image == null && string.IsNullOrEmpty(mEditorPrescription.Text))
                {
                    DisplayAlert("Alert", "Please enter text or select image", "OK");
                    return;
                }

                else if (item.image != null)
                {
                    UploadImagetoFirebaseAsync(new MemoryStream(item.image));
                }

                else
                {
                    return;
                }
            }
        }

    
        // Upload the images to the firebase logic 
        private async void UploadImagetoFirebaseAsync(Stream item)
        {
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));
            /*
             * call the firebase to uploaded the images to the firebase storage
             * give the each image a unique name that will be uploaded to the firebase
             * then add the downloaded Url to the listfirebaseURL 
             * 
             */
            double date = DateTime.Now.Millisecond;
            string img_name = string.Format("img_{0}.jpg", DateTime.UtcNow.ToString("yyyyMMddhhmmssfff"));
            listfirebaseURL.Add(await new FirebaseStorage("healthtracker-7e0f9.appspot.com").Child("ImageList").Child(img_name).PutAsync(item, new System.Threading.CancellationToken(false), "image/jpeg"));

            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
           if(listfirebaseURL.Count == finalimageUrl.Count)
            {
                await DisplayAlert("Image", "Image uploaded sucessfully", "OK");
                    //listfirebaseURL.Clear();
                    //finalimageUrl.Clear();
                    //imageUrl.Clear();
                    InitializeUiComponents();
                    SetUI();
                    AddGesturesToUiComponents();


            }
            
        }
        protected override bool OnBackButtonPressed()
        {

            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 5000)
            {
                DisplayAlert("", "Press back again to exit", "OK");
                lastPress = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }


        // remove the image from the stack on the cross image click
        private void RemoveImageEventGesture(object sender, EventArgs e)
        {
            try
            {
                /* get the crossobj as image 
                 * find the index at where the user tap by matching the same id in the finalimageUrl
                 * then remove the imagebytes from the selected index
                 * after that remove the children from the imageStack according to removalindex
                 */
                var crossObj = sender as Image;
                var removalIndex = finalimageUrl.FindIndex(x => x.imageid.ToString() == crossObj.ClassId);
                finalimageUrl.RemoveAt(removalIndex);
                imageStack.Children.RemoveAt(removalIndex);
               
            }
            catch (Exception ex)
            {

            }
        }


        // convert the image into the bytes
        public static byte[] ConvertToBytes(MediaFile input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.GetStream().CopyTo(ms);
                input.Dispose();
                return ms.ToArray();
            }
        }
    }
}
