﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Notes.Cells
{
    public class NoteItemCell : ViewCell
    {
        private Label datetimecreated;
        private Label notestext;

   
        public NoteItemCell()
        {
            var mStacklayout = new StackLayout();
            notestext = new Label
            {
                Text = "default",
                WidthRequest = 150,
                LineBreakMode = LineBreakMode.TailTruncation,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions =LayoutOptions.CenterAndExpand,
            };

            datetimecreated = new Label
            {
                Text = "123",
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions =LayoutOptions.CenterAndExpand
            };

            mStacklayout.Orientation = StackOrientation.Horizontal;
            mStacklayout.Padding = new Thickness(5, 0, 5, 0);
            mStacklayout.Children.Add(notestext);
            mStacklayout.Children.Add(datetimecreated);

            View = mStacklayout;

            notestext.SetBinding(Label.TextProperty, "Title");
            datetimecreated.SetBinding(Label.TextProperty, "Created");
        }

    }

}
