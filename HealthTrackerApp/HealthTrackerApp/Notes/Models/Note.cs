﻿using HealthTrackerApp.Utility;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Notes.Models
{
    public class Note 
    {
        [PrimaryKey , AutoIncrement]
        public int id { get; set; }
        public string Title { get; set; }
        public string Created { get; set; }

        public Note(int noteid ,string title, string Created)
        {
            this.id = noteid;
            this.Title = title;
            this.Created = Created;
        }

        public Note()
        {

        }
    }
}
