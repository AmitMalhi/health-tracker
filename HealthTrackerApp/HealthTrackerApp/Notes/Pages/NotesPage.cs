﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Notes.Cells;
using HealthTrackerApp.Notes.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Notes.Pages
{
    public class NotesPage : BasePage
    {
     //   private Label noteTxt;
        private ListView noteslistview;
        private string note, datetime;
        private List<Note> notelist;
        long lastPress;
        public NotesPage()
        {
            Title = "Notes";
            ToolbarItems.Add(new ToolbarItem("", "add.png", () =>
            {
                //logic code goes here
               
                Navigation.PushAsync(new NotesDetailPage(-1));
            }));

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
            notelist = new List<Note>();
            var notes = new NotesDBRepositary().GetAll();
            if(notes !=null && notes.Count != 0)
            {
                notelist.AddRange(notes);
            }
            notes = new List<Note>();
            noteslistview = new ListView();
            noteslistview.HorizontalOptions = LayoutOptions.FillAndExpand;
            noteslistview.VerticalOptions = LayoutOptions.FillAndExpand;
            noteslistview.RowHeight = 50;
            noteslistview.ItemsSource = notelist;
            noteslistview.ItemTemplate = new DataTemplate(typeof(NoteItemCell));
            noteslistview.ItemSelected += Noteslistview_ItemSelected;
            Content = noteslistview;

        }

        private void Noteslistview_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Note note =(Note) e.SelectedItem;
            Navigation.PushAsync(new NotesDetailPage(note.id));
        }

        protected override bool OnBackButtonPressed()
        {

            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 5000)
            {
                DisplayAlert("", "Press back again to exit", "OK");
                lastPress = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
