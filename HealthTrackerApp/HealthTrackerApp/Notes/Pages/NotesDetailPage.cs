﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Notes.Models;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Notes.Pages
{
  public  class NotesDetailPage : BasePage
    {
        private StackLayout notesStack, buttonstack, viewStack;
        private CustomEditor notesEditor;
        private Image deletenoteImg, sharenotImg;
        private string datetimetitle;
        private Note selectedNote;
        private int noteId;
        TapGestureRecognizer removenotesGestureRecognizer, sharenotesGestureRecognizer;

        public NotesDetailPage(int noteid)
        {
            noteId = noteid;
            if(noteId >= 0)
            selectedNote =  new NotesDBRepositary().Get(x => x.id == noteId);
            DateTime todaytime = DateTime.Now;
            datetimetitle = String.Format("{0:MMM d,yyyy,hh:mm tt}", todaytime);
            Title = datetimetitle;
            NavigationPage.SetHasBackButton(this, true);
            removenotesGestureRecognizer = new TapGestureRecognizer();
            sharenotesGestureRecognizer = new TapGestureRecognizer();
            notesStack = new StackLayout();
            buttonstack = new StackLayout();
            notesEditor = new CustomEditor();
            deletenoteImg = new Image();
            sharenotImg = new Image();
            viewStack = new StackLayout();
            
            // adding item to the navigation bar
            ToolbarItems.Add(new ToolbarItem("save", "", () =>
            {
                if (!string.IsNullOrEmpty(notesEditor.Text))
                {
                    Note savenoteDB = new Note();
                    savenoteDB.Title = notesEditor.Text.ToString();
                    savenoteDB.Created = datetimetitle;
                    if (noteId < 0)
                        new NotesDBRepositary().Save(savenoteDB);
                    else
                    {
                        selectedNote.Title = notesEditor.Text;
                        selectedNote.Created = datetimetitle;
                        new NotesDBRepositary().Update(selectedNote);
                    }

                    Navigation.PopAsync();
                }
                else
                {
                    DisplayAlert("Alert", "Enter Some Text!!", "OK");
                }


            }));
            removenotesGestureRecognizer.Tapped += RemoveNotesGestureRecognizer_TappedAsync;
            deletenoteImg.GestureRecognizers.Add(removenotesGestureRecognizer);
            sharenotesGestureRecognizer.Tapped += SharenotesGestureRecognizer_Tapped;
            sharenotImg.GestureRecognizers.Add(sharenotesGestureRecognizer);
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAsync();
            return false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));

            notesEditor.VerticalOptions = LayoutOptions.FillAndExpand;
            notesEditor.HorizontalOptions = LayoutOptions.FillAndExpand;
            if (selectedNote != null) 
            notesEditor.Text = selectedNote.Title;
            // adding the editor to the notes stack
            notesStack.HorizontalOptions = LayoutOptions.FillAndExpand;
            notesStack.VerticalOptions = LayoutOptions.FillAndExpand;
            notesStack.Children.Add(notesEditor);
            notesStack.Spacing = 0;

            // delete image button
            deletenoteImg.Source = "delete.png";
            deletenoteImg.HorizontalOptions = LayoutOptions.StartAndExpand;
            deletenoteImg.Margin = new Thickness(0, 0, 0, 2);
            deletenoteImg.HeightRequest = 30;
            deletenoteImg.WidthRequest = 30;

            // share image button 
            sharenotImg.Source = "share.png";
            sharenotImg.HorizontalOptions = LayoutOptions.EndAndExpand;
            sharenotImg.Margin = new Thickness(0, 0, 5, 0);
            sharenotImg.HeightRequest = 30;
            sharenotImg.WidthRequest = 30;

            
            buttonstack.Orientation = StackOrientation.Horizontal;
            buttonstack.HorizontalOptions = LayoutOptions.FillAndExpand;
            buttonstack.Children.Add(deletenoteImg);
            buttonstack.Children.Add(sharenotImg);
            buttonstack.BackgroundColor = Color.FromHex(MyUtility.MenuBackgroundColor);

            // View Stack for the full page
            viewStack.HorizontalOptions = LayoutOptions.FillAndExpand;
            viewStack.VerticalOptions = LayoutOptions.FillAndExpand;
            viewStack.Children.Add(notesStack);
            viewStack.Children.Add(buttonstack);

            Content = viewStack;

        }

        // share notes button click handle 

        private void SharenotesGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(notesEditor.Text))
                DependencyService.Get<IShareNotes>().OpenShareIntent(notesEditor.Text);

            else
                DisplayAlert("Alert", "Please Enter Text to Share!", "OK");
        }

        // Remove the notes from the list 
        private async void RemoveNotesGestureRecognizer_TappedAsync(object sender, EventArgs e)
        {
            // Display alert for deleting the notes
            if(selectedNote !=null && selectedNote.id >= 0)
            {
                bool deleteitem = await DisplayAlert("Alert", "Are you sure to want to delete?", "Yes", "No");
                if (deleteitem)
                {
                    new NotesDBRepositary().Delete(selectedNote.id);
                    await Navigation.PopAsync();
                }
            }
            else
            {
               await DisplayAlert("Alert", "No Item selected to delete", "OK");
            }

        }

    }
}
