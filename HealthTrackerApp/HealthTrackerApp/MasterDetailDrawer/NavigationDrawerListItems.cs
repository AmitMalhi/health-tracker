﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.Login.Pages;
using HealthTrackerApp.Notes.Pages;
using HealthTrackerApp.NutritionValues.Pages;
using HealthTrackerApp.Prescriptions.Pages;
using HealthTrackerApp.Reminders.Pages;
using HealthTrackerApp.Reports.Pages;
using HealthTrackerApp.Surveys.Pages;
using System.Collections.Generic;
using System.Linq;

namespace HealthTrackerApp.HelperClasses
{
    public class NavigationDrawerListItems
    {

        private static List<NavigationDrawerItem> mMenuItems = new List<NavigationDrawerItem>();
        public static List<NavigationDrawerItem> FillNavigationDrawerItems()
        {
            var surveyFolderList = APICalls.GetAllSurveys().Select(x=>x.GroupName).Distinct().ToList();

            mMenuItems.Clear();

            foreach (var item in surveyFolderList)
            {
                mMenuItems.Add(new NavigationDrawerItem { Item = item, Icon = "survey.png", Target = typeof(SurveyBasePage) });
            }
            mMenuItems.Add(new NavigationDrawerItem { Item = "Prescriptions", Icon = "prescription.png", Target = typeof(PrescriptionPage) });
            mMenuItems.Add(new NavigationDrawerItem { Item = "Reports", Icon = "Reports.png", Target = typeof(ReportsPage) });
            mMenuItems.Add(new NavigationDrawerItem { Item = "Gallery", Icon = "ImageGallery.png", Target = typeof(GalleryPage) });
            mMenuItems.Add(new NavigationDrawerItem { Item = "Reminders", Icon = "Reminder.png", Target = typeof(ReminderPage) });
            mMenuItems.Add(new NavigationDrawerItem { Item = "Notes", Icon = "Notes.png", Target = typeof(NotesPage) });
            mMenuItems.Add(new NavigationDrawerItem { Item = "Nutrition", Icon = "restaurant.png", Target = typeof(NutritionMainPage)});
            mMenuItems.Add(new NavigationDrawerItem { Item = "Logout", Icon = "logout.png", Target = typeof(LoginPage) });

            return mMenuItems.ToList();
        }
    }
}
