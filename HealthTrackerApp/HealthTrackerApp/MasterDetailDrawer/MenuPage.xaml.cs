﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Pages;
using HealthTrackerApp.Login.Pages;
using HealthTrackerApp.Notes.Pages;
using HealthTrackerApp.NutritionValues.Pages;
using HealthTrackerApp.PreferencesUtility;
using HealthTrackerApp.Prescriptions.Pages;
using HealthTrackerApp.Profile.Pages;
using HealthTrackerApp.Reminders.Pages;
using HealthTrackerApp.Reports.Pages;
using HealthTrackerApp.Surveys;
using HealthTrackerApp.Surveys.Models;
using HealthTrackerApp.Surveys.Pages;
using HealthTrackerApp.Utility;
using ImageCircle.Forms.Plugin.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static HealthTrackerApp.Profile.Pages.UserProfilePage;

namespace HealthTrackerApp.MasterDetailDrawer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : IUpdateMenuPage
    {
        private MyMasterDetailPage masterDetailPage;
        private ListView listView;
        private List<NavigationDrawerItem> menuList;
        public static MainCarouselPage surveyCarauselPage;
        public static Page currentPage;
        private Image menuLogo;
        private StackLayout profileStack;
        private CircleImage userImage;
        private Label userName;
        private UserProfile userProfile;

        public MenuPage(MyMasterDetailPage masterDetailPage)
        {
            InitializeComponent();
            this.masterDetailPage = masterDetailPage;
            listView = new ListView();
            menuLogo = new Image();
            menuList = new List<NavigationDrawerItem>();
            

            profileStack = new StackLayout();

            userProfile = new UserProfileDBRepository().GetFirstOrDefault();
            menuList = NavigationDrawerListItems.FillNavigationDrawerItems();
            userImage = new CircleImage();
            userImage.HeightRequest = 60;
            userImage.WidthRequest = 60;
            userImage.Aspect = Aspect.Fill;
            userImage.Source = "username.png";
            if (Device.OS == TargetPlatform.iOS)
            {
                userImage.Margin = new Thickness(10, 10, 0, 0);
            }

            userName = new Label();
            userName.FontSize = 20;
            userName.VerticalTextAlignment = TextAlignment.Center;
            userName.Text = "Set Profile";
            
            if (userProfile != null && !string.IsNullOrEmpty(userProfile.FirstName))
            {
                userName.Text = userProfile.FirstName + " " + userProfile.LastName;
                userName.TextColor = Color.Black;
            }
            if(userProfile != null && userProfile.Image != null)
            {
                userImage.Source = ImageSource.FromStream(() => new MemoryStream(userProfile.Image));
            }

            profileStack.Spacing = 30;
            profileStack.Orientation = StackOrientation.Horizontal;
            profileStack.Children.Add(userImage);
            profileStack.Children.Add(userName);
            profileStack.Margin = new Thickness(10, 10, 0, 10);
            profileStack.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnProfileClick()) });

            menuLogo.Source = "healthapplogo.png";
            menuLogo.HeightRequest = 85;
            menuLogo.WidthRequest = 85;

            //Navigation drawer menu UI with data source and data template
            #region ListView
            listView = new ListView
            {
                BackgroundColor = Color.Transparent,
                ItemsSource = menuList,
                ItemTemplate = new  DataTemplate(() =>
                {
                    var label = new Label();
                    label.SetBinding(Label.TextProperty, "Item");
                    label.FontSize = 19;
                    label.TextColor = Color.Black;//Color.FromHex(MyUtility.MenuTextColor);
                    label.Margin = new Thickness(0, 10, 10, 10);

                    var image = new Image();
                    image.SetBinding(Image.SourceProperty, "Icon");
                    image.HeightRequest = 40;
                    image.WidthRequest = 40;
                    image.Margin = new Thickness(10, 10, 10, 10);
                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                                image,
                                label
                            }
                        }
                    };
                })
            };
            #endregion ListView

            listView.RowHeight = 50;
            listView.ItemTapped += ListView_ItemTapped;
            listView.Margin = new Thickness(0, 10, 0, 0);
            listView.Header = profileStack;
            listView.Footer = menuLogo;
            this.Content = listView;
        }

        private async void OnProfileClick()
        {
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));

            masterDetailPage.IsPresented = false; //Menu is close by default
            await Task.Delay(225);
            Page contentPage = new UserProfilePage(this);
            ((MyMasterDetailPage)this.Parent).Detail = new NavigationPage(contentPage)
            {
                BarBackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor),
                BarTextColor = Color.FromHex(MyUtility.StatusBarTextColor),
            };
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var obj = listView.SelectedItem as NavigationDrawerItem;

            if (userProfile ==null )
            {
                if(obj.Target == typeof(LoginPage))
                {

                }
                else
                {
                    DisplayAlert("", "Update profile first", "OK");
                    return;
                }
                
            }

            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));

            
            Page contentPage = null;


            if (obj.Target == typeof(SurveyBasePage))
            {
                var SurveysInFolder = APICalls.GetSurveyUnderFoler(obj.Item);
                if (SurveysInFolder.Count == 1)
                {
                    surveyCarauselPage = new MainCarouselPage(SurveysInFolder.FirstOrDefault().id);
                    contentPage = surveyCarauselPage;
                }
                else
                {
                    /**
                     * Open Survey Listing Page
                     * Here obj.item  is the name of Group(Folder)
                     * pass this SurveysInFolder list to constructor of that page
                     * and show the listing inside that
                     *
                     */
                    contentPage = new SurveyFolderPage(SurveysInFolder);
                }
            }
            else if (obj.Target == typeof(PrescriptionPage))
            {
                contentPage = new PrescriptionPage();
            }
            else if (obj.Target == typeof(ReportsPage))
            {
                contentPage = new ReportsPage();
            }
            else if (obj.Target == typeof(GalleryPage))
            {
                contentPage = new GalleryPage();
            }
            else if (obj.Target == typeof(ReminderPage))
            {
                contentPage = new ReminderPage();
            }
            else if (obj.Target == typeof(NotesPage))
            {
                contentPage = new NotesPage();
            }
            else if (obj.Target == typeof(NutritionMainPage))
            {
                contentPage = new NutritionMainPage();
            }

            else if (obj.Target == typeof(LoginPage))
            {
                UserPreferences.getInstance.RemoveKey("AuthToken");
                contentPage = new LoginPage();
                Navigation.PushAsync(contentPage);
                Navigation.RemovePage(masterDetailPage);
                return;
            }
            //if (currentPage != null && contentPage != null && currentPage.GetType() == contentPage.GetType())
            //{
            //    masterDetailPage.IsPresented = false; //Menu is close by default

            //    Device.BeginInvokeOnMainThread(new Action(() =>
            //    {
            //        DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            //    }));

            //    return;
            //}
          
                currentPage = contentPage;
                SetPageNavigation(contentPage);
           
        }

        public void UpdateProfileInfo(byte[] Img, string name) //Update Profile Image and Profile Name at the bottom of the Navigation Drawer
        {
            if(Img!=null)
            {
                userImage.Source = ImageSource.FromStream(() => new MemoryStream(Img));
            }
            //else
            //{
            //    userImage.Source = "username.png";
            //}
            userName.Text = name;
            userName.TextColor = Color.Black;
            userProfile = new UserProfileDBRepository().GetFirstOrDefault();
        }

        //Set page navigation on basis of page target type
        private async void SetPageNavigation(Page contentPage)
        {
            masterDetailPage.IsPresented = false; //Menu is close by default
            await Task.Delay(225);
            ((MyMasterDetailPage)this.Parent).Detail = new NavigationPage(contentPage)
            {
                BarBackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor),
                BarTextColor = Color.FromHex(MyUtility.StatusBarTextColor),
            };
        }
      
    }
}