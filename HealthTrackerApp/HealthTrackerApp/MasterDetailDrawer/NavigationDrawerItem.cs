﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.HelperClasses
{
    public class NavigationDrawerItem
    {
        public string Item { get; set; }
        public string Icon { get; set; }
        public Type Target { get; set; }
        public int SortOrder { get; set; } 
    }
}
