﻿using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.Utility;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.Xaml;

namespace HealthTrackerApp.MasterDetailDrawer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BasePage : ContentPage
	{
        

		public BasePage ()
		{
			InitializeComponent ();
            if (Device.OS == TargetPlatform.iOS)
            {
                On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
                Xamarin.Forms.NavigationPage.SetBackButtonTitle(this, "");
            }
            else
            {
                Xamarin.Forms.NavigationPage.SetHasBackButton(this, false);
            }
            BackgroundColor  = Color.FromHex(MyUtility.MenuBackgroundColor);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));

        }
    }
}