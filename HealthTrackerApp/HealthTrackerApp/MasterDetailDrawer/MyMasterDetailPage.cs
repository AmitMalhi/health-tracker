﻿using HealthTrackerApp.Reports.Pages;
using HealthTrackerApp.Utility;
using Xamarin.Forms;

namespace HealthTrackerApp.MasterDetailDrawer
{
    //----->@Amit
    public class MyMasterDetailPage : MasterDetailPage
    {
        public MyMasterDetailPage()
        {
            //Hides Navigation bar for the My MasterDetail Page
            NavigationPage.SetHasNavigationBar(this, false);

            //Navigation Drawer Open-Close property
            MasterBehavior = MasterBehavior.Popover;

            //Created Menu page (Navigation Drawer)
            Master = new MenuPage(this)  
            {
                Icon = "line_menu.png",
                Title = "Health Tracker App",
                BackgroundColor = Color.FromHex(MyUtility.MenuBackgroundColor),
            };
            //IsPresented = true;  //Makes Navigation Drawer Open

            Page contentPage = new ReportsPage();

            Detail = new NavigationPage(contentPage)
            {
                BarBackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor),
                BarTextColor = Color.FromHex(MyUtility.StatusBarTextColor)
            };
        }
       
    }
}
