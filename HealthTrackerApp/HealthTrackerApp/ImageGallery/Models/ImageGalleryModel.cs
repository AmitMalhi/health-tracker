﻿using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.ImageGallery.Models
{
   public class ImageGalleryModel : BaseModel
    {
        public string ImageUrl { get; set; }

        public ImageGalleryModel(int id, string Title, string imageUrl)
        {
            this.ImageUrl = imageUrl;
            this.id = id;
            this.Title = Title;
        }
    }
}
