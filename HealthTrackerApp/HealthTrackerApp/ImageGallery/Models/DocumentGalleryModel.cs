﻿using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.ImageGallery.Models
{
    public class DocumentGalleryModel : BaseModel
    {
        public string DocumentUrl { get; set; }
        public string iconUrl { get; set; }
        public string DocTitle { get; set; }

        public DocumentGalleryModel(int id, string Title, string documentUrl)
        {
            this.DocumentUrl = documentUrl;
            this.id = id;
            this.DocTitle = Title;
            this.iconUrl = "file.png";
        }
    }
}
