﻿using FFImageLoading.Forms;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.ImageGallery.Cells
{
   public class ImageGalleryCell  : ViewCell
    {
        private CachedImage galleryImage;
        private Label Title;
        
        public ImageGalleryCell()
        {
            
            try
            {
                InitialiseAndSetUpUI();
            }
            catch
            {
                Console.WriteLine("getline");
            }
           

        }

        /* SetUp and Initialise the UI of the Cell
         * */
        private void InitialiseAndSetUpUI()
        {
            // set up the Image to the UI
            galleryImage = new CachedImage
            {
                Margin = new Thickness(0, 10, 0, 0),
                IsOpaque = true,
                TransformPlaceholders = true,
                LoadingPlaceholder = "placeholder.png",
                ErrorPlaceholder = "internet.png",
                HeightRequest = 100,
                WidthRequest = 100,
            };

            // set up the Label to the UI
            Title = new Label
            {
                Text = "Defalut",
                FontSize = 16,
                Margin = new Thickness(0, 0, 0, 10),
                HorizontalOptions = LayoutOptions.Center,
                
            };

            // adding the image and Text 
            //RelativeLayout layout = new RelativeLayout();
            //layout.BackgroundColor = Color.FromHex(MyUtility.MenuBackgroundColor);
            //// adding constraint to the gallery image
            //layout.Children.Add(galleryImage,
            //    Constraint.Constant(0),
            //    Constraint.Constant(0),
            //    Constraint.RelativeToParent((parent) => { return parent.Width; }),
            //    Constraint.RelativeToParent((parent) => { return parent.Height - 30; }));

            //// adding constraints to the Label
            //layout.Children.Add(Title,
            //    Constraint.Constant(Application.Current.MainPage.Width / 2 - 30),
            //    Constraint.Constant(280),
            //    Constraint.RelativeToParent((parent) => { return parent.Width; }),
            //    Constraint.RelativeToParent((parent) => { return parent.Height; }));

            StackLayout layout = new StackLayout();
            layout.Orientation = StackOrientation.Vertical;
            layout.Children.Add(galleryImage);
            layout.Children.Add(Title);
            View = layout;
            /*
             * Set the bindings of the Imagegallery model to the UI 
             */
            galleryImage.SetBinding(CachedImage.SourceProperty, "ImageUrl");
            Title.SetBinding(Label.TextProperty, "Title");
        }
    }
}
