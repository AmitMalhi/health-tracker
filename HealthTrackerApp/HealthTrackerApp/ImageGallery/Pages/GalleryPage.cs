﻿using HealthTrackerApp.HelperClasses;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.ImageGallery.Pages
{
    public class GalleryPage : TabbedPage
    {
        public GalleryPage()
        {
            Title = "Gallery";

            Children.Add(new ImageGallery());
            Children.Add(new DocumentGalleryPage());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
        }
    }
}
