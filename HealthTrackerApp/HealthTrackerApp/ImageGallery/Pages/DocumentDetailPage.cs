﻿using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.ImageGallery.Pages
{
    public class DocumentDetailPage : ContentPage
    {
        private WebView webView;
        private string DocUrl;
        private StackLayout stackLayout;

        public DocumentDetailPage(string docUrl)
        {
            Title = "Document";
            webView = new WebView();
            stackLayout = new StackLayout();
            DocUrl = docUrl;
        }
        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                webView.HorizontalOptions = LayoutOptions.FillAndExpand;
                webView.VerticalOptions = LayoutOptions.FillAndExpand;
                webView.Source = DocUrl;

                webView.HeightRequest = 1000;
                webView.WidthRequest = 1000;

                stackLayout.Children.Add(webView);
                this.Content = stackLayout;

                webView.Navigating += WebView_Navigating;
                webView.Navigated += WebView_Navigated;
            }
            catch (Exception ex)
            {
                
            }
        }

        private void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
        }

        private void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));
        }
    }
}
