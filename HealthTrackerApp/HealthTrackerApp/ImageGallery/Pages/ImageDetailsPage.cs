﻿using FFImageLoading.Forms;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Surveys.Pages;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HealthTrackerApp.ImageGallery.Pages
{
    public class ImageDetailsPage : BasePage
    {
        private string imageUrl;
        private CachedImage fullimage;
        private Image saveGalleryimg;
        private StackLayout mStacklayout;
        byte[] imageBytes;
        public ImageDetailsPage(string imageUrl, string title)
        {
           
            this.imageUrl = imageUrl;
            Title = title;
            
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
           
            fullimage = new CachedImage();
            saveGalleryimg = new Image();
            mStacklayout = new StackLayout();
            if (InternetConnectivity.IsInternetConnected)
            {
                await DownloadImageasync();
            }
            
            UpdateUI();


        }

    
        private async Task DownloadImageasync()
        {
           
            //using (var webClient = new WebClient())
            //{
            var webClient = new WebClient();
                imageBytes = webClient.DownloadData(imageUrl);
            //}
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
        }

        private void UpdateUI()
        {
            fullimage.Source = imageUrl;
            fullimage.LoadingPlaceholder = "placeholder.png";
            fullimage.ErrorPlaceholder = "internet.png";
            fullimage.Aspect = Aspect.AspectFit;
            fullimage.Margin = new Thickness(5, 5, 5, 0);
            fullimage.HorizontalOptions = LayoutOptions.FillAndExpand;
            fullimage.VerticalOptions = LayoutOptions.FillAndExpand;
           

            saveGalleryimg.HorizontalOptions = LayoutOptions.End;
            saveGalleryimg.VerticalOptions = LayoutOptions.End;
            saveGalleryimg.HeightRequest = 30;
            saveGalleryimg.WidthRequest = 30;
            saveGalleryimg.Source = "savebutton.png";
            saveGalleryimg.Margin = new Thickness(0, 0, 10, 5);

            mStacklayout.Children.Add(fullimage);
            mStacklayout.Children.Add(saveGalleryimg);
            //mStacklayout.Spacing = 10;
           
            Content = mStacklayout;

           
            var tappedRecogniser = new TapGestureRecognizer();
            tappedRecogniser.Tapped += TappedRecogniser_TappedAsync;
            saveGalleryimg.GestureRecognizers.Add(tappedRecogniser);

        }


        private async void TappedRecogniser_TappedAsync(object sender, EventArgs e)
        {
            string imagename = string.Format("img_{0}.jpg", DateTime.UtcNow.ToString("yyyyMMddhhmmss"));
            var storagePermission = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
            //var cameraPermission = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            if (storagePermission != PermissionStatus.Granted/* && cameraPermission != PermissionStatus.Granted*/)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] {  Permission.Storage/*,Permission.Camera*/ });
                storagePermission = results[Permission.Storage];
                //cameraPermission = results[Permission.Camera]
            }
            DependencyService.Get<IPicture>().SavePictureToDisk(imagename, imageBytes);
           await DisplayAlert("Alert", "Image Saved", "OK");
        }

        protected override bool OnBackButtonPressed()
        {

          //  Navigation.PopAsync();
            MessagingCenter.Send<InitCarauselUI>(new InitCarauselUI(), "SetOnAppearCheck");
            return base.OnBackButtonPressed();

        }


    }
}
