﻿using DLToolkit.Forms.Controls;
using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.ImageGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealthTrackerApp.ImageGallery.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DocumentGalleryPage : ContentPage
	{
        private List<DocumentGalleryModel> documentList;

        public DocumentGalleryPage ()
		{
			InitializeComponent ();

            Title = "Documents";

            documentList = APICalls.GetDocuments();

            var iconsList = documentList.Select(x => x.iconUrl).ToList();

            myList.FlowItemsSource = documentList;
            myList.HasUnevenRows = true;
            myList.FlowColumnCount = 2;
        }

        private void FlowListView_OnFlowItemTapped(object sender, ItemTappedEventArgs e)
        {
            //var grourp = e.Group;
            var docData = e.Item as DocumentGalleryModel;
            Navigation.PushAsync(new DocumentDetailPage(docData.DocumentUrl));
        }
    }
}