﻿using DLToolkit.Forms.Controls;
using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.ImageGallery.Cells;
using HealthTrackerApp.ImageGallery.Models;
using HealthTrackerApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealthTrackerApp.ImageGallery.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ImageGallery : ContentPage
	{
        private long lastPress;
        private List<ImageGalleryModel> imagelist;

        public ImageGallery ()
		{
			InitializeComponent ();
            Title = "Image Gallery";
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();
            IntialiseandSetUpUI();
        }

        private void IntialiseandSetUpUI()
        {
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));
             imagelist = new List<ImageGalleryModel>();
            //listImagegallery = new FlowListView();
            //  listImagegallery.HorizontalOptions = LayoutOptions.FillAndExpand;
            // listImagegallery.VerticalOptions = LayoutOptions.FillAndExpand;
            imagegallerylist.FlowColumnCount = 2;
            imagegallerylist.SeparatorColor = Color.FromHex(MyUtility.MenuBackgroundColor);
            /*
             * Set the ItemSource by getting the values from the API 
             * Set the ItemTemplate as ImageGalleryCell
             */

            imagelist = APICalls.GetImageGalleryModels();
            imagegallerylist.HasUnevenRows = true;
            imagegallerylist.FlowItemsSource = imagelist;

         //   listImagegallery.FlowColumnTemplate = new DataTemplate(typeof(ImageGalleryCell));
            Content = imagegallerylist;

            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));



           // listImagegallery.ItemSelected += ListImagegallery_ItemSelected;
        }

        //private void ListImagegallery_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        //{
        //    var x = e.SelectedItem as ImageGalleryModel;

        //    Navigation.PushAsync(new ImageDetailsPage(x.ImageUrl, x.Title));
        //}

        protected override bool OnBackButtonPressed()
        {

            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 5000)
            {
                DisplayAlert("", "Press back again to exit", "OK");
                lastPress = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }

       private void  FlowListView_OnFlowItemTapped(object sender, ItemTappedEventArgs e)
        {
            var imageData = e.Item as ImageGalleryModel;
            Navigation.PushAsync(new ImageDetailsPage(imageData.ImageUrl,imageData.Title));
        }
    }
}