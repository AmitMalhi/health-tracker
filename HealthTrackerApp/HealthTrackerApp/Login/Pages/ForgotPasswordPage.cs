﻿using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.Utility;
using Plugin.InputKit.Shared.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace HealthTrackerApp.Login.Pages
{
   public class ForgotPasswordPage : BasePage
    {
       private Image mImgLogo;
       private AdvancedEntry mEntryforgot_pwd;
       private Button mSendmail_btn;
        private Label mlblUsername;
       private StackLayout mForgotpwd_stack,stackBackbtn;
        private Image mimageBackbtn;

        public ForgotPasswordPage()
        {
            this.BackgroundColor = Color.FromHex(MyUtility.MenuBackgroundColor);
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
           
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            mForgotpwd_stack = new StackLayout();
            mImgLogo = new Image();
            mEntryforgot_pwd = new AdvancedEntry();
            mSendmail_btn = new Button();
            mimageBackbtn = new Image();
            stackBackbtn = new StackLayout();
            mlblUsername = new Label();
            InitialiseViews();
        }

        private void InitialiseViews()
        {
            // adding the back button to the stack
            //mimageBackbtn.HorizontalOptions = LayoutOptions.StartAndExpand;
            //mimageBackbtn.VerticalOptions = LayoutOptions.StartAndExpand;
           
            mimageBackbtn.HeightRequest = 20;
            mimageBackbtn.WidthRequest = 20;           
            mimageBackbtn.Source = "backbutton.png";
            mimageBackbtn.Margin = new Thickness(10, 10, 10, 10);
            mimageBackbtn.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnBackButtonPressed()) });

            stackBackbtn.HorizontalOptions = LayoutOptions.StartAndExpand;
            stackBackbtn.VerticalOptions = LayoutOptions.Start;
           
            stackBackbtn.Padding = new Thickness(10, 10, 0, 0);
            stackBackbtn.Children.Add(mimageBackbtn);

            // adding the image to the stack
            mImgLogo.HorizontalOptions = LayoutOptions.CenterAndExpand;
           // mImgLogo.VerticalOptions = LayoutOptions.CenterAndExpand;
            mImgLogo.Margin = new Thickness(10, 0, 10, 0);
            mImgLogo.Source = "healthapplogo.png";

            // adding the label to the stack
            mlblUsername.HorizontalOptions = LayoutOptions.StartAndExpand;
            mlblUsername.Text = "Email";
            mlblUsername.TextColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            mlblUsername.Margin = new Thickness(20, 0, 10, 0);


            // adding the password entry to the stack
            mEntryforgot_pwd.HorizontalOptions = LayoutOptions.FillAndExpand;

            //annotation defines the Type of Input Entry
            mEntryforgot_pwd.Annotation = AdvancedEntry.AnnotationType.Email;

            //Annotation color defines the Color of Error message displayed
            mEntryforgot_pwd.AnnotationColor = Color.Red;

            //Is Required defines whether to show the Error message or not
            mEntryforgot_pwd.IsRequired = true;
            mEntryforgot_pwd.Placeholder = "Enter an email";
            mEntryforgot_pwd.Margin = new Thickness(10, 0, 10, 0);

            //adding the button to the stack
            mSendmail_btn.HorizontalOptions = LayoutOptions.CenterAndExpand;
           // mSendmail_btn.VerticalOptions = LayoutOptions.CenterAndExpand;
            mSendmail_btn.WidthRequest = 75;
            mSendmail_btn.Text = "Submit";
            mSendmail_btn.CornerRadius = 5;
            mSendmail_btn.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            mSendmail_btn.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
            mSendmail_btn.Clicked += MSendmail_btn_Clicked;

            // adding the forgot password to the main stack
            mForgotpwd_stack.HorizontalOptions = LayoutOptions.Center;
            //  mForgotpwd_stack.VerticalOptions = LayoutOptions.Center;
            mForgotpwd_stack.Padding = new Thickness(0, 80, 0, 0);
            mForgotpwd_stack.Spacing = 10;
            mForgotpwd_stack.Orientation = StackOrientation.Vertical;
            mForgotpwd_stack.Children.Add(mImgLogo);
            mForgotpwd_stack.Children.Add(mlblUsername);
            mForgotpwd_stack.Children.Add(mEntryforgot_pwd);
            mForgotpwd_stack.Children.Add(mSendmail_btn);

            StackLayout stackLayout = new StackLayout();
            stackLayout.Orientation = StackOrientation.Vertical;
           
            stackLayout.Children.Add(stackBackbtn);
           stackLayout.Children.Add(mForgotpwd_stack);
            Content = stackLayout;


        }

       

        protected override bool OnBackButtonPressed()
        {
            this.Navigation.PopAsync();
            return true;
            
        }

        private void MSendmail_btn_Clicked(object sender, EventArgs e)
        {
            if (Validations.IsMailValid(mEntryforgot_pwd.Text) )
            {
                DisplayAlert("Alert", "Email sent", "OK");
            }
            else
            {
                DisplayAlert("Alert", "Enter an valid mail", "OK");
            }
            
        }
    }
}
