﻿using HealthTrackerApp.ApiHelper;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.PreferencesUtility;
using HealthTrackerApp.Profile.Pages;
using HealthTrackerApp.Utility;
using Plugin.InputKit.Shared.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Login.Pages
{
    public class LoginPage : BasePage
    {
       
        private StackLayout mStackLayout, mloginStack;
        private Button mBtnLogin;
        private Image mImgLogo;
        private AdvancedEntry mEntryUsername, mEntryPwd;
        private Label mLblUsername, mLblPass, mLblForgotPwd;

       
        public LoginPage()
        {
            /**
            
            ****/
            this.BackgroundColor = Color.FromHex(MyUtility.MenuBackgroundColor);
            
            /**
             * 
             */ 
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            
            InitializeViews();
            SetUI();
        }

        private void InitializeViews()
        {
            mStackLayout = new StackLayout();
            mBtnLogin = new Button();
            mImgLogo = new Image();
            mEntryUsername = new AdvancedEntry();
            mEntryPwd = new AdvancedEntry();
            mLblUsername = new Label();
            mLblPass = new Label();
            mLblForgotPwd = new Label();
            mloginStack = new StackLayout();
        }
        private void SetUI()
        {
            // image logo initialsation
            mImgLogo.HorizontalOptions = LayoutOptions.CenterAndExpand;
            mImgLogo.VerticalOptions = LayoutOptions.CenterAndExpand;
            mImgLogo.Margin = new Thickness(10, 0, 10, 0);
            mImgLogo.Source = "healthapplogo.png";
           

            // usernamelabel initialisation         
            mLblUsername.HorizontalOptions = LayoutOptions.StartAndExpand;
            mLblUsername.Text = "Email";
            mLblUsername.TextColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            mLblUsername.Margin = new Thickness(20, 0, 10, 0);

            // mEntryUsername initialisation
            mEntryUsername.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryUsername.Placeholder = "Email";            
            mEntryUsername.Annotation = AdvancedEntry.AnnotationType.Email;
            mEntryUsername.AnnotationColor = Color.Red;
            mEntryUsername.IsRequired = true;
            mEntryUsername.Margin = new Thickness(10, 0, 10, 0);            
            mEntryUsername.ValidationMessage = "Invalid email address";
            mEntryUsername.Text = "gopal@meetappevent.com";

            // mlblpass initialisation         
            mLblPass.HorizontalOptions = LayoutOptions.StartAndExpand;
            mLblPass.Text = "Password";
            mLblPass.TextColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            mLblPass.Margin = new Thickness(20, 0, 10, 0);

            // mEntryPwd initialisation
            mEntryPwd.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryPwd.Annotation = AdvancedEntry.AnnotationType.Password;
            mEntryPwd.Placeholder = "Password";
            mEntryPwd.Margin = new Thickness(10, 0, 10, 0);
            mEntryPwd.Text = "amit";

            // mlblForgotPwd initialisation
            mLblForgotPwd.HorizontalOptions = LayoutOptions.EndAndExpand;
            mLblForgotPwd.Margin = new Thickness(0, 0, 10, 0);
            mLblForgotPwd.Text = "Forgot Passsword?";
            mLblForgotPwd.TextColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            mLblForgotPwd.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnForgotPass()) });

            // mBtnLogin initialisation
            mBtnLogin.HorizontalOptions = LayoutOptions.CenterAndExpand;
            mBtnLogin.VerticalOptions = LayoutOptions.CenterAndExpand;
            mBtnLogin.WidthRequest = 70;
            mBtnLogin.Text = "Login";
            mBtnLogin.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            mBtnLogin.CornerRadius = 5;
            mBtnLogin.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
            mBtnLogin.Clicked += Login_btn_Clicked;

            // mlogin stack initialisation
            mloginStack.HorizontalOptions = LayoutOptions.FillAndExpand;
            mloginStack.Spacing = 10;
            mloginStack.Orientation = StackOrientation.Vertical;

            // adding the login views to the mlogin stack                        
            mloginStack.Children.Add(mLblUsername);
            mloginStack.Children.Add(mEntryUsername);
            mloginStack.Children.Add(mLblPass);
            mloginStack.Children.Add(mEntryPwd);
            mloginStack.Children.Add(mLblForgotPwd);


            //now adding all the views to the mStackLayout
            mStackLayout.Spacing = 10;
            mStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;
            mStackLayout.VerticalOptions = LayoutOptions.CenterAndExpand;
            mStackLayout.Orientation = StackOrientation.Vertical;
            mStackLayout.Children.Add(mImgLogo);
            mStackLayout.Children.Add(mloginStack);
            mStackLayout.Children.Add(mBtnLogin);
            Content = mStackLayout;
        }

        /**
         * Event Handler to Validate and Login
         */ 
        private void Login_btn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");

            try
            {
                var IsAuthenticated = APICalls.Instance.AuthenticateUser(mEntryUsername.Text.ToString(), mEntryPwd.Text.ToString());

                if (IsAuthenticated)
                {

                    UserPreferences.getInstance.SetString("AuthToken", mEntryUsername.Text.ToString());
                    Navigation.PushAsync(new UserProfilePage());
                    Navigation.RemovePage(this);
                }
                else
                {
                    DisplayAlert("Alert", "Invalid Credentials", "OK");
                }

            }
            catch(Exception ex)
            {
                DisplayAlert("Alert", "Username and password cannot be empty", "OK");
            }
            

            //IsMailVerified =   MailVerification();
        }

       

        private void OnForgotPass()
        {
            Navigation.PushAsync(new ForgotPasswordPage());
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAsync();
            return base.OnBackButtonPressed();
        }
    }
}
