﻿using HealthTrackerApp.DatabaseLayer.DBRepositary;
using HealthTrackerApp.HelperClasses;
using HealthTrackerApp.MasterDetailDrawer;
using HealthTrackerApp.PreferencesUtility;
using HealthTrackerApp.Utility;
using ImageCircle.Forms.Plugin.Abstractions;
using Plugin.InputKit.Shared.Controls;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.Profile.Pages
{
    public class UserProfilePage : BasePage
    {
        private ScrollView scrollView;
        private CircleImage userImage;
        private StackLayout stackLayout,mbtnStack, mfinalStack;
        private Button submitButton;
        private byte[] imageAsBytes;
        UserProfile userProfile;
        IUpdateMenuPage updatemenupage;
        private const string CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY = "Choose from Gallery";
        private const string CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO = "Take Photo";
        private AdvancedEntry mEntryFirstname, mEntryLastName, mEntryHeight, mEntryWeight,mEntryEmail, mEntryAge;
        private long lastPress;
        //CustomDatePicker mDOB;
        private Frame dateframe;

        public interface IUpdateMenuPage
        {
            void UpdateProfileInfo(byte[] urlImg, string name);
        }

        public UserProfilePage(IUpdateMenuPage updatemenupage)
        {
            Title = "My Profile";
            this.updatemenupage = updatemenupage;
            mbtnStack = new StackLayout();
            mfinalStack = new StackLayout();
            userImage = new CircleImage();
            scrollView = new ScrollView();
            dateframe = new Frame();
            mEntryFirstname = new AdvancedEntry();
            mEntryLastName = new AdvancedEntry();
            mEntryHeight = new AdvancedEntry();
            mEntryWeight = new AdvancedEntry();
            mEntryEmail = new AdvancedEntry();
            mEntryAge = new AdvancedEntry();
            //mDOB = new CustomDatePicker();

            submitButton = new Button();
            submitButton.Clicked += SubmitButton_Clicked;
            userImage.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnUserImageClick()) });
            stackLayout = new StackLayout();
            userProfile = new UserProfileDBRepository().GetFirstOrDefault();
        }

        public UserProfilePage()
        {
            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
            }));
            Title = "My Profile";
            userImage = new CircleImage();
            scrollView = new ScrollView();
            dateframe = new Frame();
            mbtnStack = new StackLayout();
            mfinalStack = new StackLayout();
            mEntryFirstname = new AdvancedEntry();
            mEntryLastName = new AdvancedEntry();
            mEntryHeight = new AdvancedEntry();
            mEntryWeight = new AdvancedEntry();
            mEntryEmail = new AdvancedEntry();
            mEntryAge = new AdvancedEntry();
            //mDOB = new CustomDatePicker();

            submitButton = new Button();
            submitButton.Clicked += SubmitButton_Clicked;
            userImage.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command(() => OnUserImageClick()) });
            stackLayout = new StackLayout();
            userProfile = new UserProfileDBRepository().GetFirstOrDefault();
        }

        protected override void OnAppearing()
        {
           
            base.OnAppearing();
           
            // mEntryFirstName initialisation
            mEntryFirstname.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryFirstname.Placeholder = "First Name";
            mEntryFirstname.Annotation = AdvancedEntry.AnnotationType.NameSurname;
            mEntryFirstname.AnnotationColor = Color.Red;
            mEntryFirstname.IsRequired = true;
            mEntryFirstname.Margin = new Thickness(10, 0, 10, 0);
            
            // mEntryLastName initialisation
            mEntryLastName.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryLastName.Annotation = AdvancedEntry.AnnotationType.NameSurname;
            mEntryLastName.Placeholder = "Last Name";
            mEntryLastName.Margin = new Thickness(10, 0, 10, 0);

            // mEntryEmail initialisation
            mEntryEmail.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryEmail.Annotation = AdvancedEntry.AnnotationType.NameSurname;
            mEntryEmail.Text = UserPreferences.getInstance.GetString("AuthToken");
            mEntryEmail.Placeholder = "Email";
            mEntryEmail.Margin = new Thickness(10, 0, 10, 0);
            mEntryEmail.IsDisabled = true;

            // mEntryLastName initialisation
            mEntryHeight.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryHeight.Annotation = AdvancedEntry.AnnotationType.NameSurname;
            mEntryHeight.Placeholder = "Height in cm";
            mEntryHeight.Margin = new Thickness(10, 0, 10, 0);
            
            // mEntryLastName initialisation
            mEntryWeight.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryWeight.Annotation = AdvancedEntry.AnnotationType.NameSurname;
            mEntryWeight.Placeholder = "Weight in kg";
            mEntryWeight.Margin = new Thickness(10, 0, 10, 0);

            // mEntry Date of birth
            ////mDOB.HorizontalOptions = LayoutOptions.FillAndExpand;
            //mDOB.Margin = new Thickness(10, 0, 10, 0);
            //mDOB.HeightRequest = 50;
            // mDOB.VerticalOptions = LayoutOptions.Start;
            //dateframe.Margin = new Thickness(10, 0, 10, 0);
            //dateframe.HeightRequest = 20;
            //dateframe.CornerRadius = 18;
            //dateframe.Content = mDOB;

            mEntryAge.HorizontalOptions = LayoutOptions.FillAndExpand;
            mEntryAge.Annotation = AdvancedEntry.AnnotationType.Integer;
            mEntryAge.Placeholder = "Age";
            mEntryAge.Margin = new Thickness(10, 0, 10, 0);

            userImage.HeightRequest = Application.Current.MainPage.Width / 2;
            userImage.WidthRequest = HeightRequest;
            userImage.HorizontalOptions = LayoutOptions.Center;
            userImage.Aspect = Aspect.AspectFit;
            userImage.Source = "username.png";

            userImage.Margin = new Thickness(0, 20, 0, 0);

            if (userProfile != null)
            {
                mEntryFirstname.Text = userProfile.FirstName;
                mEntryLastName.Text = userProfile.LastName;
                //mDOB.Date = Convert.ToDateTime(userProfile.dob);
                   
                if (userProfile.Height != null)
                {
                    mEntryHeight.Text = userProfile.Height;
                }
                if (userProfile.Weight != null)
                {
                    mEntryWeight.Text = userProfile.Weight;
                }
                if (userProfile.Image != null)
                {
                    userImage.Source = ImageSource.FromStream(() => new MemoryStream(userProfile.Image));
                }
                if (userProfile.Age != null)
                {
                    mEntryAge.Text = userProfile.Age;
                }
            }
            else
            {
                mEntryFirstname.Placeholder = "First Name";
                mEntryLastName.Placeholder = "Last Name";
                mEntryHeight.Placeholder = "Height in cm";
                mEntryWeight.Placeholder = "Weight in kg";
                mEntryAge.Placeholder = "Age";
            }

            submitButton.Text = "SUBMIT";
            submitButton.FontSize = 18;
            submitButton.BackgroundColor = Color.FromHex(MyUtility.StatusBarBackgroundColor);
            submitButton.TextColor = Color.FromHex(MyUtility.StatusBarTextColor);
            submitButton.VerticalOptions = LayoutOptions.EndAndExpand;

            stackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;
            stackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
            stackLayout.Children.Add(userImage);
            stackLayout.Children.Add(mEntryFirstname);
            stackLayout.Children.Add(mEntryLastName);
            stackLayout.Children.Add(mEntryEmail);
            stackLayout.Children.Add(mEntryAge);
            stackLayout.Children.Add(mEntryHeight);
            stackLayout.Children.Add(mEntryWeight);
            stackLayout.Children.Add(submitButton);
            scrollView.Content = stackLayout;

            this.Content = scrollView;

            Device.BeginInvokeOnMainThread(new Action(() =>
            {
                DependencyService.Get<IShowProgressDialog>().dissmissDialog();
            }));
        }


        private void SubmitButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    DependencyService.Get<IShowProgressDialog>().showDialog("Loading...");
                }));


                string fname = mEntryFirstname.Text;
                string lname = mEntryLastName.Text;
                string uheight = mEntryHeight.Text;
                string uweight = mEntryWeight.Text;
                string uAge = mEntryAge.Text;
                //string dob = mDOB.Date.ToShortDateString();
                if (userProfile != null && imageAsBytes == null)
                    imageAsBytes = userProfile.Image;

                if (!string.IsNullOrEmpty(mEntryFirstname.Text) && !string.IsNullOrEmpty(mEntryLastName.Text) && !string.IsNullOrEmpty(mEntryHeight.Text) && !string.IsNullOrEmpty(mEntryWeight.Text) && !string.IsNullOrEmpty(mEntryAge.Text))
                {
                   
                    var user = new UserProfile(fname, lname, uheight, uweight, imageAsBytes, uAge);
                    new UserProfileDBRepository().InsertOrReplace(user);
                     DisplayAlert("", "Profile Updated !", "OK");
                    
                    if (updatemenupage != null)
                    {
                        updatemenupage.UpdateProfileInfo(imageAsBytes, fname + " " + lname);
                    }
                    else
                    {
                        Navigation.PushAsync(new MyMasterDetailPage(), true);
                        Navigation.RemovePage(this);
                    }
                }
                else
                {
                    DisplayAlert("", "All feilds required", "OK");
                }

                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    DependencyService.Get<IShowProgressDialog>().dissmissDialog();
                }));
            }
            catch (Exception ex)
            {

                DisplayAlert("", "All feilds required", "OK");
            }
        }

        private async void OnUserImageClick()
        {
            string action = await DisplayActionSheet("Add Photo", "Cancel", null, CONST_TAKE_PHOTO_BUTTON_TAKE_PHOTO, CONST_TAKE_PHOTO_BUTTON_FROM_GALLERY);

            if (action == "Take Photo")
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await DisplayAlert("No Camera", "No Camera Available", "OK");
                    return;
                }
                var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    AllowCropping = true,
                    SaveToAlbum = true,
                    Name = "Test.jpg",
                    CompressionQuality = 50,
                });

                if (file == null)
                    return;
                string path = file.Path;
                userImage.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
                using (var memoryStream = new MemoryStream())
                {
                    file.GetStream().CopyTo(memoryStream);
                    file.Dispose();
                    imageAsBytes = memoryStream.ToArray();
                }
            }
            else if (action == "Choose from Gallery")
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("No Upload", "Picking up photo is not supported", "OK");
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                {
                    CompressionQuality = 50,
                });
                if (file == null)
                    return;

                string path = file.Path;
                userImage.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
                using (var memoryStream = new MemoryStream())
                {
                    file.GetStream().CopyTo(memoryStream);
                    file.Dispose();
                    imageAsBytes = memoryStream.ToArray();
                }
            }
            else
            {
                return;
            }
        }

        protected override bool OnBackButtonPressed()
        {

            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 5000)
            {
                DisplayAlert("", "Press back again to exit", "OK");
                lastPress = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
