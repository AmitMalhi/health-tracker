﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.Profile.Pages
{
    public class UserProfile
    {
        [PrimaryKey]
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public byte[] Image { get; set; }
        public string dob { get; set; }
        public string Age { get; set; }

        public UserProfile(string firstName, string lastName, string height, string weight, byte[] image, string age)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Height = height;
            this.Weight = weight;
            this.Image = image;
            this.Age = age;
            //this.dob = dob;
        }

        public UserProfile()
        {

        }
    }
}
