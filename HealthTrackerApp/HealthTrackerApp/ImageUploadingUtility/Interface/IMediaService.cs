﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HealthTrackerApp.ImageUploadingUtility.Interface
{
    public interface IMediaService
    {
        Task OpenGallery();
        void ClearFiles(List<string> filePaths);
    }
}
