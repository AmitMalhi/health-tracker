﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.Interface
{
    public interface IDatabaseConnection
    {
        SQLiteConnection DbConnection();
        bool DBFileExists();
    }
}
