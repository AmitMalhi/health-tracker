﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.Interface
{
    public interface IGenericRepositary<T> where T : new() // ????
    {
        T GetFirstOrDefault();
        List<T> GetAll();
        List<T> GetAll(Expression<Func<T, bool>> predicate);
        T Get(Expression<Func<T, bool>> match);
        int Save(T obj);
        int Update(T obj);
        int Delete(int id);
        int Count();
        int Count(Expression<Func<T, bool>> predicate);
    }
}
