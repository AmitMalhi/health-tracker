﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Notes.Models;
using HealthTrackerApp.NutritionValues.Models;
using HealthTrackerApp.Profile.Pages;
using HealthTrackerApp.Reminders.Models;
using HealthTrackerApp.Surveys.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealthTrackerApp.DatabaseLayer
{
    public class SQLClient
    {
        public SQLiteConnection conn;

        private static SQLClient instance;

        public static SQLClient Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SQLClient();
                }
                return instance;
            }
        }

        //Check File exists. Wether File exists or not, Connection is always created .If file does not exists, Create Table
        private SQLClient()
        {
            try
            {
                bool isFileExists = DependencyService.Get<IDatabaseConnection>().DBFileExists();
                conn = DependencyService.Get<IDatabaseConnection>().DbConnection();
                if (!isFileExists)
                {
                    conn.CreateTable<Dialogue>();
                    conn.CreateTable<DialogueOption>();
                    conn.CreateTable<Reminder>();
                    conn.CreateTable<Note>();
                    conn.CreateTable<UserProfile>();
                    conn.CreateTable<Survey>();
                    conn.CreateTable<MyImage>();
                    conn.CreateTable<FoodItems>();
                    conn.CreateTable<FoodItemHelper>();
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}
