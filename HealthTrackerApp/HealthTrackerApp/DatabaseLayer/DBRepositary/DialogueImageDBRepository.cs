﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class DialogueImageDBRepository : GenericRepositary<MyImage>, IGenericRepositary<MyImage>
    {
        public DialogueImageDBRepository(SQLClient context) : base(context)
        {
        }

        public DialogueImageDBRepository() : base()
        {
        }
    }
}
