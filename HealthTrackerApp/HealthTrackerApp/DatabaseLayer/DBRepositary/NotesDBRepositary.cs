﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Notes.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
   public class NotesDBRepositary : GenericRepositary<Note>, IGenericRepositary<Note>
    {
        public NotesDBRepositary(SQLClient context) : base(context)
        {

        }

        public NotesDBRepositary() : base()
        {

        }


    }
}
