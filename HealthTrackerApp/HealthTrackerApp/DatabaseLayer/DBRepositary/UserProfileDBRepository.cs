﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Profile.Pages;
using HealthTrackerApp.Surveys.Models;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class UserProfileDBRepository : GenericRepositary<UserProfile>, IGenericRepositary<UserProfile>
    {
        public UserProfileDBRepository(SQLClient context) : base(context)
        {
        }

        public UserProfileDBRepository() : base()
        {
        }
    }
}
