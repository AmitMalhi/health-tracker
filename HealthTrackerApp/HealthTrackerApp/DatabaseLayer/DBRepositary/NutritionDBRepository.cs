﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.NutritionValues.Models;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class NutritionDBRepository : GenericRepositary<FoodItems>, IGenericRepositary<FoodItems>
    {
        public NutritionDBRepository(SQLClient context) : base(context)
        {
        }

        public NutritionDBRepository() : base()
        {
        }
        public int Delete(string id)
        {
            lock (_context.conn)
            {
                return _context.conn.Delete<FoodItems>(id);
            }
        }
    }
}
