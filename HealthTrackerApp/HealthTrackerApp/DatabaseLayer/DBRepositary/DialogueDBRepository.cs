﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class DialogueDBRepository : GenericRepositary<Dialogue>, IGenericRepositary<Dialogue>
    {
        public DialogueDBRepository(SQLClient context) : base(context)
        {
        }

        public DialogueDBRepository() : base()
        {
        }
    }
}
