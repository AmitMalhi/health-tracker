﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class SurveyDBRepository : GenericRepositary<Survey>, IGenericRepositary<Survey>
    {
        public SurveyDBRepository(SQLClient context) : base(context)
        {
        }

        public SurveyDBRepository() : base()
        {
        }
    }
}
