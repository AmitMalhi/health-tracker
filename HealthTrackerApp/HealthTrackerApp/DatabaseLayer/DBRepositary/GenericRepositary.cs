﻿using HealthTrackerApp.DatabaseLayer.Interface;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
   public abstract class GenericRepositary<T> : IGenericRepositary<T> where T : new()
    {
        protected SQLClient _context;

        public GenericRepositary(SQLClient context)
        {
            _context = context;
        }
        public GenericRepositary()
        {
            _context = SQLClient.Instance;
        }
        public virtual T GetFirstOrDefault()
        {
            lock (_context.conn)
            {
                var obj = _context.conn.Table<T>().FirstOrDefault();
                return obj;
            }
        }
        public virtual List<T> GetAll()
        {
            lock (_context.conn)
            {
                var obj = _context.conn.Table<T>().ToList();
                return obj;
            }
        }
        public virtual int Save(T obj)
        {
            lock (_context.conn)
                return _context.conn.Insert(obj);
        }
        public virtual int Update(T obj)
        {
            lock (_context.conn)
                return _context.conn.Update(obj);
        }
        public virtual int InsertOrReplace(T obj)
        {
            lock (_context.conn)
                return _context.conn.InsertOrReplace(obj);
        }

        public virtual int Delete(int id)
        {
            lock (_context.conn)
            {
                return _context.conn.Delete<T>(id);
            }  
        }
        public virtual int DeleteAll()
        {
            lock (_context.conn)
            {
                return _context.conn.DeleteAll<T>();
            }
        }
        public virtual List<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            lock (_context.conn)
            {
                var obj = _context.conn.Table<T>().Where(predicate).ToList();
                return obj;
            }
        }
        public virtual int Count()
        {
            lock (_context.conn)
            {
                return _context.conn.Table<T>().Count();
            }
        }
        public virtual int Count(Expression<Func<T, bool>> predicate)
        {
            lock (_context.conn)
            {
                return _context.conn.Table<T>().Count(predicate);
            }
        }
        public virtual T Get(Expression<Func<T, bool>> match)
        {
            lock (_context.conn)
            {
                var obj = _context.conn.Table<T>().FirstOrDefault(match);
                return obj;
            }
        }
    }
}
