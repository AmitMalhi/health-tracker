﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.NutritionValues.Models;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class FoodItemDBRepository : GenericRepositary<FoodItemHelper>, IGenericRepositary<FoodItemHelper>
    {
        public FoodItemDBRepository(SQLClient context) : base(context)
        {
        }

        public FoodItemDBRepository() : base()
        {
        }
    }
}
