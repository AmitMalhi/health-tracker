﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Surveys.Models;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class SurveyOptionDBRepository : GenericRepositary<DialogueOption>, IGenericRepositary<DialogueOption>
    {
        public SurveyOptionDBRepository(SQLClient context) : base(context)
        {
        }

        public SurveyOptionDBRepository() : base()
        {
        }
    }
}
