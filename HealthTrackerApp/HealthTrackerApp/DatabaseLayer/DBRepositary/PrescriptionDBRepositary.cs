﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Prescriptions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class PrescriptionDBRepositary : GenericRepositary<Prescription>,IGenericRepositary<Prescription>
    {
        public PrescriptionDBRepositary (SQLClient context) : base(context)
        {

        }

        public PrescriptionDBRepositary () : base()
        {

        }
    }
}
