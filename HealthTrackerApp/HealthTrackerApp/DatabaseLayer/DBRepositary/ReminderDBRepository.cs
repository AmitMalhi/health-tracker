﻿using HealthTrackerApp.DatabaseLayer.Interface;
using HealthTrackerApp.Reminders.Models;
using HealthTrackerApp.Surveys.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthTrackerApp.DatabaseLayer.DBRepositary
{
    public class ReminderDBRepository : GenericRepositary<Reminder>, IGenericRepositary<Reminder>
    {
        public ReminderDBRepository(SQLClient context) : base(context)
        {
        }

        public ReminderDBRepository() : base()
        {
        }

        public int Delete(long id)
        {
            lock (_context.conn)
            {
                return _context.conn.Delete<Reminder>(id);
            }
        }
    }
}
